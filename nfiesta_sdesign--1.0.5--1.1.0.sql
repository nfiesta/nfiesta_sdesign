--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

ALTER TABLE sdesign.t_inventory_campaign ADD COLUMN inventory_campaign_begin INTEGER;
ALTER TABLE sdesign.t_inventory_campaign ADD COLUMN inventory_campaign_end INTEGER;

ALTER TABLE sdesign.t_inventory_campaign 
    ADD CONSTRAINT fkey__t_inventory_campaign__t_inventory_campaign_begin FOREIGN KEY (inventory_campaign_begin) 
    REFERENCES sdesign.t_inventory_campaign(id);

ALTER TABLE sdesign.t_inventory_campaign 
    ADD CONSTRAINT fkey__t_inventory_campaign__t_inventory_campaign_end FOREIGN KEY (inventory_campaign_end) 
    REFERENCES sdesign.t_inventory_campaign(id);

-- <function name="fn_check_begin_end_campaigns" schema="sdesign" src="functions/fn_check_t_inventory_campaign.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE FUNCTION sdesign.fn_check_t_inventory_campaign() RETURNS trigger AS $fn_body$
DECLARE
    BEGIN
        IF (NEW.inventory_campaign_begin IS NULL AND NEW.inventory_campaign_end IS NULL) THEN
            RETURN NEW;
        END IF;

        IF ((NEW.inventory_campaign_begin IS NULL AND NEW.inventory_campaign_end IS NOT NULL) OR
            (NEW.inventory_campaign_begin IS NOT NULL AND NEW.inventory_campaign_end IS NULL)
        )  THEN
            RAISE EXCEPTION 'both inventory_campaign_begin and inventory_campaign_end have to be NULL or NOT NULL';
        END IF;

        IF (NEW.inventory_campaign_begin = NEW.inventory_campaign_end) THEN
            RAISE EXCEPTION 'inventory_campaign_begin and inventory_campaign_end have to be different';
        END IF;

        --inventory_campaign_begin and inventory_campaign_end have to be "state"
        IF NOT (
            select begin_campaign_is_state and end_campaign_is_state
            from 
                (select inventory_campaign_begin is NULL and inventory_campaign_end is NULL as begin_campaign_is_state
                    from sdesign.t_inventory_campaign where id = NEW.inventory_campaign_begin) as begint,
                (select inventory_campaign_begin is NULL and inventory_campaign_end is NULL as end_campaign_is_state
                    from sdesign.t_inventory_campaign where id = NEW.inventory_campaign_end) as endt
        ) THEN
            RAISE EXCEPTION 'referenced campaigns have to be "state" (with NULL inventory_campaign_begin/end) ';
        ELSE
            RETURN NEW;
        END IF;
    END;
$fn_body$ LANGUAGE plpgsql;

CREATE TRIGGER check_t_inventory_campaign
    BEFORE INSERT ON sdesign.t_inventory_campaign
    FOR EACH ROW
    EXECUTE FUNCTION sdesign.fn_check_t_inventory_campaign();
-- </function>

-------------------------

ALTER TABLE sdesign.t_reference_year_set ADD COLUMN reference_year_set_begin INTEGER;
ALTER TABLE sdesign.t_reference_year_set ADD COLUMN reference_year_set_end INTEGER;

ALTER TABLE sdesign.t_reference_year_set 
    ADD CONSTRAINT fkey__t_reference_year_set__t_reference_year_set_begin FOREIGN KEY (reference_year_set_begin) 
    REFERENCES sdesign.t_reference_year_set(id);

ALTER TABLE sdesign.t_reference_year_set 
    ADD CONSTRAINT fkey__t_reference_year_set__t_reference_year_set_end FOREIGN KEY (reference_year_set_end) 
    REFERENCES sdesign.t_reference_year_set(id);

-- <function name="fn_check_begin_end_panels" schema="sdesign" src="functions/fn_check_t_reference_year_set.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--


CREATE FUNCTION sdesign.fn_check_t_reference_year_set() RETURNS trigger AS $fn_body$
DECLARE
    BEGIN
        IF (NEW.reference_year_set_begin IS NULL AND NEW.reference_year_set_end IS NULL) THEN
            RETURN NEW;
        END IF;

        IF ((NEW.reference_year_set_begin IS NULL AND NEW.reference_year_set_end IS NOT NULL) OR
            (NEW.reference_year_set_begin IS NOT NULL AND NEW.reference_year_set_end IS NULL)
        )  THEN
            RAISE EXCEPTION 'both reference_year_set_begin and reference_year_set_end have to be NULL or NOT NULL';
        END IF;

        IF (NEW.reference_year_set_begin = NEW.reference_year_set_end) THEN
            RAISE EXCEPTION 'reference_year_set_begin and reference_year_set_end have to be different';
        END IF;

         --reference_year_set_begin and reference_year_set_end have to be "state"
        IF NOT (
            select begin_reference_year_set_is_state and end_reference_year_set_is_state
            from 
                (select reference_year_set_begin is NULL and reference_year_set_end is NULL as begin_reference_year_set_is_state
                    from sdesign.t_reference_year_set where id = NEW.reference_year_set_begin) as begint,
                (select reference_year_set_begin is NULL and reference_year_set_end is NULL as end_reference_year_set_is_state
                    from sdesign.t_reference_year_set where id = NEW.reference_year_set_end) as endt
        ) THEN
            RAISE EXCEPTION 'reference_year_set_begin and reference_year_set_end have to be "state" (with NULL reference_year_set_begin/end) ';
        ELSE
            RETURN NEW;
        END IF;

        --reference_year_set_begin and reference_year_set_end have to contain same panels
        IF NOT (
            select panel_begin = panel_end
            from 
                (select array_agg(panel order by panel) as panel_begin
                    from sdesign.cm_refyearset2panel_mapping where reference_year_set = NEW.reference_year_set_begin) as begint,
                (select array_agg(panel order by panel) as panel_end
                    from sdesign.cm_refyearset2panel_mapping where reference_year_set = NEW.reference_year_set_end) as endt
        ) THEN
            RAISE EXCEPTION 'reference_year_set_begin and reference_year_set_end have to contain same panels';
        ELSE
            RETURN NEW;
        END IF;
        
        --reference_year_set_begin and reference_year_set_end must have different inventory campaigns
        IF NOT (
            select begin_inventory_campaign != end_inventory_campaign
            from 
                (select inventory_campaign as begin_inventory_campaign
                    from sdesign.t_reference_year_set where id = NEW.reference_year_set_begin) as begint,
                (select inventory_campaign as end_inventory_campaign
                    from sdesign.t_reference_year_set where id = NEW.reference_year_set_end) as endt
        ) THEN
            RAISE EXCEPTION 'reference_year_set_begin and reference_year_set_end must have different inventory campaigns';
        ELSE
            RETURN NEW;
        END IF;

        --na reference_dates - musí být ty samé, co begin z reference_year_set_begin....  
        IF NOT (
            select NEW.reference_date_begin = begin_reference_date_begin and NEW.reference_date_end = end_reference_date_end
            from 
                (select reference_date_begin as begin_reference_date_begin
                    from sdesign.t_reference_year_set where id = NEW.reference_year_set_begin) as begint,
                (select reference_date_end as end_reference_date_end
                    from sdesign.t_reference_year_set where id = NEW.reference_year_set_end) as endt
        ) THEN
            RAISE EXCEPTION 'reference_year_set_begin and reference_year_set_end must have different inventory campaigns';
        ELSE
            RETURN NEW;
        END IF;
    END;
$fn_body$ LANGUAGE plpgsql;

CREATE TRIGGER check_t_reference_year_set
    BEFORE INSERT ON sdesign.t_reference_year_set
    FOR EACH ROW
    EXECUTE FUNCTION sdesign.fn_check_t_reference_year_set();
-- </function>

-- <function name="fn_import_data" schema="sdesign" src="functions/fn_import_data.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--
---------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- fn_import_data
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS sdesign.fn_import_data(character varying) CASCADE;

CREATE OR REPLACE FUNCTION sdesign.fn_import_data(_schema character varying)
RETURNS text
AS
$$
DECLARE
		_res	text;
BEGIN
	-- Data for Name: c_country; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE 'insert into sdesign.c_country(label, description)
	select country, comment from '||_schema||'.countries';

	EXECUTE 'CREATE TEMPORARY TABLE t_cluster_configuration_temp ON COMMIT DROP AS
		SELECT (select coalesce(max(id),0) from sdesign.t_cluster_configuration) + row_number() over() AS id,
			*
		FROM '||_schema||'.cluster_configurations';

	-- Data for Name: t_cluster_configuration; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE'
	insert into sdesign.t_cluster_configuration(id, cluster_configuration, cluster_design, cluster_rotation, geom, plots_per_cluster, label, comment)
	select
		id,
		cluster_configuration,
		cluster_design,
		cluster_rotation,
		ST_GeomFromText(cluster_geom)::geometry(MULTIPOINT) as geom,
		case when cluster_design then
			ST_NumGeometries(ST_GeomFromText(cluster_geom)::geometry(MultiPoint))
		else
			1
		end as plots_per_cluster,
		label,
		comment
	from t_cluster_configuration_temp';

	PERFORM setval('sdesign.t_cluster_configuration_id_seq', (SELECT coalesce(max(id),0)+1 FROM sdesign.t_cluster_configuration), FALSE);

	-- Data for Name: t_strata_set; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE 'insert into sdesign.t_strata_set(country, strata_set, label, comment)
	select c_country.id, strata_sets.strata_set, strata_sets.label, strata_sets.comment 
	from sdesign.c_country
	inner join '||_schema||'.strata_sets
	on strata_sets.country = c_country.label
	order by country, strata_set';

	-- Data for Name: t_stratum; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	insert into sdesign.t_stratum(stratum, area_m2, buffered_area_m2, comment, geom, strata_set, label)
	select
		strata.stratum,
		coalesce(strata.area_m2, ST_Area(ST_GeomFromEWKT(strata.stratum_geometry))) as area_m2,
		strata.buffered_area_m2,
		strata.comment,
		ST_GeomFromEWKT(strata.stratum_geometry) as geom,
		t_strata_set.id as strata_set,
		strata.label
	from sdesign.c_country
	inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
	inner join '||_schema||'.strata	
		on strata.strata_set = t_strata_set.strata_set
		and strata.country = c_country.label';

	-- Data for Name: t_panel; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	with w_clusters_data as (
		select country, strata_set, stratum, panel, count(*) as cluster_count, sum(sampling_weight) as sweight_panel_sum
		from '||_schema||'.clusters
		group by country, strata_set, stratum, panel order by panel
	)
	, w_plots_data as (
		select country, strata_set, stratum, panel, count(*) as plot_count
		from '||_schema||'.plots
		group by country, strata_set, stratum, panel order by panel
	)
	insert into sdesign.t_panel(stratum, panel, cluster_configuration, label, comment, cluster_count, plot_count, sweight_panel_sum)
	select
		t_stratum.id as stratum,
		panels.panel,
		t_cluster_configuration_temp.id as cluster_configuration,
		panels.label,
		panels.comment,
		w_clusters_data.cluster_count,
		w_plots_data.plot_count,
		w_clusters_data.sweight_panel_sum
	from sdesign.c_country
	inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
	inner join sdesign.t_stratum on t_strata_set.id = t_stratum.strata_set
	inner join t_cluster_configuration_temp on c_country.label = t_cluster_configuration_temp.country
	inner join '||_schema||'.panels 
		on panels.country = c_country.label
		and panels.strata_set = t_strata_set.strata_set
		and panels.stratum = t_stratum.stratum
		and panels.cluster_configuration = t_cluster_configuration_temp.cluster_configuration
	inner join w_clusters_data
		on panels.country = w_clusters_data.country 
		and panels.strata_set = w_clusters_data.strata_set 
		and panels.stratum = w_clusters_data.stratum 
		and panels.panel = w_clusters_data.panel 
	inner join w_plots_data
		on panels.country = w_plots_data.country 
		and panels.strata_set = w_plots_data.strata_set 
		and panels.stratum = w_plots_data.stratum 
		and panels.panel = w_plots_data.panel 
	order by panels.strata_set, panels.stratum, panels.panel';

	with w_stratum as (
		SELECT
			t_stratum.id,
			t_stratum.stratum,
			t_stratum.geom,
			t_cluster_configuration.cluster_configuration,
			CASE	WHEN t_cluster_configuration.cluster_configuration in ( '2p-r', '2p', '4p' )
				THEN
						st_distance(
							ST_GeometryN(t_cluster_configuration.geom, 1),
							ST_GeometryN(t_cluster_configuration.geom, 2))::integer
			END AS cluster_distance
		FROM sdesign.t_stratum
		INNER JOIN sdesign.t_panel on (t_stratum.id = t_panel.stratum)
		INNER JOIN sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id)
		WHERE t_stratum.buffered_area_m2 IS NULL
	)
	, w_stratum_buff as (
		SELECT
			id,
			stratum,
			CASE 	WHEN cluster_configuration = '2p-r' THEN
					sdesign.fn_create_buffered_geometry(geom, 100, cluster_distance)
				WHEN cluster_configuration = '2p' THEN
					sdesign.fn_create_buffered_geometry(geom, 200, cluster_distance)
				WHEN cluster_configuration = '4p' THEN
					sdesign.fn_create_buffered_geometry(geom, 300, cluster_distance)
				WHEN cluster_configuration = '1p' THEN
					NULL
				ELSE
					NULL --sdesign.fn_create_buffered_geometry(geom, 0, NULL::integer)
			END AS buff_geom
		FROM w_stratum
	)
	, w_stratum_buff_area as (
		SELECT
			id,
			stratum,
			buff_geom AS geom_buffered,
			ST_Area(buff_geom) AS area_m2_buffered
		FROM
			w_stratum_buff
		WHERE
			buff_geom IS NOT NULL
	)
	UPDATE sdesign.t_stratum
	SET
		buffered_area_m2 = w_stratum_buff_area.area_m2_buffered,
		geom_buffered = w_stratum_buff_area.geom_buffered
	FROM w_stratum_buff_area
	WHERE w_stratum_buff_area.id = t_stratum.id;


	-- Data for Name: t_cluster; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE 'CREATE TEMPORARY TABLE t_cluster_temp ON COMMIT DROP AS
		SELECT (select coalesce(max(id),0) from sdesign.t_cluster) + row_number() over() AS id,
			*
		FROM '||_schema||'.clusters';

	-- Data for Name: f_p_plot; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE 'CREATE TEMPORARY TABLE f_p_plot_temp ON COMMIT DROP AS
		SELECT (select coalesce(max(gid),0) from sdesign.f_p_plot) + row_number() over() AS gid,
			*
		FROM '||_schema||'.plots';

	EXECUTE '
	insert into sdesign.t_cluster (id, cluster, comment)
	select id, cluster, comment
	from t_cluster_temp';

	-- reset sequence
	PERFORM setval('sdesign.t_cluster_id_seq', (SELECT coalesce(max(id),0)+1 FROM sdesign.t_cluster), FALSE);

	-- Data for Name: cm_cluster2panel_mapping; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	insert into sdesign.cm_cluster2panel_mapping(panel, cluster, sampling_weight)
	select
		t_panel.id,
		clusters.id,
		clusters.sampling_weight
	from sdesign.c_country
	inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
	inner join sdesign.t_stratum on t_strata_set.id = t_stratum.strata_set
	inner join sdesign.t_panel on t_stratum.id = t_panel.stratum
	inner join t_cluster_temp AS clusters
		on clusters.country = c_country.label
		and clusters.strata_set = t_strata_set.strata_set
		and clusters.stratum = t_stratum.stratum
		and clusters.panel = t_panel.panel';

	-- Data for Name: f_p_plot; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	insert into sdesign.f_p_plot(gid, plot, geom, cluster, coordinates_degraded, comment)
	select
		plots.gid,
		plots.plot,
		ST_GeomFromEWKT(plots.plot_geometry) as geom,
		t_cluster.id,
		plots.coordinates_degraded,
		plots.comment
	from sdesign.c_country
	inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
	inner join sdesign.t_stratum on t_strata_set.id = t_stratum.strata_set
	inner join sdesign.t_panel on t_stratum.id = t_panel.stratum
	inner join sdesign.cm_cluster2panel_mapping on t_panel.id = cm_cluster2panel_mapping.panel
	inner join sdesign.t_cluster on cm_cluster2panel_mapping.cluster = t_cluster.id
	inner join f_p_plot_temp AS plots
		on plots.country = c_country.label
		and plots.strata_set = t_strata_set.strata_set
		and plots.stratum = t_stratum.stratum
		and plots.panel = t_panel.panel
		and plots.cluster = t_cluster.cluster';

	-- reset sequence
	PERFORM setval('sdesign.f_p_plot_gid_seq', (SELECT coalesce(max(gid),0)+1 FROM sdesign.f_p_plot), FALSE);

	-- Data for Name: cm_plot2cluster_config_mapping; Type: TABLE DATA; Schema: sdesign; Owner: vagrant

	insert into sdesign.cm_plot2cluster_config_mapping (cluster_configuration, plot)
	select
		t_cluster_configuration.id,
		plots.gid
	from sdesign.c_country
	inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
	inner join sdesign.t_stratum on t_strata_set.id = t_stratum.strata_set
	inner join sdesign.t_panel on t_stratum.id = t_panel.stratum
	inner join sdesign.t_cluster_configuration on t_panel.cluster_configuration = t_cluster_configuration.id
	inner join sdesign.cm_cluster2panel_mapping on t_panel.id = cm_cluster2panel_mapping.panel
	inner join sdesign.t_cluster on cm_cluster2panel_mapping.cluster = t_cluster.id
	inner join f_p_plot_temp AS plots
		on plots.country = c_country.label
		and plots.strata_set = t_strata_set.strata_set
		and plots.stratum = t_stratum.stratum
		and plots.panel = t_panel.panel
		and plots.cluster = t_cluster.cluster
	group by t_cluster_configuration.id, plots.gid
	order by t_cluster_configuration.id, plots.gid;

	EXECUTE 'CREATE TEMPORARY TABLE t_inventory_campaign_temp ON COMMIT DROP AS
		SELECT (select coalesce(max(id),0) from sdesign.t_inventory_campaign) + row_number() over() AS id,
			*
		FROM '||_schema||'.inventory_campaigns';

	-- Data for Name: t_inventory_campaign; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	insert into sdesign.t_inventory_campaign(id, inventory, label, comment, inventory_campaign_begin, inventory_campaign_end)
	select id, inventory_campaign, label, comment, inventory_campaign_begin, inventory_campaign_end 
	from t_inventory_campaign_temp';

	PERFORM setval('sdesign.t_inventory_campaign_id_seq', (SELECT coalesce(max(id),0)+1 FROM sdesign.t_inventory_campaign), FALSE);

	-- Data for Name: t_reference_year_set; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	insert into sdesign.t_reference_year_set(inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end)
	select
		t_inventory_campaign_temp.id,
		reference_year_set,
		reference_date_begin,
		reference_date_end,
		reference_year_sets.label,
		reference_year_sets.comment,
		reference_year_sets.reference_year_set_begin,
		reference_year_sets.reference_year_set_end
	from sdesign.c_country
	inner join t_inventory_campaign_temp on c_country.label = t_inventory_campaign_temp.country
	inner join '||_schema||'.reference_year_sets
		on reference_year_sets.country = t_inventory_campaign_temp.country
		and reference_year_sets.inventory_campaign = t_inventory_campaign_temp.inventory_campaign
	order by reference_year_set';

	-- Data for Name: t_plot_measurement_dates; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	insert into sdesign.t_plot_measurement_dates(plot, reference_year_set, measurement_date)
	select
		f_p_plot.gid AS plot,
		t_reference_year_set.id,
		measurement_date
	from sdesign.c_country
	inner join t_inventory_campaign_temp on c_country.label = t_inventory_campaign_temp.country
	inner join sdesign.t_reference_year_set on t_inventory_campaign_temp.id = t_reference_year_set.inventory_campaign
	inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
	inner join sdesign.t_stratum on t_strata_set.id = t_stratum.strata_set
	inner join sdesign.t_panel on t_stratum.id = t_panel.stratum
	inner join sdesign.cm_cluster2panel_mapping on t_panel.id = cm_cluster2panel_mapping.panel
	inner join sdesign.t_cluster on cm_cluster2panel_mapping.cluster = t_cluster.id
	inner join sdesign.f_p_plot on t_cluster.id = f_p_plot.cluster
	inner join '||_schema||'.plot_measurement_dates
		on plot_measurement_dates.country = c_country.label
		and plot_measurement_dates.inventory_campaign = t_inventory_campaign_temp.inventory_campaign
		and plot_measurement_dates.reference_year_set = t_reference_year_set.reference_year_set
		and plot_measurement_dates.strata_set = t_strata_set.strata_set
		and plot_measurement_dates.stratum = t_stratum.stratum
		and plot_measurement_dates.panel = t_panel.panel
		and plot_measurement_dates.cluster = t_cluster.cluster
		and plot_measurement_dates.plot = f_p_plot.plot
	order by plot, t_reference_year_set.id';

	-- Data for Name: cm_refyearset2panel_mapping; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
EXECUTE '
	with w_data as (
		select
			t_panel.id as t_panel__id,
			t_reference_year_set.id as t_reference_year_set__id
		from sdesign.c_country
		inner join t_inventory_campaign_temp on c_country.label = t_inventory_campaign_temp.country
		inner join sdesign.t_reference_year_set on t_inventory_campaign_temp.id = t_reference_year_set.inventory_campaign
		inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
		inner join sdesign.t_stratum on t_strata_set.id = t_stratum.strata_set
		inner join sdesign.t_panel on t_stratum.id = t_panel.stratum
		inner join '||_schema||'.refyearset2panel
			on refyearset2panel.country = c_country.label
			and refyearset2panel.inventory_campaign = t_inventory_campaign_temp.inventory_campaign
			and refyearset2panel.reference_year_set = t_reference_year_set.reference_year_set
			and refyearset2panel.strata_set = t_strata_set.strata_set
			and refyearset2panel.stratum = t_stratum.stratum
			and refyearset2panel.panel = t_panel.panel
	)
	insert into sdesign.cm_refyearset2panel_mapping (panel, reference_year_set)
	select t_panel__id, t_reference_year_set__id from w_data
	order by t_panel__id, t_reference_year_set__id;
	';

	-- Name: c_country_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.c_country_id_seq', (select max(id) from sdesign.c_country), true);

	-- Name: cm_cluster2panel_mapping_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.cm_cluster2panel_mapping_id_seq', (select max(id) from sdesign.cm_cluster2panel_mapping), true);

	-- Name: cm_plot2cluster_config_mapping_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.cm_plot2cluster_config_mapping_id_seq', (select max(id) from sdesign.cm_plot2cluster_config_mapping), true);

	-- Name: cm_refyearset2panel_mapping_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.cm_refyearset2panel_mapping_id_seq', (select max(id) from sdesign.cm_refyearset2panel_mapping), true);

	-- Name: f_p_plot_gid_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.f_p_plot_gid_seq', (select max(gid) from sdesign.f_p_plot), true);

	-- Name: t_cluster_configuration_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_cluster_configuration_id_seq', (select max(id) from sdesign.t_cluster_configuration), true);

	-- Name: t_cluster_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_cluster_id_seq', (select max(id) from sdesign.t_cluster), true);

	-- Name: t_inventory_campaign_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_inventory_campaign_id_seq', (select max(id) from sdesign.t_inventory_campaign), true);

	-- Name: t_panel_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_panel_id_seq', (select max(id) from sdesign.t_panel), true);

	-- Name: t_plot_measurement_dates_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_plot_measurement_dates_id_seq', (select max(id) from sdesign.t_plot_measurement_dates), true);

	-- Name: t_reference_year_set_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_reference_year_set_id_seq', (select max(id) from sdesign.t_reference_year_set), true);

	-- Name: t_strata_set_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_strata_set_id_seq', (select max(id) from sdesign.t_strata_set), true);

	-- Name: t_stratum_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_stratum_id_seq', (select max(id) from sdesign.t_stratum), true);

	_res := 'Import is comleted.';

	return _res;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION sdesign.fn_import_data(character varying) IS
'The function imports data from csv tables into tables that are in the sdesign schema.';

-- </function>