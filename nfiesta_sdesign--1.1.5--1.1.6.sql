--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- update of forgotten column in t_panel
UPDATE sdesign.t_panel 
SET sweight_panel_sum = t1.sweight_panel_sum
FROM (SELECT panel, sum(sampling_weight_ha) AS sweight_panel_sum
	FROM sdesign.cm_cluster2panel_mapping
	GROUP BY panel) AS t1
WHERE t_panel.id = t1.panel;


