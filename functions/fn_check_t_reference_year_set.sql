--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE FUNCTION sdesign.fn_check_t_reference_year_set() RETURNS trigger AS $fn_body$
DECLARE
    BEGIN

	--reference_year_set_begin and reference_year_set_end have to be "status" (or NULL -- checked by check constraint)
	IF NOT (
		select begin_reference_year_set_is_status and end_reference_year_set_is_status
		from
		(select status_variables as begin_reference_year_set_is_status
			from sdesign.t_reference_year_set
			where id = NEW.reference_year_set_begin) as begint,
		(select status_variables as end_reference_year_set_is_status
			from sdesign.t_reference_year_set
			where id = NEW.reference_year_set_end) as endt
	) THEN
		RAISE EXCEPTION 'reference_year_set_begin and reference_year_set_end have to be "status" (with NULL reference_year_set_begin/end) ';
	END IF;

	--change refyearset has to reference change inventory campaign and status refyearset has to reference status inventory campaign
	IF NOT (
		SELECT (NEW.status_variables AND inv_is_status) OR (NOT NEW.status_variables AND inv_is_change)
		FROM
			(SELECT status_variables AS inv_is_status,
				NOT status_variables AS inv_is_change
			FROM sdesign.t_inventory_campaign WHERE id = NEW.inventory_campaign) AS inv
	) THEN
		RAISE EXCEPTION 'change refyearset has to reference change inventory campaign and status refyearset has to reference status inventory campaign';
	END IF;

	--reference_year_set_begin and reference_year_set_end must have different inventory campaigns
	IF NOT (
		select begin_inventory_campaign != end_inventory_campaign
	from
		(select inventory_campaign as begin_inventory_campaign
			from sdesign.t_reference_year_set where id = NEW.reference_year_set_begin) as begint,
		(select inventory_campaign as end_inventory_campaign
			from sdesign.t_reference_year_set where id = NEW.reference_year_set_end) as endt
	) THEN
		RAISE EXCEPTION 'reference_year_set_begin and reference_year_set_end must have different inventory campaigns';
	END IF;

	RETURN NEW;
    END;
$fn_body$ LANGUAGE plpgsql;

CREATE CONSTRAINT TRIGGER check_t_reference_year_set
    AFTER INSERT ON sdesign.t_reference_year_set
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW
    EXECUTE FUNCTION sdesign.fn_check_t_reference_year_set();
