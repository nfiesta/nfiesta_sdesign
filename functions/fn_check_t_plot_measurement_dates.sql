--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE FUNCTION sdesign.fn_check_t_plot_measurement_dates() RETURNS trigger AS $fn_body$
    DECLARE
    BEGIN
        --t_plot_measurement_dates should not contain references to chenge reference_year_sets
        IF NOT (
	    select count(*) = 0
	    from sdesign.t_reference_year_set
	    where 
	    	t_reference_year_set.id = NEW.reference_year_set
		and (reference_year_set_begin is not null
		or reference_year_set_end is not null)
        ) THEN
            RAISE EXCEPTION 't_plot_measurement_dates should not contain references to chenge reference_year_sets';
        END IF;

	RETURN NEW;
    END;
$fn_body$ LANGUAGE plpgsql;

CREATE TRIGGER check_t_plot_measurement_dates
    BEFORE INSERT ON sdesign.t_plot_measurement_dates
    FOR EACH ROW
    EXECUTE FUNCTION sdesign.fn_check_t_plot_measurement_dates();
