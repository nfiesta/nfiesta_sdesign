CREATE OR REPLACE FUNCTION sdesign.fn_stratum_clusterconf_comb()
  RETURNS trigger AS
$BODY$
    BEGIN
	IF not (
	select 
	cardinality(array_agg(distinct stratum)) = 1 
from sdesign.t_panel
where cluster_configuration = new.cluster_configuration
group by cluster_configuration) THEN
	        RAISE EXCEPTION 'fn_stratum_clusterconf_comb -- INS / UPD -- it is not possible to link one cluster_configuration to multiple strata (cluster_configuration = % to be used in strata % (stratum % is being inserted or updated)).', new.cluster_configuration, (select array_agg(distinct stratum) from sdesign.t_panel where cluster_configuration = new.cluster_configuration), new.stratum;
	END IF;
	RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

drop trigger if exists trg__stratum_clusterconf_comb ON sdesign.t_panel;
CREATE TRIGGER trg__stratum_clusterconf_comb
AFTER INSERT OR UPDATE
ON sdesign.t_panel
FOR EACH ROW
EXECUTE PROCEDURE sdesign.fn_stratum_clusterconf_comb();
