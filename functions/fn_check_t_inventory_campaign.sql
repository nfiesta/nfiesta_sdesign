--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE FUNCTION sdesign.fn_check_t_inventory_campaign() RETURNS trigger AS $fn_body$
DECLARE
    BEGIN
	--both inventory_campaign_begin and inventory_campaign_end have to be NULL or NOT NULL
        IF ((NEW.inventory_campaign_begin IS NULL AND NEW.inventory_campaign_end IS NOT NULL) OR
            (NEW.inventory_campaign_begin IS NOT NULL AND NEW.inventory_campaign_end IS NULL)
        )  THEN
            RAISE EXCEPTION 'both inventory_campaign_begin and inventory_campaign_end have to be NULL or NOT NULL';
        END IF;

	--inventory_campaign_begin and inventory_campaign_end have to be different
        IF (NEW.inventory_campaign_begin = NEW.inventory_campaign_end) THEN
            RAISE EXCEPTION 'inventory_campaign_begin and inventory_campaign_end have to be different';
        END IF;

        --inventory_campaign_begin and inventory_campaign_end have to be "state"
        IF NOT (
            select begin_campaign_is_state and end_campaign_is_state
            from 
                (select inventory_campaign_begin is NULL and inventory_campaign_end is NULL as begin_campaign_is_state
                    from sdesign.t_inventory_campaign where id = NEW.inventory_campaign_begin) as begint,
                (select inventory_campaign_begin is NULL and inventory_campaign_end is NULL as end_campaign_is_state
                    from sdesign.t_inventory_campaign where id = NEW.inventory_campaign_end) as endt
        ) THEN
            RAISE EXCEPTION 'referenced campaigns have to be "state" (with NULL inventory_campaign_begin/end) ';
        END IF;

	RETURN NEW;
    END;
$fn_body$ LANGUAGE plpgsql;

CREATE TRIGGER check_t_inventory_campaign
    BEFORE INSERT ON sdesign.t_inventory_campaign
    FOR EACH ROW
    EXECUTE FUNCTION sdesign.fn_check_t_inventory_campaign();
