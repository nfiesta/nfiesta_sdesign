--
-- Copyright 2021, 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE FUNCTION sdesign.fn_check_cm_refyearset2panel_mapping() RETURNS trigger AS $fn_body$
    DECLARE
        _refyearset_is_change		boolean;
	_status_panels			boolean;
	_panel_in_refyearset_begin	boolean;
	_panel_in_refyearset_end	boolean;
    BEGIN
  	-- panels belonging to change refyearset and also into the state reference year sets have to be contained in CORRESPONDING state refyearsets
	select 
		(reference_year_set_begin is not null and reference_year_set_end is not null) 
	into _refyearset_is_change
	from sdesign.t_reference_year_set 
	where id = NEW.reference_year_set;

	IF _refyearset_is_change THEN
		-- find out if the panel has mapping to state reference year sets too
		_status_panels := 
			(EXISTS (SELECT t1.id
			FROM sdesign.cm_refyearset2panel_mapping AS t1
			INNER JOIN sdesign.t_reference_year_set AS t2 ON t1.reference_year_set = t2.id
			WHERE t1.panel = NEW.panel AND t2.status_variables = true));

		IF _status_panels
		THEN
			-- and if so, the state reference year sets has to the ones which are referenced from the change refyearset
			select 
				count(*) = 1 
			into _panel_in_refyearset_begin
			from sdesign.cm_refyearset2panel_mapping 
			inner join sdesign.t_reference_year_set as state_refyearset on (cm_refyearset2panel_mapping.reference_year_set = state_refyearset.id)
			inner join sdesign.t_reference_year_set as change_refyearset on (change_refyearset.reference_year_set_begin = state_refyearset.id)
			where change_refyearset.id = NEW.reference_year_set and cm_refyearset2panel_mapping.panel = NEW.panel;

			select
				count(*) = 1 
			into _panel_in_refyearset_end
			from sdesign.cm_refyearset2panel_mapping 
			inner join sdesign.t_reference_year_set as state_refyearset on (cm_refyearset2panel_mapping.reference_year_set = state_refyearset.id)
			inner join sdesign.t_reference_year_set as change_refyearset on (change_refyearset.reference_year_set_end = state_refyearset.id)
			where change_refyearset.id = NEW.reference_year_set and cm_refyearset2panel_mapping.panel = NEW.panel;

			IF NOT (
			    _panel_in_refyearset_begin and _panel_in_refyearset_end
			) THEN
			    RAISE EXCEPTION 'If the panel belongs to the change refeyerence_year_set and to state reference year sets at the same time, the state reference year sets must correspond to the particular change reference year set
	(panel: %, reference_year_set: %, refyearset_is_change: %, panel_in_refyearset_begin %, panel_in_refyearset_end: %)', 
				NEW.panel, NEW.reference_year_set, _refyearset_is_change, _panel_in_refyearset_begin, _panel_in_refyearset_end;
			END IF;
		END IF;
	END IF;
	RETURN NEW;
    END;
$fn_body$ LANGUAGE plpgsql;

CREATE TRIGGER check_cm_refyearset2panel_mapping_ins
	BEFORE INSERT ON sdesign.cm_refyearset2panel_mapping
	FOR EACH ROW
	EXECUTE FUNCTION sdesign.fn_check_cm_refyearset2panel_mapping();

CREATE TRIGGER check_cm_refyearset2panel_mapping_upd
	BEFORE UPDATE ON sdesign.cm_refyearset2panel_mapping
	FOR EACH ROW
	EXECUTE FUNCTION sdesign.fn_check_cm_refyearset2panel_mapping();

