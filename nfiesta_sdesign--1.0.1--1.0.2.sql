--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

ALTER TABLE sdesign.c_country
ADD COLUMN label_en character varying(100);

COMMENT ON COLUMN sdesign.c_country.label_en IS 'Label (code) of the country.';

ALTER TABLE sdesign.c_country
ADD COLUMN description_en text;

COMMENT ON COLUMN sdesign.c_country.description_en IS 'Description (name) of the country.';
