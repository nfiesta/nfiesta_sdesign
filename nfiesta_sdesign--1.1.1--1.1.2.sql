--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

drop FUNCTION sdesign.fn_check_t_inventory_campaign() cascade;
-- <function name="fn_check_t_inventory_campaign" schema="sdesign" src="functions/fn_check_t_inventory_campaign.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE FUNCTION sdesign.fn_check_t_inventory_campaign() RETURNS trigger AS $fn_body$
DECLARE
    BEGIN
	--both inventory_campaign_begin and inventory_campaign_end have to be NULL or NOT NULL
        IF ((NEW.inventory_campaign_begin IS NULL AND NEW.inventory_campaign_end IS NOT NULL) OR
            (NEW.inventory_campaign_begin IS NOT NULL AND NEW.inventory_campaign_end IS NULL)
        )  THEN
            RAISE EXCEPTION 'both inventory_campaign_begin and inventory_campaign_end have to be NULL or NOT NULL';
        END IF;

	--inventory_campaign_begin and inventory_campaign_end have to be different
        IF (NEW.inventory_campaign_begin = NEW.inventory_campaign_end) THEN
            RAISE EXCEPTION 'inventory_campaign_begin and inventory_campaign_end have to be different';
        END IF;

        --inventory_campaign_begin and inventory_campaign_end have to be "state"
        IF NOT (
            select begin_campaign_is_state and end_campaign_is_state
            from 
                (select inventory_campaign_begin is NULL and inventory_campaign_end is NULL as begin_campaign_is_state
                    from sdesign.t_inventory_campaign where id = NEW.inventory_campaign_begin) as begint,
                (select inventory_campaign_begin is NULL and inventory_campaign_end is NULL as end_campaign_is_state
                    from sdesign.t_inventory_campaign where id = NEW.inventory_campaign_end) as endt
        ) THEN
            RAISE EXCEPTION 'referenced campaigns have to be "state" (with NULL inventory_campaign_begin/end) ';
        END IF;

	RETURN NEW;
    END;
$fn_body$ LANGUAGE plpgsql;

CREATE TRIGGER check_t_inventory_campaign
    BEFORE INSERT ON sdesign.t_inventory_campaign
    FOR EACH ROW
    EXECUTE FUNCTION sdesign.fn_check_t_inventory_campaign();

-- </function>

drop FUNCTION sdesign.fn_check_t_reference_year_set() cascade;
-- <function name="fn_check_t_reference_year_set.sql" schema="sdesign" src="functions/fn_check_t_reference_year_set.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE FUNCTION sdesign.fn_check_t_reference_year_set() RETURNS trigger AS $fn_body$
DECLARE
    BEGIN
	--both reference_year_set_begin and reference_year_set_end have to be NULL or NOT NULL
        IF ((NEW.reference_year_set_begin IS NULL AND NEW.reference_year_set_end IS NOT NULL) OR
            (NEW.reference_year_set_begin IS NOT NULL AND NEW.reference_year_set_end IS NULL)
        )  THEN
            RAISE EXCEPTION 'both reference_year_set_begin and reference_year_set_end have to be NULL or NOT NULL';
        END IF;

	--reference_year_set_begin and reference_year_set_end have to be different
        IF (NEW.reference_year_set_begin = NEW.reference_year_set_end) THEN
            RAISE EXCEPTION 'reference_year_set_begin and reference_year_set_end have to be different';
        END IF;

        --reference_year_set_begin and reference_year_set_end have to be "state"
        IF NOT (
            select begin_reference_year_set_is_state and end_reference_year_set_is_state
            from 
                (select reference_year_set_begin is NULL and reference_year_set_end is NULL as begin_reference_year_set_is_state
                    from sdesign.t_reference_year_set where id = NEW.reference_year_set_begin) as begint,
                (select reference_year_set_begin is NULL and reference_year_set_end is NULL as end_reference_year_set_is_state
                    from sdesign.t_reference_year_set where id = NEW.reference_year_set_end) as endt
        ) THEN
            RAISE EXCEPTION 'reference_year_set_begin and reference_year_set_end have to be "state" (with NULL reference_year_set_begin/end) ';
        END IF;
	
	--change refyearset has to reference change inventory campaign and state refyearset has to reference state inventory campaign
        IF NOT (
            select ((NEW.reference_year_set_begin IS NULL AND NEW.reference_year_set_end IS NULL) AND inv_is_state) OR
                   ((NEW.reference_year_set_begin IS NOT NULL AND NEW.reference_year_set_end IS NOT NULL) AND inv_is_change)
            from 
                (select inventory_campaign_begin IS NULL AND inventory_campaign_end IS NULL as inv_is_state,
                        inventory_campaign_begin IS NOT NULL AND inventory_campaign_end IS NOT NULL as inv_is_change
                    from sdesign.t_inventory_campaign where id = NEW.inventory_campaign) as inv
        ) THEN
            RAISE EXCEPTION 'change refyearset has to reference change inventory campaign and state refyearset has to reference state inventory campaign';
        END IF;
	
        --reference_year_set_begin and reference_year_set_end must have different inventory campaigns
        IF NOT (
            select begin_inventory_campaign != end_inventory_campaign
            from 
                (select inventory_campaign as begin_inventory_campaign
                    from sdesign.t_reference_year_set where id = NEW.reference_year_set_begin) as begint,
                (select inventory_campaign as end_inventory_campaign
                    from sdesign.t_reference_year_set where id = NEW.reference_year_set_end) as endt
        ) THEN
            RAISE EXCEPTION 'reference_year_set_begin and reference_year_set_end must have different inventory campaigns';
        END IF;

        --reference_year_set_begin and reference_year_set_end must have same begin and end dates as reference_date_begin and reference_date_end
        IF NOT (
            select NEW.reference_date_begin = begin_reference_date_begin and NEW.reference_date_end = end_reference_date_end
            from 
                (select reference_date_begin as begin_reference_date_begin
                    from sdesign.t_reference_year_set where id = NEW.reference_year_set_begin) as begint,
                (select reference_date_end as end_reference_date_end
                    from sdesign.t_reference_year_set where id = NEW.reference_year_set_end) as endt
        ) THEN
		RAISE EXCEPTION 'reference_year_set_begin and reference_year_set_end must have same begin and end dates as reference_date_begin and reference_date_end';
        END IF;

  	--reference_year_set_begin and reference_year_set_end panels have to overlap (have any elements in common)
        IF NOT (
            select panel_begin && panel_end
            from
                (select array_agg(panel order by panel) as panel_begin
                    from sdesign.cm_refyearset2panel_mapping where reference_year_set = NEW.reference_year_set_begin) as begint,
                (select array_agg(panel order by panel) as panel_end
                    from sdesign.cm_refyearset2panel_mapping where reference_year_set = NEW.reference_year_set_end) as endt
        ) THEN
            RAISE EXCEPTION 'reference_year_set_begin and reference_year_set_end panels have to overlap (have any elements in common)';
        END IF;

	--inventory campaign (state) from state reference_year_sets has to correspond to inventory campaign (state) referenced from change inventory campaign
	IF NOT (
            select (begint.inventory_campaign = inv.inventory_campaign_begin) and (endt.inventory_campaign = inv.inventory_campaign_end)
            from
                (select inventory_campaign
                    from sdesign.t_reference_year_set where id = NEW.reference_year_set_begin) as begint,
                (select inventory_campaign
                    from sdesign.t_reference_year_set where id = NEW.reference_year_set_end) as endt,
                (select inventory_campaign_begin, inventory_campaign_end
                    from sdesign.t_inventory_campaign where id = NEW.inventory_campaign) as inv
        ) THEN
            RAISE EXCEPTION 'inventory campaign (state) from state reference_year_sets has to correspond to inventory campaign (state) referenced from change inventory campaign';
        END IF;

	RETURN NEW;
    END;
$fn_body$ LANGUAGE plpgsql;

CREATE TRIGGER check_t_reference_year_set
    BEFORE INSERT ON sdesign.t_reference_year_set
    FOR EACH ROW
    EXECUTE FUNCTION sdesign.fn_check_t_reference_year_set();

-- </function>

--drop FUNCTION sdesign.fn_check_t_inventory_campaign() cascade;

-- <function name="fn_check_cm_refyearset2panel_mapping" schema="sdesign" src="functions/fn_check_cm_refyearset2panel_mapping.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE FUNCTION sdesign.fn_check_cm_refyearset2panel_mapping() RETURNS trigger AS $fn_body$
    DECLARE
        refyearset_is_change		boolean;
	panel_in_refyearset_begin	boolean;
	panel_in_refyearset_end		boolean;
    BEGIN
  	--panels belonging to change refyearset have to be contained in corresponding state refyearsets
	select 
		(reference_year_set_begin is not null and reference_year_set_end is not null) into refyearset_is_change
	from sdesign.t_reference_year_set 
	where id = NEW.reference_year_set;

	IF refyearset_is_change THEN
	        select 
			count(*) = 1 into panel_in_refyearset_begin
	        from sdesign.cm_refyearset2panel_mapping 
		inner join sdesign.t_reference_year_set as state_rys on (cm_refyearset2panel_mapping.reference_year_set = state_rys.id)
		inner join sdesign.t_reference_year_set as change_rys on (change_rys.reference_year_set_begin = state_rys.id)
		where change_rys.id = NEW.reference_year_set and cm_refyearset2panel_mapping.panel = NEW.panel;

		select
			count(*) = 1 into panel_in_refyearset_end
	        from sdesign.cm_refyearset2panel_mapping 
		inner join sdesign.t_reference_year_set as state_rys on (cm_refyearset2panel_mapping.reference_year_set = state_rys.id)
		inner join sdesign.t_reference_year_set as change_rys on (change_rys.reference_year_set_end = state_rys.id)
		where change_rys.id = NEW.reference_year_set and cm_refyearset2panel_mapping.panel = NEW.panel;

	        IF NOT (
	            panel_in_refyearset_begin and panel_in_refyearset_end
	        ) THEN
	            RAISE EXCEPTION 'panels belonging to change refyearset have to be contained in corresponding state refyearsets
(panel: %, reference_year_set: %, refyearset_is_change: %, panel_in_refyearset_begin %, panel_in_refyearset_end: %)', 
			NEW.panel, NEW.reference_year_set, refyearset_is_change, panel_in_refyearset_begin, panel_in_refyearset_end;
        	END IF;
	END IF;
	RETURN NEW;
    END;
$fn_body$ LANGUAGE plpgsql;

CREATE TRIGGER check_cm_refyearset2panel_mapping
    BEFORE INSERT ON sdesign.cm_refyearset2panel_mapping
    FOR EACH ROW
    EXECUTE FUNCTION sdesign.fn_check_cm_refyearset2panel_mapping();

-- </function>

-- <function name="fn_check_t_plot_measurement_dates" schema="sdesign" src="functions/fn_check_t_plot_measurement_dates.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE FUNCTION sdesign.fn_check_t_plot_measurement_dates() RETURNS trigger AS $fn_body$
    DECLARE
    BEGIN
        --t_plot_measurement_dates should not contain references to chenge reference_year_sets
        IF NOT (
	    select count(*) = 0
	    from sdesign.t_reference_year_set
	    where 
	    	t_reference_year_set.id = NEW.reference_year_set
		and (reference_year_set_begin is not null
		or reference_year_set_end is not null)
        ) THEN
            RAISE EXCEPTION 't_plot_measurement_dates should not contain references to chenge reference_year_sets';
        END IF;

	RETURN NEW;
    END;
$fn_body$ LANGUAGE plpgsql;

CREATE TRIGGER check_t_plot_measurement_dates
    BEFORE INSERT ON sdesign.t_plot_measurement_dates
    FOR EACH ROW
    EXECUTE FUNCTION sdesign.fn_check_t_plot_measurement_dates();

-- </function>
