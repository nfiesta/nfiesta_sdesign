#nfiesta_sdesign/Makefile

EXTENSION = nfiesta_sdesign # name of extension
MODULEDIR = nfiesta_sdesign_dir

DATA =  nfiesta_sdesign--1.0.0.sql \
	nfiesta_sdesign--1.0.0--1.0.1.sql \
	nfiesta_sdesign--1.0.1--1.0.2.sql \
	nfiesta_sdesign--1.0.2--1.0.3.sql \
	nfiesta_sdesign--1.0.3--1.0.4.sql \
	nfiesta_sdesign--1.0.4--1.0.5.sql \
	nfiesta_sdesign--1.0.5--1.1.0.sql \
	nfiesta_sdesign--1.1.0--1.1.1.sql \
	nfiesta_sdesign--1.1.1--1.1.2.sql \
	nfiesta_sdesign--1.1.2--1.1.3.sql \
	nfiesta_sdesign--1.1.3--1.1.4.sql \
	nfiesta_sdesign--1.1.4--1.1.5.sql \
	nfiesta_sdesign--1.1.5--1.1.6.sql \
	nfiesta_sdesign--1.1.6--1.1.7.sql \
	nfiesta_sdesign--1.1.7--1.1.8.sql \
	nfiesta_sdesign--1.1.8--1.1.9.sql \
	nfiesta_sdesign--1.1.9--1.1.10.sql \
	nfiesta_sdesign--1.1.10--1.1.11.sql \
	nfiesta_sdesign--1.1.11--1.1.12.sql \
	nfiesta_sdesign--1.1.12--1.1.13.sql \
	nfiesta_sdesign--1.1.13--1.1.14.sql \
	nfiesta_sdesign--1.1.14--1.1.15.sql


REGRESS = 	install

export SRC_DIR := $(shell pwd)

installcheck-all:
	make installcheck REGRESS_OPTS="--dbname=contrib_regression_sdesign"
	make installcheck-data
	make installcheck4nfiesta
	make csv-export

installcheck-data:
	psql -d postgres  -c "drop database if exists contrib_regression_sdesign_data;" -c "create database contrib_regression_sdesign_data template contrib_regression_sdesign;"
	make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_sdesign_data" REGRESS="csv fn_import_data check_data check_triggers"

installcheck4nfiesta: 
	psql -d postgres  -c "drop database if exists contrib_regression_sdesign4nfiesta;" -c "create database contrib_regression_sdesign4nfiesta template contrib_regression_sdesign;"
	make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_sdesign4nfiesta" REGRESS="sdesign4nfiesta/csv sdesign4nfiesta/fn_import_data sdesign4nfiesta/check_data sdesign4nfiesta/check_triggers"

csv-export:
	psql --quiet --no-psqlrc contrib_regression_sdesign_data -f sql/csv/create_csv.sql
	git diff --quiet sql/csv/*.csv

purge:
	rm -rf /usr/share/postgresql/12/extension/nfiesta_sdesign.control /usr/share/postgresql/12/nfiesta_sdesign_dir

# postgres build
PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
