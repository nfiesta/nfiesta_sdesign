--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

ALTER TABLE sdesign.t_stratum RENAME COLUMN area_m2 TO area_ha;
ALTER TABLE sdesign.t_stratum RENAME COLUMN buffered_area_m2 TO frame_area_ha;
ALTER TABLE sdesign.cm_cluster2panel_mapping RENAME COLUMN sampling_weight TO sampling_weight_ha;
ALTER TABLE sdesign.cm_cluster2panel_mapping ALTER COLUMN sampling_weight_ha TYPE double precision USING sampling_weight_ha::double precision;
COMMENT ON COLUMN sdesign.cm_cluster2panel_mapping.sampling_weight_ha IS 'Area of the sampling frame represented by the particular cluster.';

ALTER TABLE sdesign.t_inventory_campaign DROP COLUMN inventory_campaign_begin;
ALTER TABLE sdesign.t_inventory_campaign DROP COLUMN inventory_campaign_end;
ALTER TABLE sdesign.t_inventory_campaign RENAME COLUMN inventory TO inventory_campaign;
DROP TRIGGER check_t_inventory_campaign ON sdesign.t_inventory_campaign;

ALTER TABLE sdesign.t_inventory_campaign ADD COLUMN status_variables boolean;
ALTER TABLE sdesign.t_reference_year_set ADD COLUMN status_variables boolean;

ALTER TABLE sdesign.t_plot_measurement_dates ADD COLUMN comment text;
ALTER TABLE sdesign.cm_refyearset2panel_mapping ADD COLUMN comment text;

COMMENT ON COLUMN sdesign.t_inventory_campaign.status_variables IS 'Indication of type of target variables which may refer to inventory campaign: true for variables of current status, false for dynamic and change variables.';
COMMENT ON COLUMN sdesign.t_reference_year_set.status_variables IS 'Indication of type of target variables which may refer to reference year set: true for variables of current status, false for dynamic and change variables.';
COMMENT ON COLUMN sdesign.t_plot_measurement_dates.comment IS 'Optional comment.';
COMMENT ON COLUMN sdesign.cm_refyearset2panel_mapping.comment IS 'Optional comment.';


ALTER TABLE sdesign.t_reference_year_set ALTER COLUMN reference_date_begin DROP NOT NULL;
ALTER TABLE sdesign.t_reference_year_set ALTER COLUMN reference_date_end DROP NOT NULL;

-- update of dynamic data
UPDATE sdesign.t_stratum SET area_ha = area_ha/10000.0;
UPDATE sdesign.t_stratum SET frame_area_ha = frame_area_ha/10000.0 WHERE frame_area_ha != -999.999;

-- the _ha variant of sampling weight is the desired and final
UPDATE sdesign.cm_cluster2panel_mapping 
SET sampling_weight_ha = CASE WHEN t3.cluster_design AND t2.frame_area_ha != -999.999 THEN t2.frame_area_ha/t1.cluster_count::double precision 
	ELSE t2.area_ha/t1.cluster_count::double precision END
FROM sdesign.t_panel AS t1
INNER JOIN sdesign.t_stratum AS t2
ON t1.stratum = t2.id
INNER JOIN sdesign.t_cluster_configuration AS t3
ON t1.cluster_configuration = t3.id
WHERE cm_cluster2panel_mapping.panel = t1.id;

UPDATE sdesign.t_reference_year_set SET status_variables = false 
WHERE reference_year_set_begin IS NOT NULL AND reference_year_set_end IS NOT NULL;

UPDATE sdesign.t_reference_year_set SET status_variables = true
WHERE reference_year_set_begin IS NULL AND reference_year_set_end IS NULL;

UPDATE sdesign.t_reference_year_set SET reference_date_begin = NULL, reference_date_end = NULL
WHERE status_variables = false;

UPDATE sdesign.t_inventory_campaign SET status_variables = t1.status_variables
FROM 	(SELECT distinct status_variables, inventory_campaign
	FROM sdesign.t_reference_year_set) AS t1
WHERE t_inventory_campaign.id = t1.inventory_campaign;

ALTER TABLE sdesign.t_inventory_campaign ALTER COLUMN status_variables SET NOT NULL;
ALTER TABLE sdesign.t_reference_year_set ALTER COLUMN status_variables SET NOT NULL;

CREATE INDEX t_plot_measurement_dates_plot_idx ON sdesign.t_plot_measurement_dates (plot);

-- check constraint
alter table sdesign.t_reference_year_set ADD CONSTRAINT chck_reference_date_begin_end check (
	case when status_variables = true then (
		reference_date_begin 	is not null and
		reference_date_end 	is not null and
		(reference_date_begin < reference_date_end)
	)
	else (reference_date_begin is null and reference_date_end is null)
end);

alter table sdesign.t_reference_year_set ADD CONSTRAINT chck_reference_year_set_begin_end check (
	case when status_variables = false then (
		reference_year_set_begin 	is not null and
		reference_year_set_end 		is not null and
		(reference_year_set_begin != reference_year_set_end)
	)
	else (reference_year_set_begin is null and reference_year_set_end is null)
end);

DROP TRIGGER check_cm_refyearset2panel_mapping ON sdesign.cm_refyearset2panel_mapping;
DROP FUNCTION sdesign.fn_check_cm_refyearset2panel_mapping();

-- functions
DROP FUNCTION sdesign.fn_check_t_reference_year_set() CASCADE;

-- <function name="fn_check_t_reference_year_set" schema="analytical" src="functions/fn_check_t_reference_year_set.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE FUNCTION sdesign.fn_check_t_reference_year_set() RETURNS trigger AS $fn_body$
DECLARE
    BEGIN

	--reference_year_set_begin and reference_year_set_end have to be "status" (or NULL -- checked by check constraint)
	IF NOT (
		select begin_reference_year_set_is_status and end_reference_year_set_is_status
		from
		(select status_variables as begin_reference_year_set_is_status
			from sdesign.t_reference_year_set
			where id = NEW.reference_year_set_begin) as begint,
		(select status_variables as end_reference_year_set_is_status
			from sdesign.t_reference_year_set
			where id = NEW.reference_year_set_end) as endt
	) THEN
		RAISE EXCEPTION 'reference_year_set_begin and reference_year_set_end have to be "status" (with NULL reference_year_set_begin/end) ';
	END IF;

	--change refyearset has to reference change inventory campaign and status refyearset has to reference status inventory campaign
	IF NOT (
		SELECT (NEW.status_variables AND inv_is_status) OR (NOT NEW.status_variables AND inv_is_change)
		FROM
			(SELECT status_variables AS inv_is_status,
				NOT status_variables AS inv_is_change
			FROM sdesign.t_inventory_campaign WHERE id = NEW.inventory_campaign) AS inv
	) THEN
		RAISE EXCEPTION 'change refyearset has to reference change inventory campaign and status refyearset has to reference status inventory campaign';
	END IF;

	--reference_year_set_begin and reference_year_set_end must have different inventory campaigns
	IF NOT (
		select begin_inventory_campaign != end_inventory_campaign
	from
		(select inventory_campaign as begin_inventory_campaign
			from sdesign.t_reference_year_set where id = NEW.reference_year_set_begin) as begint,
		(select inventory_campaign as end_inventory_campaign
			from sdesign.t_reference_year_set where id = NEW.reference_year_set_end) as endt
	) THEN
		RAISE EXCEPTION 'reference_year_set_begin and reference_year_set_end must have different inventory campaigns';
	END IF;

	RETURN NEW;
    END;
$fn_body$ LANGUAGE plpgsql;

CREATE CONSTRAINT TRIGGER check_t_reference_year_set
    AFTER INSERT ON sdesign.t_reference_year_set
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW
    EXECUTE FUNCTION sdesign.fn_check_t_reference_year_set();

-- </function>

-- <function name="fn_import_data" schema="sdesign" src="functions/fn_import_data.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--
---------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- fn_import_data
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS sdesign.fn_import_data(character varying) CASCADE;

CREATE OR REPLACE FUNCTION sdesign.fn_import_data(_schema character varying)
RETURNS text
AS
$$
DECLARE
		_res	text;
BEGIN
	EXECUTE 'CREATE TEMPORARY TABLE t_cluster_configuration_temp ON COMMIT DROP AS
		SELECT (select coalesce(max(id),0) from sdesign.t_cluster_configuration) + row_number() over() AS id,
			*
		FROM '||_schema||'.cluster_configurations';

	--set client_min_messages = DEBUG1;
	RAISE DEBUG '% t_cluster_configuration_temp', clock_timestamp();

	-- Data for Name: t_cluster_configuration; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE'
	insert into sdesign.t_cluster_configuration(id, cluster_configuration, cluster_design, cluster_rotation, geom, plots_per_cluster, label, comment)
	select
		id,
		cluster_configuration,
		cluster_design,
		cluster_rotation,
		ST_GeomFromText(cluster_geom)::geometry(MULTIPOINT) as geom,
		case when cluster_design then
			ST_NumGeometries(ST_GeomFromText(cluster_geom)::geometry(MultiPoint))
		else
			1
		end as plots_per_cluster,
		label,
		comment
	from t_cluster_configuration_temp';

	RAISE DEBUG '% insert into sdesign.t_cluster_configuration', clock_timestamp();

	PERFORM setval('sdesign.t_cluster_configuration_id_seq', (SELECT coalesce(max(id),0)+1 FROM sdesign.t_cluster_configuration), FALSE);

	-- Data for Name: t_strata_set; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE 'insert into sdesign.t_strata_set(country, strata_set, label, comment)
	select c_country.id, strata_sets.strata_set, strata_sets.label, strata_sets.comment 
	from sdesign.c_country
	inner join '||_schema||'.strata_sets
	on strata_sets.country = c_country.label
	order by country, strata_set';

	RAISE DEBUG '% insert into sdesign.t_strata_set', clock_timestamp();
	-- Data for Name: t_stratum; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	insert into sdesign.t_stratum(stratum, area_ha, frame_area_ha, comment, geom, strata_set, label)
	select
		strata.stratum,
		coalesce(strata.area_ha, ST_Area(ST_GeomFromEWKT(strata.geometry))/10000.0) as area_ha,
		strata.frame_area_ha,
		strata.comment,
		ST_GeomFromEWKT(strata.geometry) as geom,
		t_strata_set.id as strata_set,
		strata.label
	from sdesign.c_country
	inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
	inner join '||_schema||'.strata	
		on strata.strata_set = t_strata_set.strata_set
		and strata.country = c_country.label';

	RAISE DEBUG '% insert into sdesign.t_stratum', clock_timestamp();
	-- Data for Name: t_panel; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	with w_clusters_data as (
		select country, strata_set, stratum, panel, count(*) as cluster_count, sum(sampling_weight_ha) as sweight_panel_sum
		from '||_schema||'.clusters
		group by country, strata_set, stratum, panel order by panel
	)
	, w_plots_data as (
		select country, strata_set, stratum, panel, count(*) as plot_count
		from '||_schema||'.plots
		group by country, strata_set, stratum, panel order by panel
	)
	insert into sdesign.t_panel(stratum, panel, cluster_configuration, label, comment, cluster_count, plot_count, sweight_panel_sum)
	select
		t_stratum.id as stratum,
		panels.panel,
		t_cluster_configuration_temp.id as cluster_configuration,
		panels.label,
		panels.comment,
		w_clusters_data.cluster_count,
		w_plots_data.plot_count,
		w_clusters_data.sweight_panel_sum
	from sdesign.c_country
	inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
	inner join sdesign.t_stratum on t_strata_set.id = t_stratum.strata_set
	inner join t_cluster_configuration_temp on c_country.label = t_cluster_configuration_temp.country
	inner join '||_schema||'.panels
		on panels.country = c_country.label
		and panels.strata_set = t_strata_set.strata_set
		and panels.stratum = t_stratum.stratum
		and panels.cluster_configuration = t_cluster_configuration_temp.cluster_configuration
	inner join w_clusters_data
		on panels.country = w_clusters_data.country
		and panels.strata_set = w_clusters_data.strata_set
		and panels.stratum = w_clusters_data.stratum
		and panels.panel = w_clusters_data.panel
	inner join w_plots_data
		on panels.country = w_plots_data.country
		and panels.strata_set = w_plots_data.strata_set
		and panels.stratum = w_plots_data.stratum
		and panels.panel = w_plots_data.panel
	order by panels.strata_set, panels.stratum, panels.panel';
	
	RAISE DEBUG '% insert into sdesign.t_panel', clock_timestamp();

	with w_stratum as (
		SELECT
			t_stratum.id,
			t_stratum.stratum,
			t_stratum.geom,
			t_cluster_configuration.cluster_configuration,
			CASE	WHEN t_cluster_configuration.cluster_configuration in ( '2p-r', '2p', '4p' )
				THEN
						st_distance(
							ST_GeometryN(t_cluster_configuration.geom, 1),
							ST_GeometryN(t_cluster_configuration.geom, 2))::integer
			END AS cluster_distance
		FROM sdesign.t_stratum
		INNER JOIN sdesign.t_panel on (t_stratum.id = t_panel.stratum)
		INNER JOIN sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id)
		WHERE t_stratum.frame_area_ha IS NULL
	)
	, w_stratum_buff as (
		SELECT
			id,
			stratum,
			CASE 	WHEN cluster_configuration = '2p-r' THEN
					sdesign.fn_create_buffered_geometry(geom, 100, cluster_distance)
				WHEN cluster_configuration = '2p' THEN
					sdesign.fn_create_buffered_geometry(geom, 200, cluster_distance)
				WHEN cluster_configuration = '4p' THEN
					sdesign.fn_create_buffered_geometry(geom, 300, cluster_distance)
				WHEN cluster_configuration = '1p' THEN
					NULL
				ELSE
					NULL --sdesign.fn_create_buffered_geometry(geom, 0, NULL::integer)
			END AS buff_geom
		FROM w_stratum
	)
	, w_stratum_buff_area as (
		SELECT
			id,
			stratum,
			buff_geom AS geom_buffered,
			ST_Area(buff_geom)/10000.0 AS area_ha_buffered
		FROM
			w_stratum_buff
		WHERE
			buff_geom IS NOT NULL
	)
	UPDATE sdesign.t_stratum
	SET
		frame_area_ha = w_stratum_buff_area.area_ha_buffered,
		geom_buffered = w_stratum_buff_area.geom_buffered
	FROM w_stratum_buff_area
	WHERE w_stratum_buff_area.id = t_stratum.id;

	RAISE DEBUG '% UPDATE sdesign.t_stratum (frame_area_ha, geom_buffered)', clock_timestamp();

	-- Data for Name: t_cluster; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE 'CREATE TEMPORARY TABLE t_cluster_temp ON COMMIT DROP AS
		SELECT (select coalesce(max(id),0) from sdesign.t_cluster) + row_number() over() AS id,
			*
		FROM '||_schema||'.clusters';
	
	RAISE DEBUG '% CREATE TEMPORARY TABLE t_cluster_temp', clock_timestamp();

	-- Data for Name: f_p_plot; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE 'CREATE TEMPORARY TABLE f_p_plot_temp ON COMMIT DROP AS
		SELECT (select coalesce(max(gid),0) from sdesign.f_p_plot) + row_number() over() AS gid,
			*
		FROM '||_schema||'.plots';
	
	RAISE DEBUG '% CREATE TEMPORARY TABLE f_p_plot_temp', clock_timestamp();

	EXECUTE '
	insert into sdesign.t_cluster (id, cluster, comment)
	select id, cluster, comment
	from t_cluster_temp';
	
	RAISE DEBUG '%  insert into sdesign.t_cluster', clock_timestamp();

	-- reset sequence
	PERFORM setval('sdesign.t_cluster_id_seq', (SELECT coalesce(max(id),0)+1 FROM sdesign.t_cluster), FALSE);

	-- Data for Name: cm_cluster2panel_mapping; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	insert into sdesign.cm_cluster2panel_mapping(panel, cluster, sampling_weight_ha)
	select
		t_panel.id,
		clusters.id,
		clusters.sampling_weight_ha
	from sdesign.c_country
	inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
	inner join sdesign.t_stratum on t_strata_set.id = t_stratum.strata_set
	inner join sdesign.t_panel on t_stratum.id = t_panel.stratum
	inner join t_cluster_temp AS clusters
		on clusters.country = c_country.label
		and clusters.strata_set = t_strata_set.strata_set
		and clusters.stratum = t_stratum.stratum
		and clusters.panel = t_panel.panel';
	
	RAISE DEBUG '% insert into sdesign.cm_cluster2panel_mapping', clock_timestamp();

	-- Data for Name: f_p_plot; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	insert into sdesign.f_p_plot(gid, plot, geom, cluster, coordinates_degraded, comment)
	select
		plots.gid,
		plots.plot,
		ST_GeomFromEWKT(plots.plot_geometry) as geom,
		t_cluster.id,
		plots.coordinates_degraded,
		plots.comment
	from sdesign.c_country
	inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
	inner join sdesign.t_stratum on t_strata_set.id = t_stratum.strata_set
	inner join sdesign.t_panel on t_stratum.id = t_panel.stratum
	inner join sdesign.cm_cluster2panel_mapping on t_panel.id = cm_cluster2panel_mapping.panel
	inner join sdesign.t_cluster on cm_cluster2panel_mapping.cluster = t_cluster.id
	inner join f_p_plot_temp AS plots
		on plots.country = c_country.label
		and plots.strata_set = t_strata_set.strata_set
		and plots.stratum = t_stratum.stratum
		and plots.panel = t_panel.panel
		and plots.cluster = t_cluster.cluster';

	
	RAISE DEBUG '% insert into sdesign.f_p_plot', clock_timestamp();

	-- reset sequence
	PERFORM setval('sdesign.f_p_plot_gid_seq', (SELECT coalesce(max(gid),0)+1 FROM sdesign.f_p_plot), FALSE);

	-- Data for Name: cm_plot2cluster_config_mapping; Type: TABLE DATA; Schema: sdesign; Owner: vagrant

	insert into sdesign.cm_plot2cluster_config_mapping (cluster_configuration, plot)
	select
		t_cluster_configuration.id,
		plots.gid
	from sdesign.c_country
	inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
	inner join sdesign.t_stratum on t_strata_set.id = t_stratum.strata_set
	inner join sdesign.t_panel on t_stratum.id = t_panel.stratum
	inner join sdesign.t_cluster_configuration on t_panel.cluster_configuration = t_cluster_configuration.id
	inner join sdesign.cm_cluster2panel_mapping on t_panel.id = cm_cluster2panel_mapping.panel
	inner join sdesign.t_cluster on cm_cluster2panel_mapping.cluster = t_cluster.id
	inner join f_p_plot_temp AS plots
		on plots.country = c_country.label
		and plots.strata_set = t_strata_set.strata_set
		and plots.stratum = t_stratum.stratum
		and plots.panel = t_panel.panel
		and plots.cluster = t_cluster.cluster
	group by t_cluster_configuration.id, plots.gid
	order by t_cluster_configuration.id, plots.gid;
	
	RAISE DEBUG '% insert into sdesign.cm_plot2cluster_config_mapping', clock_timestamp();

	EXECUTE 'CREATE TEMPORARY TABLE t_inventory_campaign_temp ON COMMIT DROP AS
		SELECT (select coalesce(max(id),0) from sdesign.t_inventory_campaign) + row_number() over() AS id,
			*
		FROM '||_schema||'.inventory_campaigns';
	
	RAISE DEBUG '% CREATE TEMPORARY TABLE t_inventory_campaign_temp', clock_timestamp();

	-- Data for Name: t_inventory_campaign; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	-- STATE
	EXECUTE '
		insert into sdesign.t_inventory_campaign(id, inventory_campaign, label, status_variables, comment)
		select
			id, inventory_campaign, label, status_variables, comment
		from t_inventory_campaign_temp
	';

	RAISE DEBUG '% insert into sdesign.t_inventory_campaign', clock_timestamp();
	
	PERFORM setval('sdesign.t_inventory_campaign_id_seq', (SELECT coalesce(max(id),0)+1 FROM sdesign.t_inventory_campaign), FALSE);

	-- Data for Name: t_reference_year_set; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	-- STATE
	EXECUTE '
	with w_insert as (
		insert into sdesign.t_reference_year_set(inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, status_variables, comment)
		select
			t_inventory_campaign_temp.id,
			reference_year_set,
			reference_date_begin,
			reference_date_end,
			reference_year_sets.label,
			reference_year_sets.status_variables,
			reference_year_sets.comment
		from sdesign.c_country
		inner join t_inventory_campaign_temp on c_country.label = t_inventory_campaign_temp.country
		inner join '||_schema||'.reference_year_sets
			on reference_year_sets.country = t_inventory_campaign_temp.country
			and reference_year_sets.inventory_campaign = t_inventory_campaign_temp.inventory_campaign
		where reference_year_sets.reference_year_set_begin is null and reference_year_sets.reference_year_set_end is null
		order by reference_year_set
		returning id, inventory_campaign, reference_year_set
	)
	, w_insert_country as (
		select 
			w_insert.id, t_inventory_campaign_temp.inventory_campaign, w_insert.reference_year_set, t_inventory_campaign_temp.country
		from w_insert
		inner join t_inventory_campaign_temp on (w_insert.inventory_campaign = t_inventory_campaign_temp.id)
	)	
	insert into sdesign.t_reference_year_set(inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, status_variables, comment, reference_year_set_begin, reference_year_set_end)
	select
		t_inventory_campaign_temp.id,
		reference_year_sets.reference_year_set,
		reference_year_sets.reference_date_begin,
		reference_year_sets.reference_date_end,
		reference_year_sets.label,
		reference_year_sets.status_variables,
		reference_year_sets.comment,
		(select id from w_insert_country where 
			w_insert_country.reference_year_set 	= reference_year_sets.reference_year_set_begin and 
			w_insert_country.inventory_campaign 	= reference_year_sets.inventory_campaign_begin and
			w_insert_country.country 		= reference_year_sets.country
		),
		(select id from w_insert_country where 
			w_insert_country.reference_year_set 	= reference_year_sets.reference_year_set_end and 
			w_insert_country.inventory_campaign 	= reference_year_sets.inventory_campaign_end and
			w_insert_country.country 		= reference_year_sets.country
		)
	from sdesign.c_country
	inner join t_inventory_campaign_temp on c_country.label = t_inventory_campaign_temp.country
	inner join '||_schema||'.reference_year_sets
		on reference_year_sets.country = t_inventory_campaign_temp.country
		and reference_year_sets.inventory_campaign = t_inventory_campaign_temp.inventory_campaign
	where reference_year_sets.reference_year_set_begin is not null and reference_year_sets.reference_year_set_end is not null
	order by reference_year_sets.reference_year_set';
	
	RAISE DEBUG '% insert into sdesign.t_reference_year_set', clock_timestamp();

	-- Data for Name: t_plot_measurement_dates; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	insert into sdesign.t_plot_measurement_dates(plot, reference_year_set, measurement_date, comment)
	select
		f_p_plot.gid AS plot,
		t_reference_year_set.id,
		plot_measurement_dates.measurement_date,
		plot_measurement_dates.comment
	from sdesign.c_country
	inner join t_inventory_campaign_temp on c_country.label = t_inventory_campaign_temp.country
	inner join sdesign.t_reference_year_set on t_inventory_campaign_temp.id = t_reference_year_set.inventory_campaign
	inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
	inner join sdesign.t_stratum on t_strata_set.id = t_stratum.strata_set
	inner join sdesign.t_panel on t_stratum.id = t_panel.stratum
	inner join sdesign.cm_cluster2panel_mapping on t_panel.id = cm_cluster2panel_mapping.panel
	inner join sdesign.t_cluster on cm_cluster2panel_mapping.cluster = t_cluster.id
	inner join sdesign.f_p_plot on t_cluster.id = f_p_plot.cluster
	inner join '||_schema||'.plot_measurement_dates
		on plot_measurement_dates.country = c_country.label
		and plot_measurement_dates.inventory_campaign = t_inventory_campaign_temp.inventory_campaign
		and plot_measurement_dates.reference_year_set = t_reference_year_set.reference_year_set
		and plot_measurement_dates.strata_set = t_strata_set.strata_set
		and plot_measurement_dates.stratum = t_stratum.stratum
		and plot_measurement_dates.panel = t_panel.panel
		and plot_measurement_dates.cluster = t_cluster.cluster
		and plot_measurement_dates.plot = f_p_plot.plot
	order by plot, t_reference_year_set.id';

	RAISE DEBUG '% insert into sdesign.t_plot_measurement_dates', clock_timestamp();

	-- Data for Name: cm_refyearset2panel_mapping; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
EXECUTE '
	with w_data as (
		select
			t_panel.id as t_panel__id,
			t_reference_year_set.id as t_reference_year_set__id,
			refyearset2panel.comment
		from sdesign.c_country
		inner join t_inventory_campaign_temp on c_country.label = t_inventory_campaign_temp.country
		inner join sdesign.t_reference_year_set on t_inventory_campaign_temp.id = t_reference_year_set.inventory_campaign
		inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
		inner join sdesign.t_stratum on t_strata_set.id = t_stratum.strata_set
		inner join sdesign.t_panel on t_stratum.id = t_panel.stratum
		inner join '||_schema||'.refyearset2panel
			on refyearset2panel.country = c_country.label
			and refyearset2panel.inventory_campaign = t_inventory_campaign_temp.inventory_campaign
			and refyearset2panel.reference_year_set = t_reference_year_set.reference_year_set
			and refyearset2panel.strata_set = t_strata_set.strata_set
			and refyearset2panel.stratum = t_stratum.stratum
			and refyearset2panel.panel = t_panel.panel
	)
	insert into sdesign.cm_refyearset2panel_mapping (panel, reference_year_set,comment)
	select t_panel__id, t_reference_year_set__id, comment from w_data
	order by t_panel__id, t_reference_year_set__id, comment;
	';

	RAISE DEBUG '% insert into sdesign.cm_refyearset2panel_mapping', clock_timestamp();

	-- Name: c_country_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.c_country_id_seq', (select max(id) from sdesign.c_country), true);

	-- Name: cm_cluster2panel_mapping_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.cm_cluster2panel_mapping_id_seq', (select max(id) from sdesign.cm_cluster2panel_mapping), true);

	-- Name: cm_plot2cluster_config_mapping_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.cm_plot2cluster_config_mapping_id_seq', (select max(id) from sdesign.cm_plot2cluster_config_mapping), true);

	-- Name: cm_refyearset2panel_mapping_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.cm_refyearset2panel_mapping_id_seq', (select max(id) from sdesign.cm_refyearset2panel_mapping), true);

	-- Name: f_p_plot_gid_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.f_p_plot_gid_seq', (select max(gid) from sdesign.f_p_plot), true);

	-- Name: t_cluster_configuration_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_cluster_configuration_id_seq', (select max(id) from sdesign.t_cluster_configuration), true);

	-- Name: t_cluster_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_cluster_id_seq', (select max(id) from sdesign.t_cluster), true);

	-- Name: t_inventory_campaign_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_inventory_campaign_id_seq', (select max(id) from sdesign.t_inventory_campaign), true);

	-- Name: t_panel_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_panel_id_seq', (select max(id) from sdesign.t_panel), true);

	-- Name: t_plot_measurement_dates_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_plot_measurement_dates_id_seq', (select max(id) from sdesign.t_plot_measurement_dates), true);

	-- Name: t_reference_year_set_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_reference_year_set_id_seq', (select max(id) from sdesign.t_reference_year_set), true);

	-- Name: t_strata_set_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_strata_set_id_seq', (select max(id) from sdesign.t_strata_set), true);

	-- Name: t_stratum_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_stratum_id_seq', (select max(id) from sdesign.t_stratum), true);

	_res := 'Import is comleted.';

	return _res;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION sdesign.fn_import_data(character varying) IS
'The function imports data from csv tables into tables that are in the sdesign schema.';

-- </function>

-- <function name="fn_check_sweight_panel_sum" schema="analytical" src="functions/fn_check_sweight_panel_sum.sql">
--drop function sdesign.fn_check_sweight_panel_sum() cascade;
CREATE OR REPLACE FUNCTION sdesign.fn_check_sweight_panel_sum() RETURNS TRIGGER AS $src$
DECLARE
	_output json;
BEGIN
	_output := (with w_data as (
		select
			t_panel.panel,
			round((case when t_cluster_configuration.cluster_design then t_stratum.frame_area_ha else t_stratum.area_ha end)::numeric, 6) as area,
			round(new.sweight_panel_sum::numeric, 6) as sweight_sum,
			round(sum(cm_cluster2panel_mapping.sampling_weight_ha)::numeric, 6) as sampling_weight_ha_sum
		from sdesign.t_panel
		join sdesign.cm_cluster2panel_mapping ON t_panel.id = cm_cluster2panel_mapping.panel
		join sdesign.t_cluster_configuration ON t_cluster_configuration.id = t_panel.cluster_configuration
		join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
		where t_panel.id = new.id
		group by t_panel.panel, t_panel.sweight_panel_sum, t_cluster_configuration.cluster_design, t_stratum.area_ha, t_stratum.frame_area_ha
		order by t_panel.panel
	)
	select
		json_build_object('panel', panel, 'area', area, 'sweight_sum', sweight_sum, 'sampling_weight_ha_sum', sampling_weight_ha_sum)
	from w_data
	);

	IF (_output->>'area' !=  _output->>'sweight_sum' or _output->>'sweight_sum' != _output->>'sampling_weight_ha_sum')
		THEN RAISE EXCEPTION 'fn_check_sweight_panel_sum: sum of sampling weights not equal to stratum (frame) area! (panel: %, stratum area: %, t_panel.sweight_panel_sum: %, sum(cm_cluster2panel_mapping.sampling_weight_ha): %)', 
			_output->>'panel', _output->>'area', _output->>'sweight_sum', _output->>'sampling_weight_ha_sum';
	END IF;
RETURN NULL; -- result is ignored since this is an AFTER trigger
END;
$src$ LANGUAGE plpgsql;

drop trigger if exists trg__panel__check_threshold__ins on sdesign.t_panel;
CREATE CONSTRAINT TRIGGER trg__panel__check_threshold__ins
	AFTER INSERT ON sdesign.t_panel
	DEFERRABLE INITIALLY DEFERRED
	FOR EACH ROW EXECUTE FUNCTION sdesign.fn_check_sweight_panel_sum();

drop trigger if exists trg__panel__check_threshold__upd on sdesign.t_panel;
CREATE CONSTRAINT TRIGGER trg__panel__check_threshold__upd
	AFTER UPDATE ON sdesign.t_panel
	DEFERRABLE INITIALLY DEFERRED
	FOR EACH ROW EXECUTE FUNCTION sdesign.fn_check_sweight_panel_sum();

-- </function>

-- <function name="fn_import_data" schema="analytical" src="functions/fn_import_data.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--
---------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- fn_import_data
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS sdesign.fn_import_data(character varying) CASCADE;

CREATE OR REPLACE FUNCTION sdesign.fn_import_data(_schema character varying)
RETURNS text
AS
$$
DECLARE
		_res	text;
BEGIN
	EXECUTE 'CREATE TEMPORARY TABLE t_cluster_configuration_temp ON COMMIT DROP AS
		SELECT (select coalesce(max(id),0) from sdesign.t_cluster_configuration) + row_number() over() AS id,
			*
		FROM '||_schema||'.cluster_configurations';

	--set client_min_messages = DEBUG1;
	RAISE DEBUG '% t_cluster_configuration_temp', clock_timestamp();

	-- Data for Name: t_cluster_configuration; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE'
	insert into sdesign.t_cluster_configuration(id, cluster_configuration, cluster_design, cluster_rotation, geom, plots_per_cluster, label, comment)
	select
		id,
		cluster_configuration,
		cluster_design,
		cluster_rotation,
		ST_GeomFromText(cluster_geom)::geometry(MULTIPOINT) as geom,
		case when cluster_design then
			ST_NumGeometries(ST_GeomFromText(cluster_geom)::geometry(MultiPoint))
		else
			1
		end as plots_per_cluster,
		label,
		comment
	from t_cluster_configuration_temp';

	RAISE DEBUG '% insert into sdesign.t_cluster_configuration', clock_timestamp();

	PERFORM setval('sdesign.t_cluster_configuration_id_seq', (SELECT coalesce(max(id),0)+1 FROM sdesign.t_cluster_configuration), FALSE);

	-- Data for Name: t_strata_set; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE 'insert into sdesign.t_strata_set(country, strata_set, label, comment)
	select c_country.id, strata_sets.strata_set, strata_sets.label, strata_sets.comment 
	from sdesign.c_country
	inner join '||_schema||'.strata_sets
	on strata_sets.country = c_country.label
	order by country, strata_set';

	RAISE DEBUG '% insert into sdesign.t_strata_set', clock_timestamp();
	-- Data for Name: t_stratum; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	insert into sdesign.t_stratum(stratum, area_ha, frame_area_ha, comment, geom, strata_set, label)
	select
		strata.stratum,
		coalesce(strata.area_ha, ST_Area(ST_GeomFromEWKT(strata.geometry))/10000.0) as area_ha,
		strata.frame_area_ha,
		strata.comment,
		ST_GeomFromEWKT(strata.geometry) as geom,
		t_strata_set.id as strata_set,
		strata.label
	from sdesign.c_country
	inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
	inner join '||_schema||'.strata	
		on strata.strata_set = t_strata_set.strata_set
		and strata.country = c_country.label';

	RAISE DEBUG '% insert into sdesign.t_stratum', clock_timestamp();
	-- Data for Name: t_panel; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	with w_clusters_data as (
		select country, strata_set, stratum, panel, count(*) as cluster_count, sum(sampling_weight_ha) as sweight_panel_sum
		from '||_schema||'.clusters
		group by country, strata_set, stratum, panel order by panel
	)
	, w_plots_data as (
		select country, strata_set, stratum, panel, count(*) as plot_count
		from '||_schema||'.plots
		group by country, strata_set, stratum, panel order by panel
	)
	insert into sdesign.t_panel(stratum, panel, cluster_configuration, label, comment, cluster_count, plot_count, sweight_panel_sum)
	select
		t_stratum.id as stratum,
		panels.panel,
		t_cluster_configuration_temp.id as cluster_configuration,
		panels.label,
		panels.comment,
		w_clusters_data.cluster_count,
		w_plots_data.plot_count,
		w_clusters_data.sweight_panel_sum
	from sdesign.c_country
	inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
	inner join sdesign.t_stratum on t_strata_set.id = t_stratum.strata_set
	inner join t_cluster_configuration_temp on c_country.label = t_cluster_configuration_temp.country
	inner join '||_schema||'.panels
		on panels.country = c_country.label
		and panels.strata_set = t_strata_set.strata_set
		and panels.stratum = t_stratum.stratum
		and panels.cluster_configuration = t_cluster_configuration_temp.cluster_configuration
	inner join w_clusters_data
		on panels.country = w_clusters_data.country
		and panels.strata_set = w_clusters_data.strata_set
		and panels.stratum = w_clusters_data.stratum
		and panels.panel = w_clusters_data.panel
	inner join w_plots_data
		on panels.country = w_plots_data.country
		and panels.strata_set = w_plots_data.strata_set
		and panels.stratum = w_plots_data.stratum
		and panels.panel = w_plots_data.panel
	order by panels.strata_set, panels.stratum, panels.panel';
	
	RAISE DEBUG '% insert into sdesign.t_panel', clock_timestamp();

	with w_stratum as (
		SELECT
			t_stratum.id,
			t_stratum.stratum,
			t_stratum.geom,
			t_cluster_configuration.cluster_configuration,
			CASE	WHEN t_cluster_configuration.cluster_configuration in ( '2p-r', '2p', '4p' )
				THEN
						st_distance(
							ST_GeometryN(t_cluster_configuration.geom, 1),
							ST_GeometryN(t_cluster_configuration.geom, 2))::integer
			END AS cluster_distance
		FROM sdesign.t_stratum
		INNER JOIN sdesign.t_panel on (t_stratum.id = t_panel.stratum)
		INNER JOIN sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id)
		WHERE t_stratum.frame_area_ha IS NULL
	)
	, w_stratum_buff as (
		SELECT
			id,
			stratum,
			CASE 	WHEN cluster_configuration = '2p-r' THEN
					sdesign.fn_create_buffered_geometry(geom, 100, cluster_distance)
				WHEN cluster_configuration = '2p' THEN
					sdesign.fn_create_buffered_geometry(geom, 200, cluster_distance)
				WHEN cluster_configuration = '4p' THEN
					sdesign.fn_create_buffered_geometry(geom, 300, cluster_distance)
				WHEN cluster_configuration = '1p' THEN
					NULL
				ELSE
					NULL --sdesign.fn_create_buffered_geometry(geom, 0, NULL::integer)
			END AS buff_geom
		FROM w_stratum
	)
	, w_stratum_buff_area as (
		SELECT
			id,
			stratum,
			buff_geom AS geom_buffered,
			ST_Area(buff_geom)/10000.0 AS area_ha_buffered
		FROM
			w_stratum_buff
		WHERE
			buff_geom IS NOT NULL
	)
	UPDATE sdesign.t_stratum
	SET
		frame_area_ha = w_stratum_buff_area.area_ha_buffered,
		geom_buffered = w_stratum_buff_area.geom_buffered
	FROM w_stratum_buff_area
	WHERE w_stratum_buff_area.id = t_stratum.id;

	RAISE DEBUG '% UPDATE sdesign.t_stratum (frame_area_ha, geom_buffered)', clock_timestamp();

	-- Data for Name: t_cluster; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE 'CREATE TEMPORARY TABLE t_cluster_temp ON COMMIT DROP AS
		SELECT (select coalesce(max(id),0) from sdesign.t_cluster) + row_number() over() AS id,
			*
		FROM '||_schema||'.clusters';
	
	RAISE DEBUG '% CREATE TEMPORARY TABLE t_cluster_temp', clock_timestamp();

	-- Data for Name: f_p_plot; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE 'CREATE TEMPORARY TABLE f_p_plot_temp ON COMMIT DROP AS
		SELECT (select coalesce(max(gid),0) from sdesign.f_p_plot) + row_number() over() AS gid,
			*
		FROM '||_schema||'.plots';
	
	RAISE DEBUG '% CREATE TEMPORARY TABLE f_p_plot_temp', clock_timestamp();

	EXECUTE '
	insert into sdesign.t_cluster (id, cluster, comment)
	select id, cluster, comment
	from t_cluster_temp';
	
	RAISE DEBUG '%  insert into sdesign.t_cluster', clock_timestamp();

	-- reset sequence
	PERFORM setval('sdesign.t_cluster_id_seq', (SELECT coalesce(max(id),0)+1 FROM sdesign.t_cluster), FALSE);

	-- Data for Name: cm_cluster2panel_mapping; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	insert into sdesign.cm_cluster2panel_mapping(panel, cluster, sampling_weight_ha)
	select
		t_panel.id,
		clusters.id,
		clusters.sampling_weight_ha
	from sdesign.c_country
	inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
	inner join sdesign.t_stratum on t_strata_set.id = t_stratum.strata_set
	inner join sdesign.t_panel on t_stratum.id = t_panel.stratum
	inner join t_cluster_temp AS clusters
		on clusters.country = c_country.label
		and clusters.strata_set = t_strata_set.strata_set
		and clusters.stratum = t_stratum.stratum
		and clusters.panel = t_panel.panel';
	
	RAISE DEBUG '% insert into sdesign.cm_cluster2panel_mapping', clock_timestamp();

	-- Data for Name: f_p_plot; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	insert into sdesign.f_p_plot(gid, plot, geom, cluster, coordinates_degraded, comment)
	select
		plots.gid,
		plots.plot,
		ST_GeomFromEWKT(plots.plot_geometry) as geom,
		t_cluster.id,
		plots.coordinates_degraded,
		plots.comment
	from sdesign.c_country
	inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
	inner join sdesign.t_stratum on t_strata_set.id = t_stratum.strata_set
	inner join sdesign.t_panel on t_stratum.id = t_panel.stratum
	inner join sdesign.cm_cluster2panel_mapping on t_panel.id = cm_cluster2panel_mapping.panel
	inner join sdesign.t_cluster on cm_cluster2panel_mapping.cluster = t_cluster.id
	inner join f_p_plot_temp AS plots
		on plots.country = c_country.label
		and plots.strata_set = t_strata_set.strata_set
		and plots.stratum = t_stratum.stratum
		and plots.panel = t_panel.panel
		and plots.cluster = t_cluster.cluster';

	
	RAISE DEBUG '% insert into sdesign.f_p_plot', clock_timestamp();

	-- reset sequence
	PERFORM setval('sdesign.f_p_plot_gid_seq', (SELECT coalesce(max(gid),0)+1 FROM sdesign.f_p_plot), FALSE);

	-- Data for Name: cm_plot2cluster_config_mapping; Type: TABLE DATA; Schema: sdesign; Owner: vagrant

	insert into sdesign.cm_plot2cluster_config_mapping (cluster_configuration, plot)
	select
		t_cluster_configuration.id,
		plots.gid
	from sdesign.c_country
	inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
	inner join sdesign.t_stratum on t_strata_set.id = t_stratum.strata_set
	inner join sdesign.t_panel on t_stratum.id = t_panel.stratum
	inner join sdesign.t_cluster_configuration on t_panel.cluster_configuration = t_cluster_configuration.id
	inner join sdesign.cm_cluster2panel_mapping on t_panel.id = cm_cluster2panel_mapping.panel
	inner join sdesign.t_cluster on cm_cluster2panel_mapping.cluster = t_cluster.id
	inner join f_p_plot_temp AS plots
		on plots.country = c_country.label
		and plots.strata_set = t_strata_set.strata_set
		and plots.stratum = t_stratum.stratum
		and plots.panel = t_panel.panel
		and plots.cluster = t_cluster.cluster
	group by t_cluster_configuration.id, plots.gid
	order by t_cluster_configuration.id, plots.gid;
	
	RAISE DEBUG '% insert into sdesign.cm_plot2cluster_config_mapping', clock_timestamp();

	EXECUTE 'CREATE TEMPORARY TABLE t_inventory_campaign_temp ON COMMIT DROP AS
		SELECT (select coalesce(max(id),0) from sdesign.t_inventory_campaign) + row_number() over() AS id,
			*
		FROM '||_schema||'.inventory_campaigns';
	
	RAISE DEBUG '% CREATE TEMPORARY TABLE t_inventory_campaign_temp', clock_timestamp();

	-- Data for Name: t_inventory_campaign; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	-- STATE
	EXECUTE '
		insert into sdesign.t_inventory_campaign(id, inventory_campaign, label, status_variables, comment)
		select
			id, inventory_campaign, label, status_variables, comment
		from t_inventory_campaign_temp
	';

	RAISE DEBUG '% insert into sdesign.t_inventory_campaign', clock_timestamp();
	
	PERFORM setval('sdesign.t_inventory_campaign_id_seq', (SELECT coalesce(max(id),0)+1 FROM sdesign.t_inventory_campaign), FALSE);

	-- Data for Name: t_reference_year_set; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	-- STATE
	EXECUTE '
	with w_insert as (
		insert into sdesign.t_reference_year_set(inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, status_variables, comment)
		select
			t_inventory_campaign_temp.id,
			reference_year_set,
			reference_date_begin,
			reference_date_end,
			reference_year_sets.label,
			reference_year_sets.status_variables,
			reference_year_sets.comment
		from sdesign.c_country
		inner join t_inventory_campaign_temp on c_country.label = t_inventory_campaign_temp.country
		inner join '||_schema||'.reference_year_sets
			on reference_year_sets.country = t_inventory_campaign_temp.country
			and reference_year_sets.inventory_campaign = t_inventory_campaign_temp.inventory_campaign
		where reference_year_sets.reference_year_set_begin is null and reference_year_sets.reference_year_set_end is null
		order by reference_year_set
		returning id, inventory_campaign, reference_year_set
	)
	, w_insert_country as (
		select 
			w_insert.id, t_inventory_campaign_temp.inventory_campaign, w_insert.reference_year_set, t_inventory_campaign_temp.country
		from w_insert
		inner join t_inventory_campaign_temp on (w_insert.inventory_campaign = t_inventory_campaign_temp.id)
	)	
	insert into sdesign.t_reference_year_set(inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, status_variables, comment, reference_year_set_begin, reference_year_set_end)
	select
		t_inventory_campaign_temp.id,
		reference_year_sets.reference_year_set,
		reference_year_sets.reference_date_begin,
		reference_year_sets.reference_date_end,
		reference_year_sets.label,
		reference_year_sets.status_variables,
		reference_year_sets.comment,
		(select id from w_insert_country where 
			w_insert_country.reference_year_set 	= reference_year_sets.reference_year_set_begin and 
			w_insert_country.inventory_campaign 	= reference_year_sets.inventory_campaign_begin and
			w_insert_country.country 		= reference_year_sets.country
		),
		(select id from w_insert_country where 
			w_insert_country.reference_year_set 	= reference_year_sets.reference_year_set_end and 
			w_insert_country.inventory_campaign 	= reference_year_sets.inventory_campaign_end and
			w_insert_country.country 		= reference_year_sets.country
		)
	from sdesign.c_country
	inner join t_inventory_campaign_temp on c_country.label = t_inventory_campaign_temp.country
	inner join '||_schema||'.reference_year_sets
		on reference_year_sets.country = t_inventory_campaign_temp.country
		and reference_year_sets.inventory_campaign = t_inventory_campaign_temp.inventory_campaign
	where reference_year_sets.reference_year_set_begin is not null and reference_year_sets.reference_year_set_end is not null
	order by reference_year_sets.reference_year_set';
	
	RAISE DEBUG '% insert into sdesign.t_reference_year_set', clock_timestamp();

	-- Data for Name: t_plot_measurement_dates; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	insert into sdesign.t_plot_measurement_dates(plot, reference_year_set, measurement_date, comment)
	select
		f_p_plot.gid AS plot,
		t_reference_year_set.id,
		plot_measurement_dates.measurement_date,
		plot_measurement_dates.comment
	from sdesign.c_country
	inner join t_inventory_campaign_temp on c_country.label = t_inventory_campaign_temp.country
	inner join sdesign.t_reference_year_set on t_inventory_campaign_temp.id = t_reference_year_set.inventory_campaign
	inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
	inner join sdesign.t_stratum on t_strata_set.id = t_stratum.strata_set
	inner join sdesign.t_panel on t_stratum.id = t_panel.stratum
	inner join sdesign.cm_cluster2panel_mapping on t_panel.id = cm_cluster2panel_mapping.panel
	inner join sdesign.t_cluster on cm_cluster2panel_mapping.cluster = t_cluster.id
	inner join sdesign.f_p_plot on t_cluster.id = f_p_plot.cluster
	inner join '||_schema||'.plot_measurement_dates
		on plot_measurement_dates.country = c_country.label
		and plot_measurement_dates.inventory_campaign = t_inventory_campaign_temp.inventory_campaign
		and plot_measurement_dates.reference_year_set = t_reference_year_set.reference_year_set
		and plot_measurement_dates.strata_set = t_strata_set.strata_set
		and plot_measurement_dates.stratum = t_stratum.stratum
		and plot_measurement_dates.panel = t_panel.panel
		and plot_measurement_dates.cluster = t_cluster.cluster
		and plot_measurement_dates.plot = f_p_plot.plot
	order by plot, t_reference_year_set.id';

	RAISE DEBUG '% insert into sdesign.t_plot_measurement_dates', clock_timestamp();

	-- Data for Name: cm_refyearset2panel_mapping; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
EXECUTE '
	with w_data as (
		select
			t_panel.id as t_panel__id,
			t_reference_year_set.id as t_reference_year_set__id,
			refyearset2panel.comment
		from sdesign.c_country
		inner join t_inventory_campaign_temp on c_country.label = t_inventory_campaign_temp.country
		inner join sdesign.t_reference_year_set on t_inventory_campaign_temp.id = t_reference_year_set.inventory_campaign
		inner join sdesign.t_strata_set on c_country.id = t_strata_set.country
		inner join sdesign.t_stratum on t_strata_set.id = t_stratum.strata_set
		inner join sdesign.t_panel on t_stratum.id = t_panel.stratum
		inner join '||_schema||'.refyearset2panel
			on refyearset2panel.country = c_country.label
			and refyearset2panel.inventory_campaign = t_inventory_campaign_temp.inventory_campaign
			and refyearset2panel.reference_year_set = t_reference_year_set.reference_year_set
			and refyearset2panel.strata_set = t_strata_set.strata_set
			and refyearset2panel.stratum = t_stratum.stratum
			and refyearset2panel.panel = t_panel.panel
	)
	insert into sdesign.cm_refyearset2panel_mapping (panel, reference_year_set,comment)
	select t_panel__id, t_reference_year_set__id, comment from w_data
	order by t_panel__id, t_reference_year_set__id, comment;
	';

	RAISE DEBUG '% insert into sdesign.cm_refyearset2panel_mapping', clock_timestamp();

	-- Name: c_country_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.c_country_id_seq', (select max(id) from sdesign.c_country), true);

	-- Name: cm_cluster2panel_mapping_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.cm_cluster2panel_mapping_id_seq', (select max(id) from sdesign.cm_cluster2panel_mapping), true);

	-- Name: cm_plot2cluster_config_mapping_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.cm_plot2cluster_config_mapping_id_seq', (select max(id) from sdesign.cm_plot2cluster_config_mapping), true);

	-- Name: cm_refyearset2panel_mapping_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.cm_refyearset2panel_mapping_id_seq', (select max(id) from sdesign.cm_refyearset2panel_mapping), true);

	-- Name: f_p_plot_gid_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.f_p_plot_gid_seq', (select max(gid) from sdesign.f_p_plot), true);

	-- Name: t_cluster_configuration_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_cluster_configuration_id_seq', (select max(id) from sdesign.t_cluster_configuration), true);

	-- Name: t_cluster_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_cluster_id_seq', (select max(id) from sdesign.t_cluster), true);

	-- Name: t_inventory_campaign_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_inventory_campaign_id_seq', (select max(id) from sdesign.t_inventory_campaign), true);

	-- Name: t_panel_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_panel_id_seq', (select max(id) from sdesign.t_panel), true);

	-- Name: t_plot_measurement_dates_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_plot_measurement_dates_id_seq', (select max(id) from sdesign.t_plot_measurement_dates), true);

	-- Name: t_reference_year_set_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_reference_year_set_id_seq', (select max(id) from sdesign.t_reference_year_set), true);

	-- Name: t_strata_set_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_strata_set_id_seq', (select max(id) from sdesign.t_strata_set), true);

	-- Name: t_stratum_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_stratum_id_seq', (select max(id) from sdesign.t_stratum), true);

	_res := 'Import is comleted.';

	return _res;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION sdesign.fn_import_data(character varying) IS
'The function imports data from csv tables into tables that are in the sdesign schema.';

-- </function>
