--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------




---------------------------------------------------------------------------------------------------;
--
-- Name: c_country; Type: TABLE; Schema: sdesign. Owner: -
--

CREATE TABLE sdesign.c_country
(
	id integer NOT NULL,
	"label" varchar(100) NULL,
	description text NULL
);

--
-- Name: TABLE c_country; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON TABLE sdesign.c_country IS 'Looup table of country codes.';


--
-- Name: COLUMN c_country.id; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.c_country.id IS 'Primary key, identifier of the country.';


--
-- Name: COLUMN c_country.label; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.c_country.label IS 'Label (code) of the country.';


--
-- Name: COLUMN c_country.description; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.c_country.description IS 'Description (name) of the country.';


--
-- Name: c_country pkey__c_country; Type: CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.c_country
    ADD CONSTRAINT pkey__c_country PRIMARY KEY (id);


--
-- Name: COLUMN c_country.id; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.c_country.id IS 'Identifier of country, primary key.';


--
-- Name: c_country_id_seq; Type: SEQUENCE; Schema: sdesign. Owner: -
--

CREATE SEQUENCE sdesign.c_country_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: c_country_id_seq; Type: SEQUENCE OWNED BY; Schema: sdesign. Owner: -
--

ALTER SEQUENCE sdesign.c_country_id_seq OWNED BY sdesign.c_country.id;


--
-- Name: c_country id; Type: DEFAULT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.c_country ALTER COLUMN id SET DEFAULT nextval('sdesign.c_country_id_seq'::regclass);
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
--
-- Name: t_cluster_configuration; Type: TABLE; Schema: sdesign. Owner: -
--

CREATE TABLE sdesign.t_cluster_configuration (
    id integer NOT NULL,
    cluster_configuration character varying(20) NOT NULL,
    cluster_design boolean NOT NULL,
    cluster_rotation boolean,
    geom public.geometry(MultiPoint),
    plots_per_cluster integer NOT NULL,
    label character varying(120) NOT NULL,
    comment text
);


--
-- Name: TABLE t_cluster_configuration; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON TABLE sdesign.t_cluster_configuration IS 'Table of cluster configurations.';


--
-- Name: COLUMN t_cluster_configuration.id; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_cluster_configuration.id IS 'Identifier of cluster configuration, primary key.';


--
-- Name: COLUMN t_cluster_configuration.cluster_configuration; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_cluster_configuration.cluster_configuration IS 'Character identifier of cluster configuration.';


--
-- Name: COLUMN t_cluster_configuration.cluster_design; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_cluster_configuration.cluster_design IS 'Boolean value coding if clusters are used.';


--
-- Name: COLUMN t_cluster_configuration.cluster_rotation; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_cluster_configuration.cluster_rotation IS 'Boolean value coding if there is random rotation of plots.';


--
-- Name: COLUMN t_cluster_configuration.geom; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_cluster_configuration.geom IS 'The geometry of a standard cluster (local unprojected Cartesian coordinate system). 
Axis x (first coordinate) aims to the east, y axis (second coordinate) aims to the north. Units are meters. 
Plots of the cluster would be represented by point geometries positioned with reference to the 0,0 anchor point. 
One of the plots may correspond to the cluster anchor point itself.';


--
-- Name: COLUMN t_cluster_configuration.plots_per_cluster; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_cluster_configuration.plots_per_cluster IS 'Nominal (and fixed) number of sample plots in each cluster.';


--
-- Name: COLUMN t_cluster_configuration.label; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_cluster_configuration.label IS 'Label of the cluster configuration.';


--
-- Name: COLUMN t_cluster_configuration.comment; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_cluster_configuration.comment IS 'Optional commentary.';


--
-- Name: t_cluster_configuration pkey__t_cluster_configuration; Type: CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_cluster_configuration
    ADD CONSTRAINT pkey__t_cluster_configuration PRIMARY KEY (id);


--
-- Name: COLUMN t_cluster_configuration.id; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_cluster_configuration.id IS 'Primary key, identifier of the cluster configuration.';


--
-- Name: t_cluster_configuration_id_seq; Type: SEQUENCE; Schema: sdesign. Owner: -
--

CREATE SEQUENCE sdesign.t_cluster_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_cluster_configuration_id_seq; Type: SEQUENCE OWNED BY; Schema: sdesign. Owner: -
--

ALTER SEQUENCE sdesign.t_cluster_configuration_id_seq OWNED BY sdesign.t_cluster_configuration.id;


--
-- Name: t_cluster_configuration id; Type: DEFAULT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_cluster_configuration ALTER COLUMN id SET DEFAULT nextval('sdesign.t_cluster_configuration_id_seq'::regclass);
---------------------------------------------------------------------------------------------------;

---------------------------------------------------------------------------------------------------;
--
-- Name: t_strata_set; Type: TABLE; Schema: sdesign. Owner: -
--

CREATE TABLE sdesign.t_strata_set (
    id integer NOT NULL,
    country integer NOT NULL,
    strata_set character varying(20) NOT NULL,
    label character varying(120) NOT NULL,
    comment text
);


--
-- Name: TABLE t_strata_set; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON TABLE sdesign.t_strata_set IS 'Table of agregations of stratas - valid for each country, in most cases the stratum set consists of 1 sampling stratum.';


--
-- Name: COLUMN t_strata_set.id; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_strata_set.id IS 'Identifier of strata set, primary key.';


--
-- Name: COLUMN t_strata_set.strata_set; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_strata_set.strata_set IS 'Character identifier of strata set.';


--
-- Name: COLUMN t_strata_set.label; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_strata_set.label IS 'Label of strata set.';


--
-- Name: COLUMN t_strata_set.comment; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_strata_set.comment IS 'Optional commentary.';


--
-- Name: t_strata_set pkey__t_strata_set; Type: CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_strata_set
    ADD CONSTRAINT pkey__t_strata_set PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__t_strata_set ON t_strata_set; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT pkey__t_strata_set ON sdesign.t_strata_set IS 'Primary key.';


--
-- Name: t_strata_set fkey__t_strata_set__c_country; Type: FK CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_strata_set
    ADD CONSTRAINT fkey__t_strata_set__c_country FOREIGN KEY (country) REFERENCES sdesign.c_country(id);


--
-- Name: COLUMN t_strata_set.country; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_strata_set.country IS 'Identifier of country, foreign key to table c_country.';


--
-- Name: t_strata_set_id_seq; Type: SEQUENCE; Schema: sdesign. Owner: -
--

CREATE SEQUENCE sdesign.t_strata_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_strata_set_id_seq; Type: SEQUENCE OWNED BY; Schema: sdesign. Owner: -
--

ALTER SEQUENCE sdesign.t_strata_set_id_seq OWNED BY sdesign.t_strata_set.id;


--
-- Name: t_strata_set id; Type: DEFAULT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_strata_set ALTER COLUMN id SET DEFAULT nextval('sdesign.t_strata_set_id_seq'::regclass);
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
--
-- Name: t_stratum; Type: TABLE; Schema: sdesign. Owner: -
--

CREATE TABLE sdesign.t_stratum (
    id integer NOT NULL,
    stratum character varying(20),
    area_m2 double precision,
    buffered_area_m2 double precision,
    comment text,
    geom public.geometry(MultiPolygon),
    geom_buffered public.geometry(MultiPolygon),
    strata_set integer,
    label character varying(120)
);


--
-- Name: TABLE t_stratum; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON TABLE sdesign.t_stratum IS 'Table of stratum geometries and its attributes.';


--
-- Name: COLUMN t_stratum.id; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_stratum.id IS 'Primary key, identifier of the stratum.';


--
-- Name: COLUMN t_stratum.stratum; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_stratum.stratum IS 'Id if the stratum within the country.';


--
-- Name: COLUMN t_stratum.area_m2; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_stratum.area_m2 IS 'Area of the stratum, [m2].';


--
-- Name: COLUMN t_stratum.buffered_area_m2; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_stratum.buffered_area_m2 IS 'Area of the buffered stratum (in case it is buffered), [m2].';


--
-- Name: COLUMN t_stratum.comment; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_stratum.comment IS 'How is the design implemented in particular country (e.g. systematic with grid 2x2, random origin).';


--
-- Name: COLUMN t_stratum.geom; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_stratum.geom IS 'Polygon geometry of the stratum. Can be NULL if geometry is not known.';


--
-- Name: COLUMN t_stratum.geom_buffered; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_stratum.geom_buffered IS 'Polygon geometry of the buffered stratum. Can be NULL if geometry is not known.';


--
-- Name: COLUMN t_stratum.strata_set; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_stratum.strata_set IS 'Identifier of stratum set, foreign key to table t_strata_set.';


--
-- Name: COLUMN t_stratum.label; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_stratum.label IS 'Optionaly label.';


--
-- Name: t_stratum pkey__t_stratum; Type: CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_stratum
    ADD CONSTRAINT pkey__t_stratum PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__t_stratum ON t_stratum; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT pkey__t_stratum ON sdesign.t_stratum IS 'Primary key.';


--
-- Name: t_stratum fkey__t_stratum__t_strata_set; Type: FK CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_stratum
    ADD CONSTRAINT fkey__t_stratum__t_strata_set FOREIGN KEY (strata_set) REFERENCES sdesign.t_strata_set(id);


--
-- Name: CONSTRAINT fkey__t_stratum__t_strata_set ON t_stratum; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT fkey__t_stratum__t_strata_set ON sdesign.t_stratum IS 'Foreign key to table sdesign.t_strata_set.';


--
-- Name: idx__t_stratum__geom; Type: INDEX; Schema: sdesign. Owner: -
--

CREATE INDEX idx__t_stratum__geom ON sdesign.t_stratum USING gist (geom);


--
-- Name: idx__t_stratum__geom; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON INDEX sdesign.idx__t_stratum__geom IS 'Gist index on column geom.';


--
-- Name: idx__t_stratum__geom_buffered; Type: INDEX; Schema: sdesign. Owner: -
--

CREATE INDEX idx__t_stratum__geom_buffered ON sdesign.t_stratum USING gist (geom_buffered);


--
-- Name: idx__t_stratum__geom_buffered; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON INDEX sdesign.idx__t_stratum__geom_buffered IS 'Gist index on column geom_buffered.';


--
-- Name: t_stratum_id_seq; Type: SEQUENCE; Schema: sdesign. Owner: -
--

CREATE SEQUENCE sdesign.t_stratum_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_stratum_id_seq; Type: SEQUENCE OWNED BY; Schema: sdesign. Owner: -
--

ALTER SEQUENCE sdesign.t_stratum_id_seq OWNED BY sdesign.t_stratum.id;


--
-- Name: t_stratum id; Type: DEFAULT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_stratum ALTER COLUMN id SET DEFAULT nextval('sdesign.t_stratum_id_seq'::regclass);
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
--
-- Name: t_panel; Type: TABLE; Schema: sdesign. Owner: -
--

CREATE TABLE sdesign.t_panel (
    id integer NOT NULL,
    stratum integer NOT NULL,
    panel character varying(20) NOT NULL,
    cluster_configuration integer NOT NULL,
    label character varying(120) NOT NULL,
    comment text,
    cluster_count integer NOT NULL,
    plot_count integer NOT NULL,
    panel_subset integer,
    sweight_panel_sum double precision NOT NULL
);


--
-- Name: TABLE t_panel; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON TABLE sdesign.t_panel IS 'Table of sampling panels (representative sets of sampling locations/units) within stratum.';


--
-- Name: COLUMN t_panel.id; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_panel.id IS 'Identifier of panel, primary key.';


--
-- Name: COLUMN t_panel.stratum; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_panel.stratum IS 'Identifier of sampling stratum, foreign key to table t_stratum.';


--
-- Name: COLUMN t_panel.panel; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_panel.panel IS 'Character identifier of panel.';


--
-- Name: COLUMN t_panel.cluster_configuration; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_panel.cluster_configuration IS 'Identifier of cluster configuration, foreign key to table t_cluster_configuration.';


--
-- Name: COLUMN t_panel.label; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_panel.label IS 'Label of the panel.';


--
-- Name: COLUMN t_panel.comment; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_panel.comment IS 'Optional commentary.';


--
-- Name: COLUMN t_panel.cluster_count; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_panel.cluster_count IS '.';


--
-- Name: COLUMN t_panel.plot_count; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_panel.plot_count IS '.';


--
-- Name: COLUMN t_panel.panel_subset; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_panel.panel_subset IS 'A foreign key which points to the panel from which is the current panel subseted. It is mainly designed to solve problems like "hierarchical" arrangement of panels, where one panel contains a subset of plots from another panel.';


--
-- Name: COLUMN t_panel.sweight_panel_sum; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_panel.sweight_panel_sum IS 'Sum of sampling weights for all sampling units in the panel.';


--
-- Name: t_panel pkey__t_panel; Type: CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_panel
    ADD CONSTRAINT pkey__t_panel PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__t_panel ON t_panel; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT pkey__t_panel ON sdesign.t_panel IS 'Primary key.';


--
-- Name: t_panel fkey__t_panel__t_cluster_configuration; Type: FK CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_panel
    ADD CONSTRAINT fkey__t_panel__t_cluster_configuration FOREIGN KEY (cluster_configuration) REFERENCES sdesign.t_cluster_configuration(id);


--
-- Name: CONSTRAINT fkey__t_panel__t_cluster_configuration ON t_panel; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT fkey__t_panel__t_cluster_configuration ON sdesign.t_panel IS 'Foreign key to table sdesign.t_cluster_configuration.';


--
-- Name: t_panel fkey__t_panel__t_stratum; Type: FK CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_panel
    ADD CONSTRAINT fkey__t_panel__t_stratum FOREIGN KEY (stratum) REFERENCES sdesign.t_stratum(id);


--
-- Name: CONSTRAINT fkey__t_panel__t_stratum ON t_panel; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT fkey__t_panel__t_stratum ON sdesign.t_panel IS 'Foreign key to table sdesign.t_stratum.';


--
-- Name: t_panel fkey__t_panel__t_panel; Type: FK CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_panel
    ADD CONSTRAINT fkey__t_panel__t_panel FOREIGN KEY (panel_subset) REFERENCES sdesign.t_panel(id);


--
-- Name: CONSTRAINT fkey__t_panel__t_panel ON t_panel; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT fkey__t_panel__t_panel ON sdesign.t_panel IS 'Foreign key to table sdesign.t_panel.';


--
-- Name: t_panel_id_seq; Type: SEQUENCE; Schema: sdesign. Owner: -
--

CREATE SEQUENCE sdesign.t_panel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_panel_id_seq; Type: SEQUENCE OWNED BY; Schema: sdesign. Owner: -
--

ALTER SEQUENCE sdesign.t_panel_id_seq OWNED BY sdesign.t_panel.id;


--
-- Name: t_panel id; Type: DEFAULT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_panel ALTER COLUMN id SET DEFAULT nextval('sdesign.t_panel_id_seq'::regclass);
---------------------------------------------------------------------------------------------------;



---------------------------------------------------------------------------------------------------;
--
-- Name: t_cluster; Type: TABLE; Schema: sdesign. Owner: -
--

CREATE TABLE sdesign.t_cluster (
    id integer NOT NULL,
    cluster character varying(20),
    comment text
);


--
-- Name: TABLE t_cluster; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON TABLE sdesign.t_cluster IS 'Table of sampling clusters.';


--
-- Name: COLUMN t_cluster.cluster; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_cluster.cluster IS 'Average of relative plot sampling weights per cluster. Is used to compute pix and pixy.';


--
-- Name: COLUMN t_cluster.comment; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_cluster.comment IS 'Optional commentary.';


--
-- Name: t_cluster pkey__t_cluster; Type: CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_cluster
    ADD CONSTRAINT pkey__t_cluster PRIMARY KEY (id);


--
-- Name: COLUMN t_cluster.id; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_cluster.id IS 'Primary key, identifier of the cluster.';


--
-- Name: idx__t_cluster__id; Type: INDEX; Schema: sdesign. Owner: -
--

CREATE INDEX idx__t_cluster__id ON sdesign.t_cluster USING btree (id);


--
-- Name: idx__t_cluster__id; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON INDEX sdesign.idx__t_cluster__id IS 'B-tree index on column id.';


--
-- Name: idx__t_cluster__cid; Type: INDEX; Schema: sdesign. Owner: -
--

CREATE INDEX idx__t_cluster__cid ON sdesign.t_cluster USING btree (cluster);


--
-- Name: idx__t_cluster__cid; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON INDEX sdesign.idx__t_cluster__cid IS 'B-tree index on column cid.';


--
-- Name: t_cluster_id_seq; Type: SEQUENCE; Schema: sdesign. Owner: -
--

CREATE SEQUENCE sdesign.t_cluster_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_cluster_id_seq; Type: SEQUENCE OWNED BY; Schema: sdesign. Owner: -
--

ALTER SEQUENCE sdesign.t_cluster_id_seq OWNED BY sdesign.t_cluster.id;


--
-- Name: t_cluster id; Type: DEFAULT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_cluster ALTER COLUMN id SET DEFAULT nextval('sdesign.t_cluster_id_seq'::regclass);
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
--
-- Name: cm_cluster2panel_mapping; Type: TABLE; Schema: sdesign. Owner: -
--

CREATE TABLE sdesign.cm_cluster2panel_mapping (
    id integer NOT NULL,
    panel integer NOT NULL,
    cluster integer NOT NULL,
    sampling_weight double precision NOT NULL
);


--
-- Name: TABLE cm_cluster2panel_mapping; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON TABLE sdesign.cm_cluster2panel_mapping IS 'Table with mapping between clusters and panels.';


--
-- Name: COLUMN cm_cluster2panel_mapping.id; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.cm_cluster2panel_mapping.id IS 'Identifier of mapping record, primary key.';


--
-- Name: COLUMN cm_cluster2panel_mapping.panel; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.cm_cluster2panel_mapping.panel IS 'Identifier of panel, foreign key to table t_panel.';


--
-- Name: COLUMN cm_cluster2panel_mapping.cluster; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.cm_cluster2panel_mapping.cluster IS 'Identifier of cluster (sampling unit), foreign key to table t_cluster.';


--
-- Name: COLUMN cm_cluster2panel_mapping.sampling_weight; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.cm_cluster2panel_mapping.sampling_weight IS 'Sampling weight - for each cluster can vary among panels.';


--
-- Name: cm_cluster2panel_mapping pkey__cm_cluster2panel_mapping; Type: CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.cm_cluster2panel_mapping
    ADD CONSTRAINT pkey__cm_cluster2panel_mapping PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__cm_cluster2panel_mapping ON cm_cluster2panel_mapping; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT pkey__cm_cluster2panel_mapping ON sdesign.cm_cluster2panel_mapping IS 'Primary key.';


--
-- Name: cm_cluster2panel_mapping fkey__cm_cluster2panel_mapping__t_cluster; Type: FK CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.cm_cluster2panel_mapping
    ADD CONSTRAINT fkey__cm_cluster2panel_mapping__t_cluster FOREIGN KEY (cluster) REFERENCES sdesign.t_cluster(id);


--
-- Name: CONSTRAINT fkey__cm_cluster2panel_mapping__t_cluster ON cm_cluster2panel_mapping; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT fkey__cm_cluster2panel_mapping__t_cluster ON sdesign.cm_cluster2panel_mapping IS 'Foreign key to table sdesign.t_cluster.';


--
-- Name: cm_cluster2panel_mapping fkey__cm_cluster2panel_mapping__t_panel; Type: FK CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.cm_cluster2panel_mapping
    ADD CONSTRAINT fkey__cm_cluster2panel_mapping__t_panel FOREIGN KEY (panel) REFERENCES sdesign.t_panel(id);


--
-- Name: CONSTRAINT fkey__cm_cluster2panel_mapping__t_panel ON cm_cluster2panel_mapping; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT fkey__cm_cluster2panel_mapping__t_panel ON sdesign.cm_cluster2panel_mapping IS 'Foreign key to table sdesign.t_panel.';


--
-- Name: cm_cluster2panel_mapping_id_seq; Type: SEQUENCE; Schema: sdesign. Owner: -
--

CREATE SEQUENCE sdesign.cm_cluster2panel_mapping_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cm_cluster2panel_mapping_id_seq; Type: SEQUENCE OWNED BY; Schema: sdesign. Owner: -
--

ALTER SEQUENCE sdesign.cm_cluster2panel_mapping_id_seq OWNED BY sdesign.cm_cluster2panel_mapping.id;


--
-- Name: cm_cluster2panel_mapping id; Type: DEFAULT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.cm_cluster2panel_mapping ALTER COLUMN id SET DEFAULT nextval('sdesign.cm_cluster2panel_mapping_id_seq'::regclass);
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
--
-- Name: f_p_plot; Type: TABLE; Schema: sdesign. Owner: -
--

CREATE TABLE sdesign.f_p_plot (
    gid integer NOT NULL,
    pid character varying(50),
    plot character varying(20) NOT NULL,
    geom public.geometry(Point),
    cluster integer,
    coordinates_degraded boolean,
    comment text
);


--
-- Name: TABLE f_p_plot; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON TABLE sdesign.f_p_plot IS 'Table of inventarization plots.';


--
-- Name: COLUMN f_p_plot.gid; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.f_p_plot.gid IS 'Primary key, identifier of the plot DB wide.';


--
-- Name: COLUMN f_p_plot.pid; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.f_p_plot.pid IS 'Identifier of the plot in stratum.';


--
-- Name: COLUMN f_p_plot.plot; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.f_p_plot.plot IS 'Identifier of the plot within cluster.';


--
-- Name: COLUMN f_p_plot.geom; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.f_p_plot.geom IS 'Geometry of the plot.';


--
-- Name: COLUMN f_p_plot.cluster; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.f_p_plot.cluster IS 'Identifier of cluster, foreign key to table t_cluster.';


--
-- Name: COLUMN f_p_plot.coordinates_degraded; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.f_p_plot.coordinates_degraded IS 'Identifier if coordinates of plot are degraded.';


--
-- Name: COLUMN f_p_plot.comment; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.f_p_plot.comment IS 'Optional commentary.';


--
-- Name: f_p_plot pkey__f_p_plot; Type: CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.f_p_plot
    ADD CONSTRAINT pkey__f_p_plot PRIMARY KEY (gid);


--
-- Name: CONSTRAINT pkey__f_p_plot ON f_p_plot; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT pkey__f_p_plot ON sdesign.f_p_plot IS 'Primary key.';


--
-- Name: f_p_plot f_p_plot__pid_cluster_unique; Type: CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.f_p_plot
    ADD CONSTRAINT f_p_plot__pid_cluster_unique UNIQUE (pid, plot, cluster);


--
-- Name: CONSTRAINT f_p_plot__pid_cluster_unique ON f_p_plot; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT f_p_plot__pid_cluster_unique ON sdesign.f_p_plot IS 'Unique key.';


--
-- Name: f_p_plot fkey__f_p_plot__t_cluster; Type: FK CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.f_p_plot
    ADD CONSTRAINT fkey__f_p_plot__t_cluster FOREIGN KEY (cluster) REFERENCES sdesign.t_cluster(id);


--
-- Name: CONSTRAINT fkey__f_p_plot__t_cluster ON f_p_plot; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT fkey__f_p_plot__t_cluster ON sdesign.f_p_plot IS 'Foreign key to table sdesign.t_cluster.';


--
-- Name: idx__f_p_plot__geom; Type: INDEX; Schema: sdesign. Owner: -
--

CREATE INDEX idx__f_p_plot__geom ON sdesign.f_p_plot USING gist (geom);


--
-- Name: idx__f_p_plot__geom; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON INDEX sdesign.idx__f_p_plot__geom IS 'Gist index on column geom.';


--
-- Name: idx__f_p_plot__gid; Type: INDEX; Schema: sdesign. Owner: -
--

CREATE INDEX idx__f_p_plot__gid ON sdesign.f_p_plot USING btree (gid);


--
-- Name: idx__f_p_plot__gid; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON INDEX sdesign.idx__f_p_plot__gid IS 'B-tree index on column gid.';


--
-- Name: idx__f_p_plot__gid__cluster; Type: INDEX; Schema: sdesign. Owner: -
--

CREATE INDEX idx__f_p_plot__gid__cluster ON sdesign.f_p_plot USING btree (gid, cluster);


--
-- Name: idx__f_p_plot__gid__cluster; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON INDEX sdesign.idx__f_p_plot__gid__cluster IS 'B-tree index on columns gid, cluster.';


--
-- Name: idx__f_p_plot__plot; Type: INDEX; Schema: sdesign. Owner: -
--

CREATE INDEX idx__f_p_plot__plot ON sdesign.f_p_plot USING btree (plot);


--
-- Name: idx__f_p_plot__plot; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON INDEX sdesign.idx__f_p_plot__plot IS 'B-tree index on column plot.';


--
-- Name: f_p_plot_gid_seq; Type: SEQUENCE; Schema: sdesign. Owner: -
--

CREATE SEQUENCE sdesign.f_p_plot_gid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: f_p_plot_gid_seq; Type: SEQUENCE OWNED BY; Schema: sdesign. Owner: -
--

ALTER SEQUENCE sdesign.f_p_plot_gid_seq OWNED BY sdesign.f_p_plot.gid;


--
-- Name: f_p_plot gid; Type: DEFAULT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.f_p_plot ALTER COLUMN gid SET DEFAULT nextval('sdesign.f_p_plot_gid_seq'::regclass);
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
--
-- Name: t_inventory_campaign; Type: TABLE; Schema: sdesign. Owner: -
--

CREATE TABLE sdesign.t_inventory_campaign (
    id integer NOT NULL,
    inventory character varying(20) NOT NULL,
    label character varying(120) NOT NULL,
    comment text
);


--
-- Name: TABLE t_inventory_campaign; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON TABLE sdesign.t_inventory_campaign IS 'Table of inventory campaigns or cycles.';


--
-- Name: COLUMN t_inventory_campaign.id; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_inventory_campaign.id IS 'Identifier of inventory campaign, primary key.';


--
-- Name: COLUMN t_inventory_campaign.inventory; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_inventory_campaign.inventory IS 'Character identifier of inventory campaign.';


--
-- Name: COLUMN t_inventory_campaign.label; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_inventory_campaign.label IS 'Label of inventory campaign.';


--
-- Name: COLUMN t_inventory_campaign.comment; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_inventory_campaign.comment IS 'Optional commentary.';


--
-- Name: t_inventory_campaign pkey__t_inventory_campaign; Type: CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_inventory_campaign
    ADD CONSTRAINT pkey__t_inventory_campaign PRIMARY KEY (id);


--
-- Name: COLUMN t_inventory_campaign.id; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_inventory_campaign.id IS 'Primary key, identifier of the inventory campaign.';


--
-- Name: t_inventory_campaign_id_seq; Type: SEQUENCE; Schema: sdesign. Owner: -
--

CREATE SEQUENCE sdesign.t_inventory_campaign_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_inventory_campaign_id_seq; Type: SEQUENCE OWNED BY; Schema: sdesign. Owner: -
--

ALTER SEQUENCE sdesign.t_inventory_campaign_id_seq OWNED BY sdesign.t_inventory_campaign.id;


--
-- Name: t_inventory_campaign id; Type: DEFAULT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_inventory_campaign ALTER COLUMN id SET DEFAULT nextval('sdesign.t_inventory_campaign_id_seq'::regclass);
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
--
-- Name: t_reference_year_set; Type: TABLE; Schema: sdesign. Owner: -
--

CREATE TABLE sdesign.t_reference_year_set (
    id integer NOT NULL,
    inventory_campaign integer NOT NULL,
    reference_year_set character varying(20) NOT NULL,
    reference_date_begin date NOT NULL,
    reference_date_end date NOT NULL,
    label character varying(120) NOT NULL,
    comment text
);


--
-- Name: TABLE t_reference_year_set; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON TABLE sdesign.t_reference_year_set IS 'Table of reference year or season sets (generally speaking representative periods).';


--
-- Name: COLUMN t_reference_year_set.id; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_reference_year_set.id IS 'Identifier of reference period set, primary key.';


--
-- Name: COLUMN t_reference_year_set.inventory_campaign; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_reference_year_set.inventory_campaign IS 'Identifier of inventory campaign, foreign key to table t_inventory_campaign.';


--
-- Name: COLUMN t_reference_year_set.reference_year_set; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_reference_year_set.reference_year_set IS 'Character identifier of reference period set.';


--
-- Name: COLUMN t_reference_year_set.reference_date_begin; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_reference_year_set.reference_date_begin IS 'Date from which the period set begins.';


--
-- Name: COLUMN t_reference_year_set.reference_date_end; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_reference_year_set.reference_date_end IS 'Date to which the period set ends.';


--
-- Name: COLUMN t_reference_year_set.label; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_reference_year_set.label IS 'Label of reference year set.';


--
-- Name: COLUMN t_reference_year_set.comment; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_reference_year_set.comment IS 'Optional commentary.';


--
-- Name: t_reference_year_set pkey__t_reference_year_set; Type: CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_reference_year_set
    ADD CONSTRAINT pkey__t_reference_year_set PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__t_reference_year_set ON t_reference_year_set; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT pkey__t_reference_year_set ON sdesign.t_reference_year_set IS 'Primary key.';


--
-- Name: t_reference_year_set fkey__t_reference_year_set__t_inventory_campaign; Type: FK CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_reference_year_set
    ADD CONSTRAINT fkey__t_reference_year_set__t_inventory_campaign FOREIGN KEY (inventory_campaign) REFERENCES sdesign.t_inventory_campaign(id);


--
-- Name: CONSTRAINT fkey__t_reference_year_set__t_inventory_campaign ON t_reference_year_set; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT fkey__t_reference_year_set__t_inventory_campaign ON sdesign.t_reference_year_set IS 'Foreign key to table sdesign.t_inventory_campaign.';


--
-- Name: t_reference_year_set_id_seq; Type: SEQUENCE; Schema: sdesign. Owner: -
--

CREATE SEQUENCE sdesign.t_reference_year_set_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_reference_year_set_id_seq; Type: SEQUENCE OWNED BY; Schema: sdesign. Owner: -
--

ALTER SEQUENCE sdesign.t_reference_year_set_id_seq OWNED BY sdesign.t_reference_year_set.id;

--
-- Name: t_reference_year_set id; Type: DEFAULT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_reference_year_set ALTER COLUMN id SET DEFAULT nextval('sdesign.t_reference_year_set_id_seq'::regclass);
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
--
-- Name: t_plot_measurement_dates; Type: TABLE; Schema: sdesign. Owner: -
--

CREATE TABLE sdesign.t_plot_measurement_dates (
    id integer NOT NULL,
    plot integer NOT NULL,
    reference_year_set integer NOT NULL,
    measurement_date date
);


--
-- Name: TABLE t_plot_measurement_dates; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON TABLE sdesign.t_plot_measurement_dates IS 'Table of measurement dates of plots.';


--
-- Name: COLUMN t_plot_measurement_dates.id; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_plot_measurement_dates.id IS 'Identifier of mapping record, primary key.';


--
-- Name: COLUMN t_plot_measurement_dates.plot; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_plot_measurement_dates.plot IS 'Identifier of inventory panel, foreign key to table t_panel.';


--
-- Name: COLUMN t_plot_measurement_dates.reference_year_set; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_plot_measurement_dates.reference_year_set IS 'Identifier of reference year set, foreign key to t_reference_year_set.';


--
-- Name: COLUMN t_plot_measurement_dates.measurement_date; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.t_plot_measurement_dates.measurement_date IS 'Measurement date on which the plot was measured.';


--
-- Name: t_plot_measurement_dates pkey__t_plot_measurement_dates; Type: CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_plot_measurement_dates
    ADD CONSTRAINT pkey__t_plot_measurement_dates PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__t_plot_measurement_dates ON t_plot_measurement_dates; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT pkey__t_plot_measurement_dates ON sdesign.t_plot_measurement_dates IS 'Primary key.';


--
-- Name: t_plot_measurement_dates fkey__t_plot_measurement_dates__f_p_plot; Type: FK CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_plot_measurement_dates
    ADD CONSTRAINT fkey__t_plot_measurement_dates__f_p_plot FOREIGN KEY (plot) REFERENCES sdesign.f_p_plot(gid);


--
-- Name: CONSTRAINT fkey__t_plot_measurement_dates__f_p_plot ON t_plot_measurement_dates; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT fkey__t_plot_measurement_dates__f_p_plot ON sdesign.t_plot_measurement_dates IS 'Foreign key to table sdesign.f_p_plot.';


--
-- Name: t_plot_measurement_dates fkey__t_plot_measurement_dates__t_reference_year_set; Type: FK CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_plot_measurement_dates
    ADD CONSTRAINT fkey__t_plot_measurement_dates__t_reference_year_set FOREIGN KEY (reference_year_set) REFERENCES sdesign.t_reference_year_set(id);


--
-- Name: CONSTRAINT fkey__t_plot_measurement_dates__t_reference_year_set ON t_plot_measurement_dates; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT fkey__t_plot_measurement_dates__t_reference_year_set ON sdesign.t_plot_measurement_dates IS 'Foreign key to table sdesign.t_reference_year_set.';


--
-- Name: t_plot_measurement_dates_id_seq; Type: SEQUENCE; Schema: sdesign. Owner: -
--

CREATE SEQUENCE sdesign.t_plot_measurement_dates_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: t_plot_measurement_dates_id_seq; Type: SEQUENCE OWNED BY; Schema: sdesign. Owner: -
--

ALTER SEQUENCE sdesign.t_plot_measurement_dates_id_seq OWNED BY sdesign.t_plot_measurement_dates.id;


--
-- Name: t_plot_measurement_dates id; Type: DEFAULT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.t_plot_measurement_dates ALTER COLUMN id SET DEFAULT nextval('sdesign.t_plot_measurement_dates_id_seq'::regclass);
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
--
-- Name: cm_plot2cluster_config_mapping; Type: TABLE; Schema: sdesign. Owner: -
--

CREATE TABLE sdesign.cm_plot2cluster_config_mapping (
    id integer NOT NULL,
    cluster_configuration integer NOT NULL,
    plot integer NOT NULL
);


--
-- Name: TABLE cm_plot2cluster_config_mapping; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON TABLE sdesign.cm_plot2cluster_config_mapping IS 'Table with mapping between plots and cluster configurations.';


--
-- Name: COLUMN cm_plot2cluster_config_mapping.id; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.cm_plot2cluster_config_mapping.id IS 'Identifier of mapping record, primary key.';


--
-- Name: COLUMN cm_plot2cluster_config_mapping.cluster_configuration; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.cm_plot2cluster_config_mapping.cluster_configuration IS 'Identifier of cluster_configuration, foreign key to table t_cluster_configuration.';


--
-- Name: COLUMN cm_plot2cluster_config_mapping.plot; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.cm_plot2cluster_config_mapping.plot IS 'Identifier of plot, foreign key to table f_p_plot.';


--
-- Name: cm_plot2cluster_config_mapping pkey__cm_plot2cluster_config_mapping; Type: CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.cm_plot2cluster_config_mapping
    ADD CONSTRAINT pkey__cm_plot2cluster_config_mapping PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__cm_plot2cluster_config_mapping ON cm_plot2cluster_config_mapping; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT pkey__cm_plot2cluster_config_mapping ON sdesign.cm_plot2cluster_config_mapping IS 'Primary key.';


--
-- Name: cm_plot2cluster_config_mapping fkey__cm_plot2cluster_config_mapping__f_p_plot; Type: FK CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.cm_plot2cluster_config_mapping
    ADD CONSTRAINT fkey__cm_plot2cluster_config_mapping__f_p_plot FOREIGN KEY (plot) REFERENCES sdesign.f_p_plot(gid);


--
-- Name: CONSTRAINT fkey__cm_plot2cluster_config_mapping__f_p_plot ON cm_plot2cluster_config_mapping; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT fkey__cm_plot2cluster_config_mapping__f_p_plot ON sdesign.cm_plot2cluster_config_mapping IS 'Foreign key to table sdesign.f_p_plot.';


--
-- Name: cm_plot2cluster_config_mapping fkey__cm_plot2cluster_config_mapping__t_cluster_configuration; Type: FK CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.cm_plot2cluster_config_mapping
    ADD CONSTRAINT fkey__cm_plot2cluster_config_mapping__t_cluster_configuration FOREIGN KEY (cluster_configuration) REFERENCES sdesign.t_cluster_configuration(id);


--
-- Name: CONSTRAINT fkey__cm_plot2cluster_config_mapping__t_cluster_configuration ON cm_plot2cluster_config_mapping; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT fkey__cm_plot2cluster_config_mapping__t_cluster_configuration ON sdesign.cm_plot2cluster_config_mapping IS 'Foreign key to table sdesign.t_cluster_configuration.';


--
-- Name: cm_plot2cluster_config_mapping_id_seq; Type: SEQUENCE; Schema: sdesign. Owner: -
--

CREATE SEQUENCE sdesign.cm_plot2cluster_config_mapping_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cm_plot2cluster_config_mapping_id_seq; Type: SEQUENCE OWNED BY; Schema: sdesign. Owner: -
--

ALTER SEQUENCE sdesign.cm_plot2cluster_config_mapping_id_seq OWNED BY sdesign.cm_plot2cluster_config_mapping.id;


--
-- Name: cm_plot2cluster_config_mapping id; Type: DEFAULT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.cm_plot2cluster_config_mapping ALTER COLUMN id SET DEFAULT nextval('sdesign.cm_plot2cluster_config_mapping_id_seq'::regclass);
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
--
-- Name: cm_refyearset2panel_mapping; Type: TABLE; Schema: sdesign. Owner: -
--

CREATE TABLE sdesign.cm_refyearset2panel_mapping (
    id integer NOT NULL,
    panel integer NOT NULL,
    reference_year_set integer NOT NULL
);


--
-- Name: TABLE cm_refyearset2panel_mapping; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON TABLE sdesign.cm_refyearset2panel_mapping IS 'Table with mapping between panels and reference year sets. Representativnes is met both spatially (panel) and within the time scale (ref year set).';


--
-- Name: COLUMN cm_refyearset2panel_mapping.id; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.cm_refyearset2panel_mapping.id IS 'Identifier of mapping record, primary key.';


--
-- Name: COLUMN cm_refyearset2panel_mapping.panel; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.cm_refyearset2panel_mapping.panel IS 'Identifier of inventory panel, foreign key to table t_panel.';


--
-- Name: COLUMN cm_refyearset2panel_mapping.reference_year_set; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON COLUMN sdesign.cm_refyearset2panel_mapping.reference_year_set IS 'Identifier of reference year set, foreign key to t_reference_year_set.';


--
-- Name: cm_refyearset2panel_mapping pkey__cm_refyearset2panel_mapping; Type: CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.cm_refyearset2panel_mapping
    ADD CONSTRAINT pkey__cm_refyearset2panel_mapping PRIMARY KEY (id);


--
-- Name: CONSTRAINT pkey__cm_refyearset2panel_mapping ON cm_refyearset2panel_mapping; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT pkey__cm_refyearset2panel_mapping ON sdesign.cm_refyearset2panel_mapping IS 'Primary key.';


--
-- Name: cm_refyearset2panel_mapping fkey__cm_refyearset2panel_mapping__t_panel; Type: FK CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.cm_refyearset2panel_mapping
    ADD CONSTRAINT fkey__cm_refyearset2panel_mapping__t_panel FOREIGN KEY (panel) REFERENCES sdesign.t_panel(id);


--
-- Name: CONSTRAINT fkey__cm_refyearset2panel_mapping__t_panel ON cm_refyearset2panel_mapping; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT fkey__cm_refyearset2panel_mapping__t_panel ON sdesign.cm_refyearset2panel_mapping IS 'Foreign key to table sdesign.t_panel.';


--
-- Name: cm_refyearset2panel_mapping fkey__cm_refyearset2panel_mapping__t_reference_year_set; Type: FK CONSTRAINT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.cm_refyearset2panel_mapping
    ADD CONSTRAINT fkey__cm_refyearset2panel_mapping__t_reference_year_set FOREIGN KEY (reference_year_set) REFERENCES sdesign.t_reference_year_set(id);


--
-- Name: CONSTRAINT fkey__cm_refyearset2panel_mapping__t_reference_year_set ON cm_refyearset2panel_mapping; Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON CONSTRAINT fkey__cm_refyearset2panel_mapping__t_reference_year_set ON sdesign.cm_refyearset2panel_mapping IS 'Foreign key to table sdesign.t_reference_year_set.';


--
-- Name: cm_refyearset2panel_mapping_id_seq; Type: SEQUENCE; Schema: sdesign. Owner: -
--

CREATE SEQUENCE sdesign.cm_refyearset2panel_mapping_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cm_refyearset2panel_mapping_id_seq; Type: SEQUENCE OWNED BY; Schema: sdesign. Owner: -
--

ALTER SEQUENCE sdesign.cm_refyearset2panel_mapping_id_seq OWNED BY sdesign.cm_refyearset2panel_mapping.id;


--
-- Name: cm_refyearset2panel_mapping id; Type: DEFAULT; Schema: sdesign. Owner: -
--

ALTER TABLE ONLY sdesign.cm_refyearset2panel_mapping ALTER COLUMN id SET DEFAULT nextval('sdesign.cm_refyearset2panel_mapping_id_seq'::regclass);
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
--
-- PostgreSQL database dump complete
--

SELECT pg_catalog.pg_extension_config_dump('sdesign.c_country', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.c_country_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.cm_cluster2panel_mapping', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.cm_cluster2panel_mapping_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.cm_plot2cluster_config_mapping', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.cm_plot2cluster_config_mapping_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.cm_refyearset2panel_mapping', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.cm_refyearset2panel_mapping_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.f_p_plot', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.f_p_plot_gid_seq', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.t_cluster', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.t_cluster_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.t_cluster_configuration', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.t_cluster_configuration_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.t_inventory_campaign', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.t_inventory_campaign_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.t_panel', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.t_panel_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.t_plot_measurement_dates', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.t_plot_measurement_dates_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.t_reference_year_set', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.t_reference_year_set_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.t_strata_set', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.t_strata_set_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.t_stratum', '');
SELECT pg_catalog.pg_extension_config_dump('sdesign.t_stratum_id_seq', '');
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
--
-- Name: fn_create_buffered_geometry(public.geometry, integer, integer); Type: FUNCTION; Schema: sdesign. Owner: -
--

CREATE FUNCTION sdesign.fn_create_buffered_geometry(_geom public.geometry, _tract_type integer, _point_distance_m integer) RETURNS public.geometry
    LANGUAGE plpgsql
    AS $$
DECLARE
	_geom_buff		geometry(MultiPolygon,3035);
	_axis_shift		double precision;
BEGIN

	CASE
	WHEN _tract_type = 100
	THEN
		 RETURN ST_Multi(ST_Buffer(_geom,_point_distance_m));

	WHEN _tract_type = 200
	THEN
		_axis_shift := ( (_point_distance_m^2) / 2.0 )^(1/2.0);


		WITH w_only_shifted AS (
			SELECT
				-- sjednoceni puvodni a posunute geometrie
				ST_Union(
					_geom,
					-- posunuti geometrie o vzdalenost bodu, na opacnou stratnu, nez se vytvari trakt
					ST_Translate(_geom, -_axis_shift, -_axis_shift)
				) AS geom
			)
		,w_original_points AS (
			SELECT
				ST_DumpPoints(geom) AS dp
			FROM
				(SELECT _geom AS geom) AS t1
			)
		,w_dump AS (
			SELECT
				(dp).path, (dp).geom
			FROM
				w_original_points
			)
		,w_coordinates AS (
			SELECT
				st_x(geom) AS x, st_y(geom) AS y, geom, path
			FROM
				w_dump
			)
		,w_shift_points AS (
			SELECT
				path,
				x, y, geom,
				x-_point_distance_m AS x_shift,
				y-_point_distance_m AS y_shift,
				ST_SetSRID(ST_MakePoint(x-_axis_shift, y-_axis_shift),3035) AS geom_shift
			FROM
				w_coordinates
		)
		,w_make_line AS (
			SELECT
				ST_Union(ST_MakeLine(geom,geom_shift)) AS line
			FROM 
				w_shift_points
			)
		,w_union_all AS (
			SELECT
				ST_Union(t1.line, ST_Boundary(t2.geom)) AS all_lines
			FROM
				w_make_line AS t1,
				w_only_shifted AS t2
			)
		SELECT
			ST_Multi(ST_buildarea(all_lines))
		FROM
			w_union_all
		INTO
			_geom_buff;

		RETURN _geom_buff;

	WHEN _tract_type = 300
	THEN

		WITH w_only_shifted AS (
			SELECT
				-- sjednoceni puvodni a posunute geometrie
				ST_Union(
					_geom,
					-- posunuti geometrie o vzdalenost bodu doleva
					ST_Translate(_geom, -(x_shift*_point_distance_m), -(y_shift*_point_distance_m))
				) AS geom_unioned
			FROM
				(SELECT unnest(ARRAY[1,1,0]) AS x_shift, unnest(ARRAY[0,1,1]) AS y_shift) AS t1
			)
		,w_union_only_shifted AS (
			SELECT ST_Union(ST_MakeValid(geom_unioned)) AS geom_unioned
			FROM w_only_shifted
		)
		,w_original_points AS (
			SELECT
				ST_DumpPoints(geom) AS dp
			FROM
				(SELECT _geom AS geom) AS t1
			)
		,w_dump AS (
			SELECT
				(dp).path, (dp).geom
			FROM
				w_original_points
			)
		,w_coordinates AS (
			SELECT
				st_x(geom) AS x, st_y(geom) AS y, geom, path
			FROM
				w_dump
			)
		,w_shift_points AS (
			SELECT
				path,
				geom,
				ST_SetSRID(ST_MakePoint(x-(x_shift[1]*_point_distance_m), y-(y_shift[1]*_point_distance_m)),3035) AS geom_shift_l,
				ST_SetSRID(ST_MakePoint(x-(x_shift[2]*_point_distance_m), y-(y_shift[2]*_point_distance_m)),3035) AS geom_shift_ld,
				ST_SetSRID(ST_MakePoint(x-(x_shift[3]*_point_distance_m), y-(y_shift[3]*_point_distance_m)),3035) AS geom_shift_d
			FROM
				w_coordinates AS t1,
				(SELECT ARRAY[1,1,0] AS x_shift, ARRAY[0,1,1] AS y_shift) AS t2
		)
		,w_make_line AS (
			-- make the square from 4 lines
			SELECT	ST_MakeLine(geom,geom_shift_l) AS line			FROM w_shift_points
			UNION ALL
			SELECT	ST_MakeLine(geom_shift_l,geom_shift_ld) AS line		FROM w_shift_points
			UNION ALL
			SELECT	ST_MakeLine(geom_shift_ld,geom_shift_d) AS line		FROM w_shift_points
			UNION ALL
			SELECT	ST_MakeLine(geom_shift_d,geom) AS line			FROM w_shift_points
			-- diagonal line to prevent st_buildarea to exclude squares
			UNION ALL
			SELECT	ST_MakeLine(geom,geom_shift_ld) AS line			FROM w_shift_points
		)
		,w_union_lines AS (
			SELECT ST_Collect(line) AS line FROM w_make_line
		)
		,w_area_lines AS (
			SELECT
					ST_BuildArea(line) as area_lines
			FROM
					w_union_lines
		)
		,w_area_geoms AS (
			SELECT
				ST_BuildArea(ST_MakeValid(geom_unioned)) AS area_geoms
			FROM
				w_union_only_shifted
		),
		w_union_all AS (
			SELECT
				ST_union(t1.area_lines,t2.area_geoms) AS geom
			FROM
				w_area_lines AS t1,
				w_area_geoms AS t2
		)
		SELECT
			ST_Multi(geom)
		FROM
			w_union_all
		INTO
			_geom_buff;

		RETURN _geom_buff;
	ELSE
		RAISE EXCEPTION 'Not known tract type!';
	END CASE;

END;
$$;


--
-- Name: FUNCTION fn_create_buffered_geometry(_geom public.geometry, _tract_type integer, _point_distance_m integer); Type: COMMENT; Schema: sdesign. Owner: -
--

COMMENT ON FUNCTION sdesign.fn_create_buffered_geometry(_geom public.geometry, _tract_type integer, _point_distance_m integer) IS 'Functions generates a buffered geometry of original geographical domain for specified tract_type.';
---------------------------------------------------------------------------------------------------;


---------------------------------------------------------------------------------------------------;
--
-- Name: fn_import_data(character varying); Type: FUNCTION; Schema: sdesign. Owner: -
--
CREATE OR REPLACE FUNCTION sdesign.fn_import_data(_schema character varying)
RETURNS text
AS
$$
DECLARE
		_res	text;
BEGIN
	-- Data for Name: c_country; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE 'insert into sdesign.c_country(label, description)
	select country, comment from '||_schema||'.countries';


	-- Data for Name: t_cluster; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE 'insert into sdesign.t_cluster(cluster, comment)
	select cluster, comment from '||_schema||'.clusters';


	-- Data for Name: t_cluster_configuration; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE'
	insert into sdesign.t_cluster_configuration(cluster_configuration, cluster_design, cluster_rotation, geom, plots_per_cluster, label, comment)
	select
		cluster_configuration as cluster_configuration,
		cluster_design as cluster_design,
		cluster_rotation as cluster_rotation,
		ST_GeomFromText(cluster_geom)::geometry(MULTIPOINT) as geom,
		case when cluster_design then
			ST_NumGeometries(ST_GeomFromText(cluster_geom)::geometry(MultiPoint))
		else
			1
		end as plots_per_cluster,
		label as label,
		comment as comment
	from '||_schema||'.cluster_configurations';


	-- Data for Name: t_strata_set; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE 'insert into sdesign.t_strata_set(country, strata_set, label, comment)
	select country, strata_set, label, comment from '||_schema||'.strata_sets';


	-- Data for Name: t_stratum; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	insert into sdesign.t_stratum(stratum, area_m2, buffered_area_m2, comment, geom, strata_set, label)
	select
		stratum,
		coalesce(area_m2, ST_Area(ST_GeomFromEWKT(stratum_geometry))) as area_m2,
		buffered_area_m2,
		comment,
		ST_GeomFromEWKT(stratum_geometry) as geom,
		(select id from sdesign.t_strata_set where t_strata_set.strata_set = strata.strata_set) as strata_set,
		label
	from '||_schema||'.strata';


	-- Data for Name: t_panel; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	with w_clusters_data as (
		select panel, count(*) as cluster_count, sum(sampling_weight) as sweight_panel_sum
		from '||_schema||'.clusters
		group by panel order by panel
	)
	, w_plots_data as (
		select panel, count(*) as plot_count
		from '||_schema||'.plots
		group by panel order by panel
	)
	insert into sdesign.t_panel(stratum, panel, cluster_configuration, label, comment, cluster_count, plot_count, sweight_panel_sum)
	select
		(select id from sdesign.t_stratum where t_stratum.stratum = panels.stratum) as stratum,
		panels.panel,
		(select id from sdesign.t_cluster_configuration where t_cluster_configuration.cluster_configuration = panels.cluster_configuration) as cluster_configuration,
		label,
		comment,
		w_clusters_data.cluster_count,
		w_plots_data.plot_count,
		w_clusters_data.sweight_panel_sum
	from '||_schema||'.panels, w_clusters_data, w_plots_data
	where panels.panel = w_clusters_data.panel and panels.panel = w_plots_data.panel
	order by panels.strata_set, panels.stratum, panels.panel';

	with w_stratum as (
		SELECT
			t_stratum.id,
			t_stratum.stratum,
			t_stratum.geom,
			t_cluster_configuration.cluster_configuration,
			CASE	WHEN t_cluster_configuration.cluster_configuration in ( '2p-r', '2p', '4p' )
				THEN
						st_distance(
							ST_GeometryN(t_cluster_configuration.geom, 1),
							ST_GeometryN(t_cluster_configuration.geom, 2))::integer
			END AS cluster_distance
		FROM sdesign.t_stratum
		INNER JOIN sdesign.t_panel on (t_stratum.id = t_panel.stratum)
		INNER JOIN sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id)
		WHERE t_stratum.buffered_area_m2 IS NULL
	)
	, w_stratum_buff as (
		SELECT
			id,
			stratum,
			CASE 	WHEN cluster_configuration = '2p-r' THEN
					sdesign.fn_create_buffered_geometry(geom, 100, cluster_distance)
				WHEN cluster_configuration = '2p' THEN
					sdesign.fn_create_buffered_geometry(geom, 200, cluster_distance)
				WHEN cluster_configuration = '4p' THEN
					sdesign.fn_create_buffered_geometry(geom, 300, cluster_distance)
				WHEN cluster_configuration = '1p' THEN
					NULL
				ELSE
					NULL --sdesign.fn_create_buffered_geometry(geom, 0, NULL::integer)
			END AS buff_geom
		FROM w_stratum
	)
	, w_stratum_buff_area as (
		SELECT
			id,
			stratum,
			buff_geom AS geom_buffered,
			ST_Area(buff_geom) AS area_m2_buffered
		FROM
			w_stratum_buff
		WHERE
			buff_geom IS NOT NULL
	)
	UPDATE sdesign.t_stratum
	SET
		buffered_area_m2 = w_stratum_buff_area.area_m2_buffered,
		geom_buffered = w_stratum_buff_area.geom_buffered
	FROM w_stratum_buff_area
	WHERE w_stratum_buff_area.id = t_stratum.id;


	-- Data for Name: cm_cluster2panel_mapping; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	insert into sdesign.cm_cluster2panel_mapping(panel, cluster, sampling_weight)
	select
		(select id from sdesign.t_panel where t_panel.panel = clusters.panel) as panel,
		(select id from sdesign.t_cluster where t_cluster.cluster = clusters.cluster) as cluster,
		sampling_weight
	from '||_schema||'.clusters';


	-- Data for Name: f_p_plot; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	insert into sdesign.f_p_plot(plot, geom, cluster, coordinates_degraded, comment)
	select
		plot,
		ST_GeomFromEWKT(plot_geometry) as geom,
		(select id from sdesign.t_cluster where t_cluster.cluster = plots.cluster) as cluster,
		coordinates_degraded,
		comment
	from '||_schema||'.plots';


	-- Data for Name: cm_plot2cluster_config_mapping; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	insert into sdesign.cm_plot2cluster_config_mapping (cluster_configuration, plot)
	select
		t_cluster_configuration.id,
		f_p_plot.gid
	from csv.plots
	inner join sdesign.f_p_plot ON (plots.plot = f_p_plot.plot)
	inner join sdesign.t_panel ON plots.panel = t_panel.panel
	inner join sdesign.t_cluster_configuration ON t_cluster_configuration.id = t_panel.cluster_configuration
	group by t_cluster_configuration.id, f_p_plot.gid
	order by t_cluster_configuration.id, f_p_plot.gid;


	-- Data for Name: t_inventory_campaign; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	insert into sdesign.t_inventory_campaign(inventory, label, comment)
	select inventory_campaign, label, comment from '||_schema||'.inventory_campaigns';


	-- Data for Name: t_reference_year_set; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	insert into sdesign.t_reference_year_set(inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment)
	select
		(select id from sdesign.t_inventory_campaign where t_inventory_campaign.inventory = reference_year_sets.inventory_campaign),
		reference_year_set,
		reference_date_begin,
		reference_date_end,
		label,
		comment
	from '||_schema||'.reference_year_sets';


	-- Data for Name: t_plot_measurement_dates; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	EXECUTE '
	insert into sdesign.t_plot_measurement_dates(plot, reference_year_set, measurement_date)
	select
		(select gid from sdesign.f_p_plot where f_p_plot.plot = plot_measurement_dates.plot),
		(select id from sdesign.t_reference_year_set where t_reference_year_set.reference_year_set = plot_measurement_dates.reference_year_set),
		measurement_date
	from '||_schema||'.plot_measurement_dates';


	-- Data for Name: cm_refyearset2panel_mapping; Type: TABLE DATA; Schema: sdesign; Owner: vagrant
	with w_data as (
		select
			t_panel.id as t_panel__id,
			t_reference_year_set.id as t_reference_year_set__id
		from sdesign.t_panel
		inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
		inner join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping.cluster
		inner join sdesign.f_p_plot ON f_p_plot.cluster = t_cluster.id
		inner join sdesign.t_plot_measurement_dates ON t_plot_measurement_dates.plot = f_p_plot.gid
		inner join sdesign.t_reference_year_set ON t_reference_year_set.id = t_plot_measurement_dates.reference_year_set
		group by t_panel.id, t_reference_year_set.id
		order by t_panel.id, t_reference_year_set.id
	)
	insert into sdesign.cm_refyearset2panel_mapping (panel, reference_year_set)
	select t_panel__id, t_reference_year_set__id from w_data;


	-- Name: c_country_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.c_country_id_seq', (select max(id) from sdesign.c_country), true);

	-- Name: cm_cluster2panel_mapping_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.cm_cluster2panel_mapping_id_seq', (select max(id) from sdesign.cm_cluster2panel_mapping), true);

	-- Name: cm_plot2cluster_config_mapping_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.cm_plot2cluster_config_mapping_id_seq', (select max(id) from sdesign.cm_plot2cluster_config_mapping), true);

	-- Name: cm_refyearset2panel_mapping_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.cm_refyearset2panel_mapping_id_seq', (select max(id) from sdesign.cm_refyearset2panel_mapping), true);

	-- Name: f_p_plot_gid_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.f_p_plot_gid_seq', (select max(gid) from sdesign.f_p_plot), true);

	-- Name: t_cluster_configuration_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_cluster_configuration_id_seq', (select max(id) from sdesign.t_cluster_configuration), true);

	-- Name: t_cluster_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_cluster_id_seq', (select max(id) from sdesign.t_cluster), true);

	-- Name: t_inventory_campaign_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_inventory_campaign_id_seq', (select max(id) from sdesign.t_inventory_campaign), true);

	-- Name: t_panel_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_panel_id_seq', (select max(id) from sdesign.t_panel), true);

	-- Name: t_plot_measurement_dates_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_plot_measurement_dates_id_seq', (select max(id) from sdesign.t_plot_measurement_dates), true);

	-- Name: t_reference_year_set_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_reference_year_set_id_seq', (select max(id) from sdesign.t_reference_year_set), true);

	-- Name: t_strata_set_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_strata_set_id_seq', (select max(id) from sdesign.t_strata_set), true);

	-- Name: t_stratum_id_seq; Type: SEQUENCE SET; Schema: sdesign; Owner: vagrant
	PERFORM pg_catalog.setval('sdesign.t_stratum_id_seq', (select max(id) from sdesign.t_stratum), true);

	_res := 'Import is comleted.';

	return _res;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION sdesign.fn_import_data(character varying) IS
'The function imports data from csv tables into tables that are in the sdesign schema.';
---------------------------------------------------------------------------------------------------;





