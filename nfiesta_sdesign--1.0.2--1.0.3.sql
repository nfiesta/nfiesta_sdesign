--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 sdesign.c_country TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 sdesign.c_country_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 sdesign.t_strata_set TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 sdesign.t_strata_set_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 sdesign.t_stratum TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 sdesign.t_stratum_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 sdesign.t_panel TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 sdesign.t_panel_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 sdesign.t_cluster_configuration TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 sdesign.t_cluster_configuration_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 sdesign.cm_refyearset2panel_mapping TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 sdesign.cm_refyearset2panel_mapping_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 sdesign.cm_cluster2panel_mapping TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 sdesign.cm_cluster2panel_mapping_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 sdesign.cm_plot2cluster_config_mapping TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 sdesign.cm_plot2cluster_config_mapping_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 sdesign.t_cluster TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 sdesign.t_cluster_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 sdesign.f_p_plot TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 sdesign.f_p_plot_gid_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 sdesign.t_plot_measurement_dates TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 sdesign.t_plot_measurement_dates_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 sdesign.t_reference_year_set TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 sdesign.t_reference_year_set_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 sdesign.t_inventory_campaign TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 sdesign.t_inventory_campaign_id_seq TO app_nfiesta;
