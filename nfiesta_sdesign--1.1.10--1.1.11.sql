--
-- Copyright 2021, 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <function name="ftrg__stratum_clusterconf_comb" schema="sdesign" src="functions/trg__stratum_clusterconf_comb.sql">
CREATE OR REPLACE FUNCTION sdesign.fn_stratum_clusterconf_comb()
  RETURNS trigger AS
$BODY$
    BEGIN
	IF not (
	select 
	cardinality(array_agg(distinct stratum)) = 1 
from sdesign.t_panel
where cluster_configuration = new.cluster_configuration
group by cluster_configuration) THEN
	        RAISE EXCEPTION 'fn_stratum_clusterconf_comb -- INS / UPD -- it is not possible to link one cluster_configuration to multiple strata (cluster_configuration = % to be used in strata % (stratum % is being inserted or updated)).', new.cluster_configuration, (select array_agg(distinct stratum) from sdesign.t_panel where cluster_configuration = new.cluster_configuration), new.stratum;
	END IF;
	RETURN NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

drop trigger if exists trg__stratum_clusterconf_comb ON sdesign.t_panel;
CREATE TRIGGER trg__stratum_clusterconf_comb
AFTER INSERT OR UPDATE
ON sdesign.t_panel
FOR EACH ROW
EXECUTE PROCEDURE sdesign.fn_stratum_clusterconf_comb();

-- </function>
