--
-- Copyright 2021, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- adding indexes to improve speed of single-phase estimates calculation:

-- 207 estimates of ratio in CZ NUTS1+2+3 using 12 cores, standard time 7 seconds increases to 23 seconds without this index
DROP INDEX IF EXISTS sdesign.cm_cluster2panel_mapping_cluster_idx; -- this preexisting index does not include cluster and sampling_weight
DROP INDEX IF EXISTS sdesign.idx__cm_cluster2panel_mapping__panel;
CREATE INDEX idx__cm_cluster2panel_mapping__panel ON sdesign.cm_cluster2panel_mapping USING btree (panel) INCLUDE (cluster, sampling_weight_ha);

-- 207 estimates of ratio in CZ NUTS1+2+3 using 12 cores, standard time 7 seconds increases to 300 seconds without this index
DROP INDEX IF EXISTS sdesign.idx__f_p_plot__cluster;
CREATE INDEX idx__f_p_plot__cluster ON sdesign.f_p_plot USING btree (cluster) INCLUDE (gid);