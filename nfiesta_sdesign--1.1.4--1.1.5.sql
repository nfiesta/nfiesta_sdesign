--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <function name="fn_check_sweight_panel_sum" schema="sdesign" src="functions/fn_check_sweight_panel_sum.sql">
--drop function sdesign.fn_check_sweight_panel_sum() cascade;
CREATE OR REPLACE FUNCTION sdesign.fn_check_sweight_panel_sum() RETURNS TRIGGER AS $src$
DECLARE
	_output json;
BEGIN
	_output := (with w_data as (
		select
			t_panel.panel,
			round((case when t_cluster_configuration.cluster_design then t_stratum.frame_area_ha else t_stratum.area_ha end)::numeric, 6) as area,
			round(new.sweight_panel_sum::numeric, 6) as sweight_sum,
			round(sum(cm_cluster2panel_mapping.sampling_weight_ha)::numeric, 6) as sampling_weight_ha_sum
		from sdesign.t_panel
		join sdesign.cm_cluster2panel_mapping ON t_panel.id = cm_cluster2panel_mapping.panel
		join sdesign.t_cluster_configuration ON t_cluster_configuration.id = t_panel.cluster_configuration
		join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
		where t_panel.id = new.id
		group by t_panel.panel, t_panel.sweight_panel_sum, t_cluster_configuration.cluster_design, t_stratum.area_ha, t_stratum.frame_area_ha
		order by t_panel.panel
	)
	select
		json_build_object('panel', panel, 'area', area, 'sweight_sum', sweight_sum, 'sampling_weight_ha_sum', sampling_weight_ha_sum)
	from w_data
	);

	IF NOT ((abs((_output->>'area')::double precision/(_output->>'sweight_sum')::double precision - 1.0)) <= 1e-10) OR (_output->>'sweight_sum' != _output->>'sampling_weight_ha_sum')
		THEN RAISE EXCEPTION 'fn_check_sweight_panel_sum: sum of sampling weights is not equal to stratum (frame) area! (panel: %, stratum area: %, t_panel.sweight_panel_sum: %, sum(cm_cluster2panel_mapping.sampling_weight_ha): %)', 
			_output->>'panel', _output->>'area', _output->>'sweight_sum', _output->>'sampling_weight_ha_sum';
	END IF;
RETURN NULL; -- result is ignored since this is an AFTER trigger
END;
$src$ LANGUAGE plpgsql;

drop trigger if exists trg__panel__check_threshold__ins on sdesign.t_panel;
CREATE CONSTRAINT TRIGGER trg__panel__check_threshold__ins
	AFTER INSERT ON sdesign.t_panel
	DEFERRABLE INITIALLY DEFERRED
	FOR EACH ROW EXECUTE FUNCTION sdesign.fn_check_sweight_panel_sum();

drop trigger if exists trg__panel__check_threshold__upd on sdesign.t_panel;
CREATE CONSTRAINT TRIGGER trg__panel__check_threshold__upd
	AFTER UPDATE ON sdesign.t_panel
	DEFERRABLE INITIALLY DEFERRED
	FOR EACH ROW EXECUTE FUNCTION sdesign.fn_check_sweight_panel_sum();

-- </function>

