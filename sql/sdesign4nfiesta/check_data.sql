--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--
---------------------------------------------------------------------------------------------------
SELECT label, description 
    FROM sdesign.c_country ORDER BY label;

SELECT c_country.label, strata_set, t_strata_set.label, comment 
    FROM sdesign.t_strata_set 
    INNER JOIN sdesign.c_country ON (t_strata_set.country = c_country.id)
    ORDER BY c_country.label, strata_set, t_strata_set.label, comment;

SELECT stratum, round(area_ha::numeric,5) AS area_m2, t_strata_set.strata_set, t_stratum.label 
    FROM sdesign.t_stratum 
    INNER JOIN sdesign.t_strata_set ON (t_stratum.strata_set = t_strata_set.id)
    ORDER BY stratum, area_ha, t_strata_set.strata_set, t_stratum.label;

SELECT t_stratum.stratum, panel, t_cluster_configuration.cluster_configuration, t_panel.label, cluster_count, plot_count, sweight_panel_sum 
    FROM sdesign.t_panel 
    INNER JOIN sdesign.t_stratum ON (t_panel.stratum = t_stratum.id)
    INNER JOIN sdesign.t_cluster_configuration ON (t_panel.cluster_configuration = t_cluster_configuration.id)
    ORDER BY t_stratum.stratum, panel, t_cluster_configuration.cluster_configuration, t_panel.label, cluster_count, plot_count, sweight_panel_sum;

SELECT cluster_configuration, cluster_design, cluster_rotation, plots_per_cluster, label, round(frame_area_ha::numeric,3) AS frame_area_ha 
    FROM sdesign.t_cluster_configuration ORDER BY cluster_configuration, cluster_design, cluster_rotation, plots_per_cluster, label, frame_area_ha;

SELECT cluster 
    FROM sdesign.t_cluster ORDER BY cluster;

SELECT t_panel.panel, t_cluster.cluster, sampling_weight_ha 
    FROM sdesign.cm_cluster2panel_mapping 
    INNER JOIN sdesign.t_panel ON (cm_cluster2panel_mapping.panel = t_panel.id)
    INNER JOIN sdesign.t_cluster ON (cm_cluster2panel_mapping.cluster = t_cluster.id)
    ORDER BY t_panel.panel, t_cluster.cluster, sampling_weight_ha;

SELECT plot, t_cluster.cluster 
    FROM sdesign.f_p_plot 
    INNER JOIN sdesign.t_cluster ON (f_p_plot.cluster = t_cluster.id)
    ORDER BY plot, t_cluster.cluster;

SELECT t_cluster_configuration.cluster_configuration, f_p_plot.plot 
    FROM sdesign.cm_plot2cluster_config_mapping
    INNER JOIN sdesign.f_p_plot ON (cm_plot2cluster_config_mapping.plot = f_p_plot.gid) 
    INNER JOIN sdesign.t_cluster_configuration ON (cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
    ORDER BY t_cluster_configuration.cluster_configuration, f_p_plot.plot;

SELECT t_inventory_campaign.inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, t_reference_year_set.label, reference_year_set_begin, reference_year_set_end 
    FROM sdesign.t_reference_year_set
    INNER JOIN sdesign.t_inventory_campaign ON (t_reference_year_set.inventory_campaign = t_inventory_campaign.id) 
    ORDER BY t_inventory_campaign.inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, t_reference_year_set.label;

SELECT t_panel.panel, t_reference_year_set.reference_year_set 
    FROM sdesign.cm_refyearset2panel_mapping 
    INNER JOIN sdesign.t_panel ON (cm_refyearset2panel_mapping.panel = t_panel.id)
    INNER JOIN sdesign.t_reference_year_set ON (cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id)
    ORDER BY t_panel.panel, t_reference_year_set.reference_year_set;

SELECT inventory_campaign, label, comment 
    FROM sdesign.t_inventory_campaign ORDER BY inventory_campaign, label, comment;

SELECT f_p_plot.plot, t_reference_year_set.reference_year_set
    FROM sdesign.t_plot_measurement_dates
    INNER JOIN sdesign.f_p_plot ON (t_plot_measurement_dates.plot = f_p_plot.gid) 
    INNER JOIN sdesign.t_reference_year_set ON (t_plot_measurement_dates.reference_year_set = t_reference_year_set.id)
    ORDER BY plot, t_reference_year_set.reference_year_set;
