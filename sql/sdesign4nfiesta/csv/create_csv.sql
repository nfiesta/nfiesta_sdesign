-- script for the export of csvs from the database (some kind of reverse ingeneering)
-- this will export the data at the last state (after the second ETL - fst_data_generations)

-- temporary views are alive for the current session (not transaction)
\set srcdir `echo $SRC_DIR`

CREATE OR REPLACE TEMPORARY VIEW v_countries AS (
	select
		id,
		label,
		description
	from sdesign.c_country
	order by id);

\set afile :srcdir '/sql/csv/countries.csv'
COPY (SELECT * FROM v_countries) TO STDOUT DELIMITER ';' CSV HEADER \g :afile


CREATE OR REPLACE TEMPORARY VIEW v_strata_sets AS (
	select
		c_country.label as country,
		t_strata_set.strata_set,
		t_strata_set.label,
		t_strata_set.comment
	from sdesign.t_strata_set
	inner join sdesign.c_country on (t_strata_set.country = c_country.id)
	order by country, strata_set);

\set afile :srcdir '/sql/csv/strata_sets.csv'
COPY (SELECT * FROM v_strata_sets) TO STDOUT DELIMITER ';' CSV HEADER \g :afile

CREATE OR REPLACE TEMPORARY VIEW v_strata AS (
	select
		c_country.label as country,
		t_strata_set.strata_set,
		t_stratum.stratum,
		t_stratum.label,
		--st_asewkt(st_transform(t_stratum.geom, 3035)) as stratum_geometry,
		st_asewkt(st_transform(t_stratum.geom, 5514)) as stratum_geometry,
		area_ha,
		t_stratum.comment
	from sdesign.t_stratum
	inner join sdesign.t_strata_set on t_strata_set.id = t_stratum.strata_set
	inner join sdesign.c_country on (t_strata_set.country = c_country.id)
	order by country, strata_set, stratum);
 
\set afile :srcdir '/sql/csv/strata.csv'
COPY (SELECT * FROM v_strata) TO STDOUT DELIMITER ';' CSV HEADER \g :afile

CREATE OR REPLACE TEMPORARY VIEW v_cluster_configurations AS (
	select
		c_country.label as country,
		t_cluster_configuration.cluster_configuration,
		t_cluster_configuration.label,
		t_cluster_configuration.cluster_design,
		st_astext(t_cluster_configuration.geom) as cluster_geom,
		t_cluster_configuration.cluster_rotation,
		t_cluster_configuration.comment,
		t_cluster_configuration.frame_area_ha
	from sdesign.t_cluster_configuration
	inner join sdesign.t_panel ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
	inner join sdesign.t_strata_set ON t_strata_set.id = t_stratum.strata_set
	inner join sdesign.c_country ON c_country.id = t_strata_set.country
	group by
		c_country.id,
		t_cluster_configuration.cluster_configuration,
		t_cluster_configuration.label,
		t_cluster_configuration.cluster_design,
		t_cluster_configuration.cluster_rotation,
		st_astext(t_cluster_configuration.geom),
		t_cluster_configuration.comment,
		t_cluster_configuration.id
	order by country, cluster_configuration); 

\set afile :srcdir '/sql/csv/cluster_configurations.csv'
COPY (SELECT * FROM v_cluster_configurations) TO STDOUT DELIMITER ';' CSV HEADER \g :afile

CREATE OR REPLACE TEMPORARY VIEW v_panels AS (
	select
		c_country.label as country,
		t_strata_set.strata_set,
		t_stratum.stratum,
		t_cluster_configuration.cluster_configuration,
		t_panel.panel,
		t_panel.label,
		t_panel.comment
	from sdesign.t_cluster_configuration
	inner join sdesign.t_panel ON t_panel.cluster_configuration = t_cluster_configuration.id
	inner join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
	inner join sdesign.t_strata_set ON t_strata_set.id = t_stratum.strata_set
	inner join sdesign.c_country ON c_country.id = t_strata_set.country
	order by country, strata_set, stratum, panel, cluster_configuration); 

\set afile :srcdir '/sql/csv/panels.csv'
COPY (SELECT * FROM v_panels) TO STDOUT DELIMITER ';' CSV HEADER \g :afile

CREATE OR REPLACE TEMPORARY VIEW v_clusters AS (
	select
		c_country.label as country,
		t_strata_set.strata_set,
		t_stratum.stratum,
		t_panel.panel,
		t_cluster.cluster,
		cm_cluster2panel_mapping.sampling_weight_ha,
		t_cluster.comment
	from sdesign.t_panel
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
	inner join sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping.cluster
	inner join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
	inner join sdesign.t_strata_set ON t_strata_set.id = t_stratum.strata_set
	inner join sdesign.c_country ON c_country.id = t_strata_set.country
	order by country, strata_set, stratum, panel, cluster);
 
\set afile :srcdir '/sql/csv/clusters.csv'
COPY (SELECT * FROM v_clusters) TO STDOUT DELIMITER ';' CSV HEADER \g :afile

CREATE OR REPLACE TEMPORARY VIEW v_plots AS (
	select
		c_country.label as country,
		t_strata_set.strata_set,
		t_stratum.stratum,
		t_panel.panel,
		t_cluster.cluster,
		f_p_plot.plot,
		ST_AsEWKT(f_p_plot.geom) as plot_geometry,
		f_p_plot.coordinates_degraded,
		f_p_plot.comment
	from sdesign.f_p_plot
	inner join sdesign.cm_plot2cluster_config_mapping ON f_p_plot.gid = cm_plot2cluster_config_mapping.plot
	inner join sdesign.t_cluster ON t_cluster.id = f_p_plot.cluster
	inner join sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.cluster = t_cluster.id
	inner join sdesign.t_panel ON t_panel.id = cm_cluster2panel_mapping.panel AND cm_plot2cluster_config_mapping.cluster_configuration = t_panel.cluster_configuration
	inner join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
	inner join sdesign.t_strata_set ON t_strata_set.id = t_stratum.strata_set
	inner join sdesign.c_country ON c_country.id = t_strata_set.country
	order by country, strata_set, stratum, panel, cluster, plot); 

\set afile :srcdir '/sql/csv/plots.csv'
COPY (SELECT * FROM v_plots) TO STDOUT DELIMITER ';' CSV HEADER \g :afile

CREATE OR REPLACE TEMPORARY VIEW v_inventory_campaigns AS (
	select
		c_country.label as country,
		t_inventory_campaign.inventory_campaign,
		t_inventory_campaign.label,
		t_inventory_campaign.status_variables,
		t_inventory_campaign.comment
	from sdesign.t_inventory_campaign
	inner join sdesign.t_reference_year_set ON t_reference_year_set.inventory_campaign = t_inventory_campaign.id
	inner join sdesign.cm_refyearset2panel_mapping ON cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
	inner join sdesign.t_panel ON t_panel.id = cm_refyearset2panel_mapping.panel
	inner join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
	inner join sdesign.t_strata_set ON t_strata_set.id = t_stratum.strata_set
	inner join sdesign.c_country ON c_country.id = t_strata_set.country
	group by
		c_country.id,
		t_inventory_campaign.inventory_campaign,
		t_inventory_campaign.label,
		t_inventory_campaign.status_variables,
		t_inventory_campaign.comment
	order by country, t_inventory_campaign.inventory_campaign); 

\set afile :srcdir '/sql/csv/inventory_campaigns.csv'
COPY (SELECT * FROM v_inventory_campaigns) TO STDOUT DELIMITER ';' CSV HEADER \g :afile

CREATE OR REPLACE TEMPORARY VIEW v_reference_year_sets AS (
	select
		c_country.label as country,
		t_inventory_campaign.inventory_campaign,
		t_reference_year_set.reference_year_set,
		t_reference_year_set.label,
		t_reference_year_set.status_variables,
		t_reference_year_set.reference_date_begin,
		t_reference_year_set.reference_date_end,
		(select ic.inventory_campaign 
			from sdesign.t_inventory_campaign as ic 
			inner join sdesign.t_reference_year_set as t on (ic.id = t.inventory_campaign) 
			where t.id = t_reference_year_set.reference_year_set_begin) as inventory_campaign_begin,
		(select reference_year_set from sdesign.t_reference_year_set as t where t.id = t_reference_year_set.reference_year_set_begin) as reference_year_set_begin,
		(select ic.inventory_campaign 
			from sdesign.t_inventory_campaign as ic 
			inner join sdesign.t_reference_year_set as t on (ic.id = t.inventory_campaign) 
			where t.id = t_reference_year_set.reference_year_set_end) as inventory_campaign_end,
		(select reference_year_set from sdesign.t_reference_year_set as t where t.id = t_reference_year_set.reference_year_set_end) as reference_year_set_end,
		t_reference_year_set.comment
	from sdesign.t_inventory_campaign
	inner join sdesign.t_reference_year_set ON t_reference_year_set.inventory_campaign = t_inventory_campaign.id
	inner join sdesign.cm_refyearset2panel_mapping ON cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
	inner join sdesign.t_panel ON t_panel.id = cm_refyearset2panel_mapping.panel
	inner join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
	inner join sdesign.t_strata_set ON t_strata_set.id = t_stratum.strata_set
	inner join sdesign.c_country ON c_country.id = t_strata_set.country
	group by
		c_country.id,
		t_inventory_campaign.inventory_campaign,
		t_reference_year_set.reference_year_set,
		t_reference_year_set.label,
		t_reference_year_set.status_variables,
		t_reference_year_set.reference_date_begin,
		t_reference_year_set.reference_date_end,
		t_reference_year_set.comment,
		t_reference_year_set.reference_year_set_begin,
		t_reference_year_set.reference_year_set_end
	order by country, t_reference_year_set.reference_year_set); 

\set afile :srcdir '/sql/csv/reference_year_sets.csv'
COPY (SELECT * FROM v_reference_year_sets) TO STDOUT DELIMITER ';' CSV HEADER \g :afile

CREATE OR REPLACE TEMPORARY VIEW v_plot_measurement_dates AS (
	select
		c_country.label as country,
		t_inventory_campaign.inventory_campaign,
		t_reference_year_set.reference_year_set,
		t_strata_set.strata_set,
		t_stratum.stratum,
		t_panel.panel,
		t_cluster.cluster,
		f_p_plot.plot,
		t_plot_measurement_dates.measurement_date as measurement_date,
		t_plot_measurement_dates.comment as comment
	from sdesign.t_reference_year_set
	inner join sdesign.cm_refyearset2panel_mapping ON cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
	inner join sdesign.t_panel ON t_panel.id = cm_refyearset2panel_mapping.panel
	inner join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
	inner join sdesign.t_strata_set ON t_strata_set.id = t_stratum.strata_set
	inner join sdesign.c_country ON c_country.id = t_strata_set.country
	inner join sdesign.t_inventory_campaign ON t_inventory_campaign.id = t_reference_year_set.inventory_campaign
	inner join sdesign.cm_cluster2panel_mapping ON t_panel.id = cm_cluster2panel_mapping.panel
	inner join sdesign.t_cluster ON cm_cluster2panel_mapping.cluster = t_cluster.id
	inner join sdesign.f_p_plot ON t_cluster.id = f_p_plot.cluster
	inner join sdesign.t_plot_measurement_dates ON f_p_plot.gid = t_plot_measurement_dates.plot AND t_reference_year_set.id = t_plot_measurement_dates.reference_year_set
	where t_reference_year_set.reference_year_set_begin is null and t_reference_year_set.reference_year_set_end is null
	order by t_inventory_campaign.inventory_campaign, t_reference_year_set.reference_year_set, t_panel.panel, t_cluster.cluster, f_p_plot.plot, t_plot_measurement_dates.comment);
 
\set afile :srcdir '/sql/csv/plot_measurement_dates.csv'
COPY (SELECT * FROM v_plot_measurement_dates) TO STDOUT DELIMITER ';' CSV HEADER \g :afile

CREATE OR REPLACE TEMPORARY VIEW v_refyearset2panel AS (
	select
		c_country.label as country,
		t_inventory_campaign.inventory_campaign,
		t_reference_year_set.reference_year_set,
		t_strata_set.strata_set,
		t_stratum.stratum,
		t_panel.panel,
		cm_refyearset2panel_mapping.comment
	from sdesign.t_reference_year_set
	inner join sdesign.cm_refyearset2panel_mapping ON cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
	inner join sdesign.t_panel ON t_panel.id = cm_refyearset2panel_mapping.panel
	inner join sdesign.t_stratum ON t_stratum.id = t_panel.stratum
	inner join sdesign.t_strata_set ON t_strata_set.id = t_stratum.strata_set
	inner join sdesign.c_country ON c_country.id = t_strata_set.country
	inner join sdesign.t_inventory_campaign ON t_inventory_campaign.id = t_reference_year_set.inventory_campaign
	order by c_country.label, t_inventory_campaign.inventory_campaign, t_reference_year_set.reference_year_set, t_strata_set.strata_set, t_stratum.stratum, t_panel.panel);
 
\set afile :srcdir '/sql/csv/refyearset2panel.csv'
COPY (SELECT * FROM v_refyearset2panel) TO STDOUT DELIMITER ';' CSV HEADER \g :afile
