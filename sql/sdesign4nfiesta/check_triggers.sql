/*
select * from sdesign.t_inventory_campaign;
 id | inventory_campaign |     label     |                                      comment                                      | status_variables
----+--------------------+---------------+-----------------------------------------------------------------------------------+------------------
  1 | NFI1               | NFI1          | 1st cycle of National Forest Inventory (2001 - 2004)                              | t
  2 | NFI2               | NFI2          | 2nd cycle of National Forest Inventory (2011 - 2015)                              | t
  3 | NFI2-3_change      | NFI2-3_change | change between s2a panels in NFI2 and NFI3                                        | f
  4 | NFI2sgrid1         | NFI2sgrid1    | 2nd cycle of National Forest Inventory (remeasurement of NFI1 grid) (2011 - 2014) | t
  5 | NFI3               | NFI3          | 3rd cycle of National Forest Inventory (od 2016)                                  | t
(5 rows)
*/

--OK
insert into sdesign.t_inventory_campaign
    (id, inventory_campaign, label, status_variables, comment)
    values
    (60, 'NFI1-2', 'NFI1-2', false, 'change beteween NFI1 and NFI2'),
    (70, 'NFI1', 'NFI1', true, 'state NFI1');
delete from sdesign.t_inventory_campaign where id in (60, 70);

/*
select * from sdesign.t_reference_year_set;
 id | inventory_campaign | reference_year_set | reference_date_begin | reference_date_end |           label           | comment | reference_year_set_begin | reference_year_set_end | status_variables
----+--------------------+--------------------+----------------------+--------------------+---------------------------+---------+--------------------------+------------------------+------------------
  1 |                  1 | 2001 - 2004        | 2001-01-01           | 2004-12-31         | NFI1 (2001-2004)          |         |                          |                        | t
  2 |                  4 | 2011 - 2014        | 2011-01-01           | 2014-12-31         | NFI2 (2011-2014)          |         |                          |                        | t
  3 |                  2 | 2011 - 2015        | 2011-01-01           | 2015-12-31         | NFI2 (2011-2015)          |         |                          |                        | t
  4 |                  5 | 2016 - 2020        | 2016-01-01           | 2020-12-31         | NFI3 (2016-2020)          |         |                          |                        | t
  5 |                  3 | 2011 - 2020        |                      |                    | NFI2-3_change (2011-2020) |         |                        3 |                      4 | f
(5 rows)
*/

--OK
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end, status_variables)
    values
    (60, 5, '2016 - 2020', '2016-01-01', '2020-12-31', 'NFI3 (2016-2020)', NULL, NULL, NULL, true),
    (70, 3, '2011 - 2020', NULL, NULL, 'NFI2-3_change (2011-2020)', NULL, 3, 4, false);
delete from sdesign.t_reference_year_set where id in (60, 70);

----------------------------------check constraints
--reference_date_begin | reference_date_end
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end, status_variables)
    values
    (6, 5, '2016 - 2020', '2016-01-01', NULL, 'NFI3 (2016-2020)', NULL, NULL, NULL, true);
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end, status_variables)
    values
    (6, 5, '2016 - 2020', NULL, '2020-12-31', 'NFI3 (2016-2020)', NULL, NULL, NULL, true);
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end, status_variables)
    values
    (6, 5, '2016 - 2020', NULL, NULL, 'NFI3 (2016-2020)', NULL, NULL, NULL, true);
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end, status_variables)
    values
    (6, 5, '2016 - 2020', '2020-12-31', '2016-01-01', 'NFI3 (2016-2020)', NULL, NULL, NULL, true);
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end, status_variables)
    values
    (6, 5, '2016 - 2020', '2020-12-31', '2020-12-31', 'NFI3 (2016-2020)', NULL, NULL, NULL, true);
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end, status_variables)
    values
    (7, 3, '2011 - 2020', '2016-01-01', NULL, 'NFI2-3_change (2011-2020)', NULL, 3, 4, false);
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end, status_variables)
    values
    (7, 3, '2011 - 2020', NULL, '2020-12-31', 'NFI2-3_change (2011-2020)', NULL, 3, 4, false);
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end, status_variables)
    values
    (7, 3, '2011 - 2020', '2016-01-01', '2020-12-31', 'NFI2-3_change (2011-2020)', NULL, 3, 4, false);

--reference_year_set_begin | reference_year_set_end

insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end, status_variables)
    values
    (7, 3, '2011 - 2020', NULL, NULL, 'NFI2-3_change (2011-2020)', NULL, 3, NULL, false);
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end, status_variables)
    values
    (7, 3, '2011 - 2020', NULL, NULL, 'NFI2-3_change (2011-2020)', NULL, NULL, 4, false);
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end, status_variables)
    values
    (7, 3, '2011 - 2020', NULL, NULL, 'NFI2-3_change (2011-2020)', NULL, NULL, NULL, false);
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end, status_variables)
    values
    (7, 3, '2011 - 2020', NULL, NULL, 'NFI2-3_change (2011-2020)', NULL, 4, 4, false);
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end, status_variables)
    values
    (6, 5, '2016 - 2020', '2016-01-01', '2020-12-31', 'NFI3 (2016-2020)', NULL, NULL, 4, true);
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end, status_variables)
    values
    (6, 5, '2016 - 2020', '2016-01-01', '2020-12-31', 'NFI3 (2016-2020)', NULL, 3, NULL, true);
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end, status_variables)
    values
    (6, 5, '2016 - 2020', '2016-01-01', '2020-12-31', 'NFI3 (2016-2020)', NULL, 3, 4, true);

----------------------------------trigger
--reference_year_set_begin and reference_year_set_end have to be "status" (or NULL -- checked by check constraint)
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end, status_variables)
    values
    (7, 3, '2011 - 2020', NULL, NULL, 'NFI2-3_change (2011-2020)', NULL, 3, 5, false);

insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end, status_variables)
    values
    (7, 3, '2011 - 2020', NULL, NULL, 'NFI2-3_change (2011-2020)', NULL, 5, 3, false);

--change refyearset has to reference change inventory campaign and status refyearset has to reference status inventory campaign
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end, status_variables)
    values
    (6, 3, '2016 - 2020', '2016-01-01', '2020-12-31', 'NFI3 (2016-2020)', NULL, NULL, NULL, true);
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end, status_variables)
    values
    (7, 5, '2011 - 2020', NULL, NULL, 'NFI2-3_change (2011-2020)', NULL, 3, 4, false);

--reference_year_set_begin and reference_year_set_end must have different inventory campaigns
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end, status_variables)
    values
    (60, 5, '2016 - 2020', '2016-01-01', '2020-12-31', 'NFI3 (2016-2020)', 'test campaign 5', NULL, NULL, true);

insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end, status_variables)
    values
    (7, 3, '2011 - 2020', NULL, NULL, 'NFI2-3_change (2011-2020)', NULL, 4, 60, false);
delete from sdesign.t_reference_year_set where id = 60;

--reference_year_set_begin and reference_year_set_end panels have to overlap (have any elements in common)
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, comment, reference_year_set_begin, reference_year_set_end, status_variables)
    values
    (60, 4, '2016 - 2020', '2016-01-01', '2020-12-31', 'NFI3 (2016-2020)', NULL, NULL, NULL, true),
    (70, 5, '2016 - 2020', '2016-01-01', '2020-12-31', 'NFI3 (2016-2020)', NULL, NULL, NULL, true);

/*
select * from sdesign.cm_refyearset2panel_mapping;
 id | panel | reference_year_set | comment
----+-------+--------------------+---------
  1 |     1 |                  1 |
  2 |     1 |                  2 |
  3 |     2 |                  1 |
  4 |     2 |                  2 |
  7 |     3 |                  5 |
  8 |     4 |                  4 |
  9 |     5 |                  3 |
 10 |     5 |                  4 |
 11 |     5 |                  5 |
 12 |     6 |                  4 |
 13 |     7 |                  3 |
 14 |     7 |                  4 |
 15 |     7 |                  5 |
(13 rows)
*/
--------------------
--panels belonging to change refyearset have to be contained in corresponding state refyearsets
insert into sdesign.cm_refyearset2panel_mapping (reference_year_set , panel) values (5,1);


/*
select *
from sdesign.t_plot_measurement_dates
inner join sdesign.t_reference_year_set on (t_plot_measurement_dates.reference_year_set = t_reference_year_set.id)
--where t_reference_year_set.reference_year_set_begin is not null
limit 5
;
*/
--------------------
--t_plot_measurement_dates should not contain references to chenge reference_year_sets

insert into sdesign.t_plot_measurement_dates (plot, reference_year_set) values (1,5);

--------------------
update sdesign.t_panel SET sweight_panel_sum = 13 where id = 1;

-------------------------------------
insert into sdesign.t_panel (stratum, panel, cluster_configuration, label, comment, cluster_count, plot_count, panel_subset, sweight_panel_sum) 
values (5, 'NFRD14- 1plot, s2c', 1, 'blahblah', NULL, 123, 456, NULL, 123.132 );
