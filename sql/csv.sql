--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--

CREATE EXTENSION file_fdw with schema public;
CREATE SERVER csv_files FOREIGN DATA WRAPPER file_fdw;
CREATE SCHEMA csv;

--
-- PostgreSQL database dump
--
\set srcdir `echo $SRC_DIR`
--
-- Data for Name: countries; Type: TABLE DATA; Schema: csv; Owner: vagrant
--

\set afile :srcdir '/sql/csv/countries.csv'

CREATE FOREIGN TABLE csv.countries (
    id              integer           not null,
    label           character varying(20)           not null,
    description     character varying(20)           not null
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );


--
-- Data for Name: clusters; Type: TABLE DATA; Schema: csv; Owner: vagrant
--

\set afile :srcdir '/sql/csv/clusters.csv'
CREATE FOREIGN TABLE csv.clusters (
	country			character varying(20)		not null,
	strata_set		character varying(20)		not null,
	stratum			character varying(20)		not null,
	panel			character varying(20)		not null,
	cluster			character varying(20)		not null,
	sampling_weight_ha	float				not null,
	comment			text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );


--
-- Data for Name: cluster_configurations; Type: TABLE DATA; Schema: csv; Owner: vagrant
--

\set afile :srcdir '/sql/csv/cluster_configurations.csv'
CREATE FOREIGN TABLE csv.cluster_configurations (
	country			character varying(20)		not null,
	strata_set		character varying(20)		not null,
	stratum			character varying(20)		not null,
	cluster_configuration	character varying(20)		not null,
	label			character varying(120)		not null,
	cluster_design		boolean				not null,
	cluster_geom		text,
	cluster_rotation	boolean				not null,
	frame_area_ha		double precision,
	comment			text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );


--
-- Data for Name: strata_sets; Type: TABLE DATA; Schema: csv; Owner: vagrant
--

\set afile :srcdir '/sql/csv/strata_sets.csv'
CREATE FOREIGN TABLE csv.strata_sets (
	country		character varying(20)		not null,
	strata_set	character varying(20)		not null,
	label		character varying(120)		not null,
	comment		text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );


--
-- Data for Name: strata; Type: TABLE DATA; Schema: csv; Owner: vagrant
--

\set afile :srcdir '/sql/csv/strata.csv'
CREATE FOREIGN TABLE csv.strata (
	country			character varying(20)		not null,
	strata_set		character varying(20)		not null,
	stratum			character varying(20)		not null,
	label			character varying(120)		not null,
	geometry		text				not null,
	area_ha			double precision,
	comment			text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );


--
-- Data for Name: plots; Type: TABLE DATA; Schema: csv; Owner: vagrant
--

\set afile :srcdir '/sql/csv/plots.csv'
CREATE FOREIGN TABLE csv.plots (
	country			character varying(20)		not null,
	strata_set		character varying(20)		not null,
	stratum			character varying(20)		not null,
	panel			character varying(20)		not null,
	cluster			character varying(20)		not null,
	plot			character varying(20)		not null,
	plot_geometry		text,
	coordinates_degraded	boolean,
	comment			text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );
/*
CREATE VIEW csv.plots AS
SELECT
	country,
	strata_set,
	stratum,
	panel,
	cluster,
	plot,
	NULL::text as plot_geometry,
	NULL::boolean as coordinates_degraded,
	comment
FROM csv.plots_origin;
*/
--
-- Data for Name: panels; Type: TABLE DATA; Schema: csv; Owner: vagrant
--

\set afile :srcdir '/sql/csv/panels.csv'
CREATE FOREIGN TABLE csv.panels (
	country			character varying(20)		not null,
	strata_set		character varying(20)		not null,
	stratum			character varying(20)		not null,
	cluster_configuration	character varying(20)		not null,
	panel			character varying(20)		not null,
	panel_subset		character varying(20),
	label			character varying(120)		not null,
	comment			text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );


--
-- Data for Name: inventory_campaigns; Type: TABLE DATA; Schema: csv; Owner: vagrant
--

\set afile :srcdir '/sql/csv/inventory_campaigns.csv'
CREATE FOREIGN TABLE csv.inventory_campaigns (
	country				character varying(20)		not null,
	inventory_campaign		character varying(20)		not null,
	label				character varying(120)		not null,
	status_variables		boolean				not null,
	comment				text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );


--
-- Data for Name: reference_year_sets; Type: TABLE DATA; Schema: csv; Owner: vagrant
--

\set afile :srcdir '/sql/csv/reference_year_sets.csv'
CREATE FOREIGN TABLE csv.reference_year_sets (
	country				character varying(20)		not null,
	inventory_campaign		character varying(20)		not null,
	reference_year_set		character varying(20)		not null,
	label				character varying(120)		not null,
	status_variables		boolean				not null,
	reference_date_begin		date				not null,
	reference_date_end		date				not null,
	inventory_campaign_begin	character varying(20),
	reference_year_set_begin	character varying(20),
	inventory_campaign_end 		character varying(20),
	reference_year_set_end 		character varying(20),
	comment				text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );


--
-- Data for Name: plot_measurement_dates; Type: TABLE DATA; Schema: csv; Owner: vagrant
--

\set afile :srcdir '/sql/csv/plot_measurement_dates.csv'
CREATE FOREIGN TABLE csv.plot_measurement_dates (
	country			character varying(20)		not null,
	inventory_campaign	character varying(20)		not null,
	reference_year_set	character varying(20)		not null,
	strata_set		character varying(20)		not null,
	stratum			character varying(20)		not null,
	panel			character varying(20)		not null,
	cluster			character varying(20)		not null,
	plot			character varying(20)		not null,
	measurement_date	date,
	comment			text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );


--
-- Data for Name: refyearset2panel; Type: TABLE DATA; Schema: csv; Owner: vagrant
--

\set afile :srcdir '/sql/csv/refyearset2panel.csv'
CREATE FOREIGN TABLE csv.refyearset2panel (
	country			character varying(20)		not null,
	inventory_campaign	character varying(20)		not null,
	reference_year_set	character varying(20)		not null,
	strata_set		character varying(20)		not null,
	stratum			character varying(20)		not null,
	panel			character varying(20)		not null,
	comment			text
) SERVER csv_files
OPTIONS ( header 'true', format 'csv', delimiter ';', filename :'afile' );

--
-- PostgreSQL database dump complete
--
