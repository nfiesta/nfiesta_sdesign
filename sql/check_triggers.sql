/*
select * from sdesign.t_inventory_campaign;
*/

--OK
insert into sdesign.t_inventory_campaign
    (id, inventory_campaign, label, status_variables, comment) 
    values
    (6, 'NFI1-2', 'NFI1-2', false, 'change bteween NFI1 and NFI2');
delete from sdesign.t_inventory_campaign where id = 6;

/*
--both inventory_campaign_begin and inventory_campaign_end have to be NULL or NOT NULL
insert into sdesign.t_inventory_campaign
    (id, inventory, label, status_variables, comment, inventory_campaign_begin, inventory_campaign_end) 
    values
    (6, 'NFI1-2', 'NFI1-2', 'change bteween NFI1 and NFI2', 1, NULL);

insert into sdesign.t_inventory_campaign
    (id, inventory, label, status_variables, comment, inventory_campaign_begin, inventory_campaign_end) 
    values
    (6, 'NFI1-2', 'NFI1-2', 'change bteween NFI1 and NFI2', NULL, 1);

--inventory_campaign_begin and inventory_campaign_end have to be different
insert into sdesign.t_inventory_campaign
    (id, inventory, label, status_variables, comment, inventory_campaign_begin, inventory_campaign_end) 
    values
    (6, 'NFI1-2', 'NFI1-2', 'change bteween NFI1 and NFI2', 1, 1);

--OK
insert into sdesign.t_inventory_campaign
    (id, inventory, label, status_variables, comment, inventory_campaign_begin, inventory_campaign_end) 
    values
    (6, 'NFI1-2', 'NFI1-2', 'change bteween NFI1 and NFI2', 1, 2);
delete from sdesign.t_inventory_campaign where id = 6;

--referenced campaigns have to be "state" (with NULL inventory_campaign_begin/end)
insert into sdesign.t_inventory_campaign
    (id, inventory, label, status_variables, comment, inventory_campaign_begin, inventory_campaign_end) 
    values
    (5, 'NFI1-2-3', 'NFI1-2-3', 'change bteween NFI1-2 and NFI3', 4, 3);

insert into sdesign.t_inventory_campaign
    (id, inventory, label, status_variables, comment, inventory_campaign_begin, inventory_campaign_end) 
    values
    (5, 'NFI1-2-3', 'NFI1-2-3', 'change bteween NFI1-2 and NFI3', 3, 4);

--insert or update on table "t_inventory_campaign" violates foreign key constraint "fkey__t_inventory_campaign__t_inventory_campaign_end"
insert into sdesign.t_inventory_campaign
    (id, inventory, label, status_variables, comment, inventory_campaign_begin, inventory_campaign_end) 
    values
    (6, 'NFI1-2', 'NFI1-2', 'change bteween NFI1 and NFI2', 1, 13);
*/
/*
select * from sdesign.t_reference_year_set;
*/

--OK
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, status_variables, comment, reference_year_set_begin, reference_year_set_end)
    values
    (40, 4, '2016 - 2020', '2016-01-01', '2020-12-31', 'NFI3 (2016-2020)', true, NULL, NULL, NULL);
delete from sdesign.t_reference_year_set where id = 40;

--check constraint "chck_reference_date_begin"/check constraint "chck_reference_date_end"
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, status_variables, comment, reference_year_set_begin, reference_year_set_end)
    values
    (40, 4, '2016 - 2020', '2016-01-01', '2020-12-31', 'NFI3 (2016-2020)', false, NULL, NULL, 2);

insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, status_variables, comment, reference_year_set_begin, reference_year_set_end)
    values
    (40, 4, '2016 - 2020', NULL, '2020-12-31', 'NFI3 (2016-2020)', false, NULL, NULL, 2);

--reference_year_set_begin and reference_year_set_end have to be different
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, status_variables, comment, reference_year_set_begin, reference_year_set_end)
    values
    (40, 4, '2016 - 2020', NULL, NULL, 'NFI3 (2016-2020)', false, NULL, 2, 2);

--reference_year_set_begin and reference_year_set_end have to be "state"
insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, status_variables, comment, reference_year_set_begin, reference_year_set_end)
    values
    (40, 4, '2016 - 2020', NULL, NULL, 'NFI3 (2016-2020)', false, NULL, 2, 22);

insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, status_variables, comment, reference_year_set_begin, reference_year_set_end)
    values
    (40, 4, '2016 - 2020', NULL, NULL, 'NFI3 (2016-2020)', false, NULL, 22, 2);

--change refyearset has to reference change inventory campaign and state refyearset has to reference state inventory campaign

insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, status_variables, comment, reference_year_set_begin, reference_year_set_end)
    values
    (40, 5, '2016 - 2020', '2016-01-01', '2020-12-31', 'NFI3 (2016-2020)', true, NULL, NULL, NULL);

insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, status_variables, comment, reference_year_set_begin, reference_year_set_end)
    values
    (40, 4, '2016 - 2020', NULL, NULL, 'NFI3 (2016-2020)', false, NULL, 2, 12);


--reference_year_set_begin and reference_year_set_end must have different inventory campaigns

insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, status_variables, comment, reference_year_set_begin, reference_year_set_end)
    values
    (40, 2, 'abc', '2011-01-01', '2015-12-31', 'NFI3 (2016-2020) test', true, NULL, NULL, NULL);

insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, status_variables, comment, reference_year_set_begin, reference_year_set_end)
    values
    (41, 3, 'abc', NULL, NULL, 'NFI3 (2016-2020) test', false, NULL, 4, 40);
delete from sdesign.t_reference_year_set where id = 40;

--reference_year_set_begin and reference_year_set_end panels have to overlap (have any elements in common)

insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, status_variables, comment, reference_year_set_begin, reference_year_set_end)
    values
    (40, 5, 'abc', NULL, NULL, 'NFI3 (2016-2020)', false, NULL, 1, 11);

--inventory campaign (state) of referenced state reference_year_sets has to correspond to inventory campaign (state) referenced from change inventory campaign
/*
insert into sdesign.t_inventory_campaign (id, inventory, label, status_variables, comment, inventory_campaign_begin, inventory_campaign_end) 
	values (6, 'NFI1-2_change', 'NFI1-2_change', 'test', 1, 2);

insert into sdesign.t_reference_year_set
    (id, inventory_campaign, reference_year_set, reference_date_begin, reference_date_end, label, status_variables, comment, reference_year_set_begin, reference_year_set_end)
    values
    (40, 6, 'abc', '2011-01-01', '2020-12-31', 'test', NULL, 3, 4);

delete from sdesign.t_inventory_campaign where id = 6;
*/
/*
select reference_year_set, array_agg(panel order by panel) as panels 
from sdesign.cm_refyearset2panel_mapping group by reference_year_set order by reference_year_set;
*/
--------------------
--panels belonging to change refyearset have to be contained in corresponding state refyearsets

insert into sdesign.cm_refyearset2panel_mapping (reference_year_set , panel) values (30,1);

/*
select * 
from sdesign.t_plot_measurement_dates 
inner join sdesign.t_reference_year_set on (t_plot_measurement_dates.reference_year_set = t_reference_year_set.id)
--where t_reference_year_set.reference_year_set_begin is not null
limit 5
;
*/
--------------------
--t_plot_measurement_dates should not contain references to chenge reference_year_sets

insert into sdesign.t_plot_measurement_dates (plot, reference_year_set) values (1,39);

--------------------
update sdesign.t_panel SET sweight_panel_sum = 13 where id = 1;

-------------------------------------
insert into sdesign.t_panel (stratum, panel, cluster_configuration, label, comment, cluster_count, plot_count, panel_subset, sweight_panel_sum) 
values (2, 'y1_s1_s2ab', 2, 'blahblah', NULL, 123, 456, NULL, 123.132 );

